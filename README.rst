.. -*- coding: utf-8 -*-

=======================================
Entrenamiento "Diseño Web en Plone CMS"
=======================================

Repositorio de manuales y recursos del entrenamiento "Diseño Web en Plone CMS"
realizado por la empresa `Covantec R.L. <https://github.com/Covantec>`_

.. contents :: :local:

Estructura general
==================

La estructura general de contenidos esta confirmada por los principales archivos:

**00-capacitacion-diseno-web-plone.odt**
  Describe el contenido del entrenamiento.

**01-introduccion-diseno-web-plone.odp**
  Describe los contenidos del módulo *1* del entrenamiento.

**02-haciendo-temas-diazo**
  Describe los contenidos del módulo *2* del entrenamiento.

**03-0-editor-temas-diazo**
  Describe los contenidos del módulo *3* del entrenamiento.

**03-1-guias-temas-diazo**
  Describe los contenidos del módulo *3* del entrenamiento.

**04-portal-zmi-registro-config.odp**
  Describe los contenidos del módulo *4* del entrenamiento.

**06-tal-metal-zpt**
  Describe los contenidos del módulo *6* del entrenamiento.

**07-0-personalizar-vistas-viewlets.odp**
  Describe los contenidos del módulo *7* del entrenamiento.

**07-1-personalizar-vistas-viewlets**
  Describe los contenidos del módulo *7* del entrenamiento.

**08-0-maqueta-estilo-tema.odp**
  Describe los contenidos del módulo *8* del entrenamiento.

**08-1-maqueta-via-productos.odp**
  Describe los contenidos del módulo *8* del entrenamiento.

**08-2-prototipos-frontend-patternslib.odp**
  Describe los contenidos del módulo *8* del entrenamiento.

**09-framework-css-dinamico**
  Describe los contenidos del módulo *9* del entrenamiento.

**POR TERMINAR**.


Obtener y compilar la documentación
===================================

El almacenamiento de este material está disponible en un repositorio Git 
en "`entrenamiento.diseno_web_plone`_". 

Si usted tiene una credenciales en este servidor y desea convertirse en un colaborador 
de los materiales de este entrenamiento, usted debe seguir los siguientes pasos:


Dependencias
------------

Para construir estos recursos, debe ejecutar las dependencias, entonces debe ejecutar 
los siguientes comando:

::

  $ sudo apt-get install python-pip python-setuptools git
  $ sudo apt-get install texlive-latex-base texlive-latex-recommended texlive-lang-spanish
  $ sudo pip install virtualenv


Descargar repositorio
---------------------

Para descargar repositorio para modificar los recursos del entrenamiento, ejecute los 
siguientes comando:

::

  $ cd $HOME
  $ git clone https://bitbucket.org/covantec/entrenamiento.diseno_web_plone.git

Crear entorno virtual de Python para reconstruir este proyecto, ejecutando el siguiente 
comando:

::

  $ cd ~/entrenamiento.python_basico
  $ virtualenv --python=/usr/bin/python venv
  $ source ./venv/bin/activate

Luego instale dependencias del paquete ``Sphinx``, ejecutando el siguiente comando:

::

  (venv)$ pip install -r requirements.txt

De esta forma, usted ya instalo todas las dependencias necesarias de los diversos recursos 
usado en el entrenamiento, los cuales fueron hechos con **reStructuredText (reST)**, 
**OpenDocument Presentation** o **OpenDocument Text**.


Recursos del entrenamiento
==========================

Los recursos de este entrenamiento estaba realizado con los siguientes formatos: 

- **OpenDocument Text**, usando la herramienta `LibreOffice <https://es.libreoffice.org/>`_ *Writer*,
  este formato es usado para generar ciertos documentos de textos del entrenamiento, mas comúnmente en formatos ODT o PDF.

- **OpenDocument Presentation**, usando la herramienta `LibreOffice <https://es.libreoffice.org/>`_ 
  *Impress*, este formato es usado para generar las presentaciones del entrenamiento, mas comúnmente en formatos ODP o PDF.

- **reStructuredText (reST)**, usando la herramienta `Sphinx <https://www.sphinx-doc.org/>`_, este 
  se formato es usado para generar los recursos del entrenamiento, mas comúnmente en formato PDF:


Formato PDF
-----------

Usted puede generar la documentación en PDF de los módulos *2, 3, 5, 6, 7, 8, 9* usando la herramienta 
``Sphinx``; para realizar esto debe instalar ejecute los siguientes comando:


Módulo 2
````````

Ahora puede generar la documentación en PDF del módulo *2*; ejecute los siguientes comando: ::

  (venv)$ cd ./02-haciendo-temas-diazo
  (venv)$ make pdf

Una vez generado el PDF se puede abrir desde el directorio ``build/latex/02-haciendo-temas-diazo.pdf``
con cualquiera de sus programas de visor de PDF favorito (Evince, Acrobat Reader, etc).


Módulo 3
````````

Ahora puede generar la documentación en PDF del módulo *3*; ejecute los siguientes comando: ::

  (venv)$ cd ./03-0-editor-temas-diazo
  (venv)$ make pdf

Una vez generado el PDF se puede abrir desde el directorio ``build/latex/03-0-editor-temas-diazo.pdf``
con cualquiera de sus programas de visor de PDF favorito (Evince, Acrobat Reader, etc).


Módulo 3.1
``````````

Ahora puede generar la documentación en PDF del módulo *3*; ejecute los siguientes comando: ::

  (venv)$ cd ./03-1-guias-temas-diazo
  (venv)$ make pdf

Una vez generado el PDF se puede abrir desde el directorio ``build/latex/03-1-guias-temas-diazo.pdf``
con cualquiera de sus programas de visor de PDF favorito (Evince, Acrobat Reader, etc).


Módulo 6
````````

Ahora puede generar la documentación en PDF del módulo *6*; ejecute los siguientes comando: ::

  (venv)$ cd ./06-tal-metal-zpt
  (venv)$ make pdf

Una vez generado el PDF se puede abrir desde el directorio ``build/latex/06-tal-metal-zpt.pdf``
con cualquiera de sus programas de visor de PDF favorito (Evince, Acrobat Reader, etc).


Módulo 7
````````

Ahora puede generar la documentación en PDF del módulo *7*; ejecute los siguientes comando: ::

  (venv)$ cd ./07-1-personalizar-vistas-viewlets
  (venv)$ make pdf

Una vez generado el PDF se puede abrir desde el directorio ``build/latex/07-1-personalizar-vistas-viewlets.pdf``
con cualquiera de sus programas de visor de PDF favorito (Evince, Acrobat Reader, etc).


Módulo 9
````````

Ahora puede generar la documentación en PDF del módulo *9*; ejecute los siguientes comando: ::

  (venv)$ cd ./09-framework-css-dinamico
  (venv)$ make pdf

Una vez generado el PDF se puede abrir desde el directorio ``build/latex/09-framework-css-dinamico.pdf``
con cualquiera de sus programas de visor de PDF favorito (Evince, Acrobat Reader, etc).


Formato HTML
------------

Alternativamente usted puede generar la documentación en formato HTML  de los módulos *2, 3, 5, 6, 7, 8, 9* 
usando la herramienta ``Sphinx``; dentro del directorio respectivo de cada módulo mencionado, ejecutando el 
siguiente comando:

::

  (venv)$ make html

Una vez generado el formato HTML se puede abrir desde el directorio ``build/html/index.html``
con su navegador Web favorito (Mozilla Firefox, Google Chrome, etc).


Formato OpenDocument
--------------------

Usted puede manipular la documentación en ODT y ODP, no solo para visualizar la presentación, sino 
ademas para generar el formato PDF de los módulos *1, 4, 7, 8, 9* usando la herramienta ``LibreOffice``.


Colabora
========

¿Tiene alguna idea?, ¿Encontró un error? Por favor, hágalo saber registrando un `ticket de soporte`_.

.. _entrenamiento.diseno_web_plone: https://bitbucket.org/covantec/entrenamiento.diseno_web_plone
.. _ticket de soporte: https://bitbucket.org/covantec/entrenamiento.diseno_web_plone/issues/new
