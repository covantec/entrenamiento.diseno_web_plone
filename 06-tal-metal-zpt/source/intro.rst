.. -*- coding: utf-8 -*-

Introducción
============

Plone CMS utiliza lenguaje dinámico de plantillas basado 
en atributos HTML para integrar los elementos dinámicos 
de la base de datos con las plantillas estáticas HTML.

Debido a este lenguaje esta basado en atributos HTML facilita 
a los diseñadores Web que escriben HTML a que pueden modificar 
dichas plantillas de forma gráfica con cualquier editor WYSIWYG 
sin afectar los elementos y sintaxis dinámicas que conectan con 
la base de datos.

Esta tecnología se llama *Template Attribute Language - TAL*, 
más adelante en este manual se explicará su uso con ejemplos.

