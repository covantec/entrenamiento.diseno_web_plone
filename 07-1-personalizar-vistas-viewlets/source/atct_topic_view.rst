.. -*- coding: utf-8 -*-

Práctica 1 - Vista tabular de Colección con resumen
===================================================

Se requiere personalizar la vista tabular del tipo de contenido 
Colección para agregar el campo de resumen.

Sobrescribir vistas existentes
------------------------------

#. Primero se define la dependencia de paquete :ref:`z3c.jbot <z3c.jbot>` 
en el paquete tema ``ploneconf2014.theme`` en el archivo 
``ploneconf2014.theme/setup.py``:

   .. code-block:: python

        ...
        install_requires=[
            'setuptools',
            'z3c.jbot',
            ...
        ],
        ...

#. A continuación se especifica la siguiente directiva ``ZCML``
   en el archivo del paquete tema ``ploneconf2014.theme`` ubicado 
   en ``ploneconf2014.theme/ploneconf2014/theme/configure.zcml``:

   .. code-block:: xml

       <configure
           ...
           xmlns:browser="http://namespaces.zope.org/browser">
       ...
       <include package="z3c.jbot" file="meta.zcml" />
       ...
       <browser:jbot
           directory="template_overrides"
           layer=".interfaces.IThemeSpecific"
         />

#. La ``BrowserLayer`` está ahora configurada en el archivo
   del paquete tema ``ploneconf2014.theme`` ubicado en ``ploneconf2014.theme/ploneconf2014/theme/profiles/default/browserlayer.xml``:

   .. code-block:: xml

       <?xml version="1.0"?>
       <layers>
           <layer name="ploneconf2014.theme"
                  interface="ploneconf2014.theme.interfaces.IThemeSpecific" />
       </layers>

#. El clase *Layer Interface* esperada ``IThemeSpecific`` esta en el archivo 
   del paquete tema ``ploneconf2014.theme`` ubicado en ``ploneconf2014.theme/ploneconf2014/theme/interfaces.py``:

   .. code-block:: python

       from plone.theme.interfaces import IDefaultPloneLayer

       class IThemeSpecific(IDefaultPloneLayer):
           """Marker interface that defines a Zope 3 browser layer.
           """

#. Por último, se crea la carpeta ``template_overrides`` dentro del paquete tema 
   ``ploneconf2014.theme`` y cree las sobrescrituras dentro de este, por ejemplo, 
   se copia del archivo ``atct_topic_view.pt`` del paquete ``Products.ATContentTypes``
   con el siguiente nombre ``Products.ATContentTypes.skins.ATContentTypes.atct_topic_view.pt`` 
   en el directorio ``ploneconf2014.theme/ploneconf2014/theme/template_overrides/``.

   ..
     This page template can then, for example, a teaser element expand:

     Dieses PageTemplate lässt sich dann z.B. um ein Teaser-Element
     erweitern:

   En esta page template puede entonces, por ejemplo, ampliar un elemento de reclamo:

   .. code-block:: xml

      <td>
          <img tal:condition="obj/hasTeaserImage"
               tal:attributes="src string:${obj/getURL}/@@teaserImage?scale=teaser"
               class="teaserImage"
               />
          <div class="teaserText"
               tal:condition="obj/teaserText"
               tal:content="structure obj/teaserText" />
      </td>

Expandir plantillas existentes
------------------------------

Con :ref:`z3c.jbot <z3c.jbot>` también puede crear fácilmente nuevas plantillas de existente.

..
  #. Hierzu wird nun in
     ``ploneconf2014.theme/ploneconf2014/theme/template_overrides/Products.ATContentTypes.skins.ATContentTypes.atct_topic_view.pt`` 
     eine Bedingung für die Tabellenzeile mit dem Teaser eingefügt:

#. En la page template ``Products.ATContentTypes.skins.ATContentTypes.atct_topic_view.pt`` 
   ubicada en el directorio ``ploneconf2014.theme/ploneconf2014/theme/template_overrides/`` 
   del paquete tema ``ploneconf2014.theme`` se inserta ahora una condición para la fila de 
   la tabla con el resumen:

    .. code-block:: xml

      <table class="listing" summary="Content listing"
             i18n:attributes="summary summary_content_listing;">
        <thead>
          <tr tal:condition="options/with_teaser | request/with_teaser | nothing">
          ...
          <td tal:condition="options/with_teaser | request/with_teaser | nothing">
            <tal:if condition="obj/hasTeaserImage">
              <a tal:attributes="href obj/getURL">
              <img tal:condition="obj/hasTeaserImage"
                tal:attributes="src string:${obj/getURL}/@@teaserImage?scale=teaser"
                class="teaserImage"
              />
              </a>
            </tal:if>
            <tal:if condition="not: obj/hasTeaserImage">
              <div class="title" tal:content="obj/Title" />
              <div class="description" tal:content="obj/Description" />
              <a tal:attributes="href obj/getURL"
                 i18n:translate="label_more">more</a>
            </tal:if>
          </td>
          ...

#. Ahora ``with_teaser`` se define en el paquete tema 
   ``ploneconf2014.theme`` dentro del modulo ubicado en 
   ``ploneconf2014.theme/ploneconf2014/theme/browser/topic.py``:

    .. code-block:: python

      from Products.Five.browser import BrowserView

      class TopicTeaserView(BrowserView):
          """ Topic table view with teaser """

          def __call__(self):
              view = self.context.restrictedTraverse('atct_topic_view')
              return view(with_teaser=True)


#. El atributo ``hasTeaserImage`` se recupera a partir del índice.

   .. note:: 
       Más información sobre el Indice consulte sobre 
       `plone.indexer <https://www.plone-entwicklerhandbuch.de/anhang/praxisbeispiele/plone.indexer>`_.


#. La nueva ``view`` se encuentra registrada dentro del paquete tema 
   ``ploneconf2014.theme`` en el archivo ``ZCML`` ubicado en 
   ``ploneconf2014.theme/ploneconf2014/theme/browser/configure.zcml``:

    .. code-block:: xml

        <browser:page
            name="atct_topic_teaser_view"
            for="Products.ATContentTypes.interfaces.topic.IATTopic"
            permission="zope2.View"
            class=".topic.TopicTeaserView"
          />

#. Por último, esta ``view`` debe estar configurada en el mismo archivo 
   ``ZCML`` del paquete tema ``ploneconf2014.theme`` ubicado en 
   ``ploneconf2014.theme/ploneconf2014/theme/browser/configure.zcml`` 
   para agregarla al menú ``Mostrar``:

    .. code-block:: xml

      <include package="plone.app.contentmenu" />
      ...
      <browser:menuItem
          for="Products.ATContentTypes.interfaces.topic.IATTopic"
          menu="plone_displayviews"
          title="Collection with teaser"
          action="atct_topic_teaser_view"
          description="Collection table view with teaser"
        />

#. Reinstalar paquete tema ``ploneconf2014.theme``.

#. Acceder a la sección Noticias, haga clic a la opción menú 
   ``Mostrar > Collection with teaser``.

    .. todo::

        Agregar una captura de pantalla de la opción menú Mostrar.

#. Visualice la vista ``Collection with teaser`` aplicada al tipo 
   de contenido Colección.

    .. todo::

        Agregar una captura de pantalla de la opción menú Mostrar.


Referencia 
----------

* `z3c.jbot en Plone Entwicklerhandbuch`_.

.. _z3c.jbot en Plone Entwicklerhandbuch: https://www.plone-entwicklerhandbuch.de/anhang/praxisbeispiele/z3c.jbot
