.. -*- coding: utf-8 -*-

Introducción
============

Debido a que Plone gestiona sus plantillas, vistas programáticas, 
recursos de imágenes, archivos CSS y Javascript dentro de paquete egg 
Python, esto hace que debe entender como personalizar esos cambios 
de forma tal que pueda reproducir esos cambios en futuras instalaciones.

Zope puede hospedar varios sitios Plone en una misma instancia 
del servidor aplicación, entonces las apariencias se personalizan 
mediante técnicas sobrescritura en vez de modificar directamente 
los archivos a personalizar en el paquete egg. Si se realiza de esta 
forma todos esos cambios se aplicaran a todos los sitios Plone 
hospedados en el mismo servidor Zope, es muy posible que no se 
desee eso, sino que esos cambios sea para un sitio en especifico.

Además sin olvidar lo difícil de mantener el código fuente cuando 
estén disponibles nuevos cambios o actualizaciones de seguridad.

Entonces su misión es comprender el mecanismo de personalizar a 
través de la Web usando la ZMI y productos Plone para esto, como 
hacer a esas personalizaciones sean reproducidos en otros 
instalaciones de forma asistida con la instalación de un 
paquete egg usando técnicas de sobrescritura vistas, viewlets y 
archivos recursos por defecto, con el paquete :ref:`z3c.jbot <z3c_jbot>` 
en el sistema de archivos y el navegador Web.
