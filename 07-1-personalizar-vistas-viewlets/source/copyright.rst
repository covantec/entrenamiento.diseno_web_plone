.. -*- coding: utf-8 -*-

.. acerca_copyright:

=========
Copyright
=========

Plone y el logo Plone
=====================

Plone® y el logotipo de Plone son marcas registradas por la `Fundación Plone`_.

Plone® and the Plone Logo are registered trademarks of `The Plone Foundation`_.


Autores originales
==================

El capítulo 1 están licenciado bajo `Creative Commons Attribution 4.0 International 
License <https://creativecommons.org/licenses/by/4.0/>`_ para los materiales del entrenamiento *The Mastering Plone Training* de la fundación Plone.

* Copyright © 2014 - 2015 `The Plone Foundation`_.

..
  Los capítulos 4, 5 y Los apéndices A, B, C, D, E, F, G están licenciado bajo `Creative Commons Atribución-NoComercial-CompartirIgual 3.0 Unported (CC BY-NC-SA 3.0) <https://creativecommons.org/licenses/by-nc-sa/3.0/deed.es>`_.

Traducción al Español
=====================

Todas esta traducción de esta documentación es licencia bajo la Creative Commons
Reconocimiento-CompartirIgual 3.0 Venezuela por:

* Copyright © 2010 - 2021 `Covantec R.L.`_. Algunos derechos reservados.

  * Leonardo J. Caballero G. <leonardoc@plone.org>.

.. seealso:: Ver :ref:`licencias <licencias>` para información completa sobre los términos y licencia.

.. _`Fundación Plone`: https://plone.org/foundation/
.. _`The Plone Foundation`: https://plone.org/foundation/
.. _`Covantec R.L.`: https://github.com/Covantec
