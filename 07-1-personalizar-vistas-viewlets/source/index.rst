.. -*- coding: utf-8 -*-

==============================
Personalizar Vistas y Viewlets
==============================

.. toctree::
    :maxdepth: 2
    :numbered: 1

    intro
    z3c_jbot
    atct_topic_view
    newsitem_summary_view
