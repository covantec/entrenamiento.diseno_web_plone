.. -*- coding: utf-8 -*-

.. _z3c_jbot:

Paquete z3c.jbot
================

El paquete `z3c.jbot`_ (o *"Just a bunch of templates"*) le 
permite fácilmente personalizar (sean por sobrescritura o 
sean extendidos) las plantillas, imágenes existentes y los 
objetos de la *Skins Tool*, como las *Page Templates*, 
archivos *CSS* y *Javascript* simplemente sean sobrescritos 
o extendidos.

Uso
---

Para sobrescribir un archivo en particular, primero debe 
determinar su nombre de *archivo de archivo canonical*. 
Ese se define como la ruta relativa al paquete el cual 
contiene el archivo; los separadores del directorio son 
remplazados por el carácter punto ``.``.

Ejemplo:

  Suponga usted quiere sobrescribir: ``/plone/app/layout/viewlets/logo.pt``

  Usted debe usar el nombre de archivo: ``plone.app.layout.viewlets.logo.pt``

Simplemente coloque el archivo en un directorio, este 
directorio sera usado por el paquete ``jbot`` y registrelo 
mediante una directiva ``ZCML``::

  <include package="z3c.jbot" file="meta.zcml" />

  <browser:jbot
      directory="<ruta-al-directorio>"
      layer="<ruta-relativa-interface-layer>" />

Plantillas en vistas, viewlets y portlets
-----------------------------------------

Cualquier plantillas que es definida como un atributo de 
clase puede ser sobrescrito usando ``jbot``, ej. esas 
usadas en vistas, viewlets y portlets. La plantilla 
sobrescrita quizás se registro para cualquier request de 
capa o solamente para una especifica capa.

Objetos CMF
-----------

Cualquier objetos ``skin`` (ej. imágenes, plantillas) en 
el sistema de archivo (directorio de vistas) pueden ser 
sobrescrito.

.. _`z3c.jbot`: https://pypi.org/project/z3c.jbot
