.. -*- coding: utf-8 -*-

===========================
manual de plone.app.theming
===========================

Esta guía proporciona una descripción general sobre los tema Diazo en las versiones
4.3 y superiores de Plone.

.. contents:: Contenidos
    :local:

Introducción
============

En versiones 4.3 y superiores de `Plone <https://plone.org>`_ usted
puede editar el tema de su sitio web a través del navegador web en el panel de control 
de configuración del sitio de Plone.
Solo HTML, CSS y poco conocimiento de XML son necesarios como requisitos previos.
Esta guía explica cómo usar esta característica de Plone.

Ver `vídeo de presentación a plone.app.theming <https://vimeo.com/42564510>`_.

¿Qué es un tema de Diazo?
=========================

Un "tema" hace que un sitio web (en este caso, uno impulsado por Plone) tome una
aspecto y sensación particular.

*Diazo* (anteriormente conocido como XDV) es una tecnología que se puede utilizar para tematizar
sitios web. No es específico de Plone per se, sino que ha sido creado por Plone
comunidad y, a partir de Plone 4.3, proporciona la forma predeterminada de aplicar un tema a un
Sitio de Plone. Puede obtener más información sobre Diazo en http://docs.diazo.org/en/latest/.

Los temas de Diazo pueden ser un poco diferentes a los temas que ha creado en otros
sistemas, y de hecho a los temas que puede haber creado para versiones anteriores de
Plone. Un tema de Diazo trata realmente de transformar algunos contenidos, en este caso
el resultado HTML desde el sitio web "vanilla" de Plone - en un conjunto diferente de marcado HTML para
aplicar un conjunto de reglas para combinar una maqueta HTML estática del resultado final que
querer con el contenido dinámico proveniente de Plone.

En comparación, la forma anterior de tema un sitio Plone (como la forma en que muchos otros
los sistemas de gestión de contenido son tematizados) se basa en anular selectivamente las
plantillas y los scripts que Plone usa para construir una página con versiones personalizadas
que producen diferentes marcas HTML. El último enfoque puede ser más poderoso,
sin duda, pero también requiere un conocimiento mucho más profundo de las partes internas de Plone y
comando de las tecnologías del lado del servidor tales como Zope Page Templates e incluso Python.
Los temas de Diazo, por el contrario, son fáciles de entender para los diseñadores web y no
desarrolladores por igual.

Un tema Diazo consta de tres elementos:

1. Una o más maquetas HTML, también conocidas como archivos *theme*, que representan
   la apariencia deseada.

   Estos contendrán marcadores de posición para el contenido que debe proporcionar el
   Sistema de gestión de contenido Plone. Las maquetas usualmente hacen referencia a CSS, JavaScript
   y archivos de imagen por ruta relativa. La forma más común de crear un tema es
   utilizar software de escritorio como Dreamweaver o un editor de texto para crear el relevante
   marcado, estilos y scripts, y prueba el tema localmente en un navegador web.
2. El *contenido* que se está tematizando. En este caso, ese es el resultado de
   Plone.
3. Un *rules file*, que define cómo los marcadores de posición en el tema (es decir, la
   Maqueta HTML) debe ser reemplazado por marcado relevante en el contenido.

   El archivo de reglas usa sintaxis XML (similar a HTML). Aquí hay un muy simple
   ejemplo::

     <?xml version="1.0" encoding="UTF-8"?>
     <rules
         xmlns="http://namespaces.plone.org/diazo"
         xmlns:css="http://namespaces.plone.org/diazo/css"
         xmlns:xsl="http://www.w3.org/1999/XSL/Transform">`

         <theme href="theme.html" />

         <replace css:content-children="#content" css:theme-children="#main" />

     </rules>

   Aquí, estamos reemplazando los contenidos (nodos secundarios) de un elemento 
   de marcador de posición con HTML id ``main`` en el archivo de tema (``theme.html``, 
   encontrado en el mismo directorio como el archivo ``rules.xml``, como se hace 
   referencia en la regla ``<theme />`` con los contenidos (hijos) del elemento
   con el id de HTML ``content`` en el marcado generado por Plone.

   Cuando se aplica este tema, el resultado se parecerá mucho al archivo
   estática HTML ``theme.html`` (y su archivos CSS, JavaScript e imagen referenciados),
   excepto el marcador de posición que se identifica por el nodo en el tema
   con id ``main`` se rellenará con el área de contenido principal de Plone.

Plone incorpora un tema de ejemplo llamado, apropiadamente, *Example theme*, que
usa el venerable `Twitter Bootstrap <https://getbootstrap.com/2.3.2/>`_
para construir un tema simple pero funcional exponiendo la mayor parte del núcleo de Plone
funcionalidad. Se le aconseja estudiarlo, en particular el archivo 
``rules.xml``: para obtener más información sobre cómo funcionan los temas Diazo.

Usando el panel de control
==========================

Después de la instalación del paquete *Soporte a temas Diazo* en un sitio Plone, el
panel de control *Temas* aparecerá en la *Configuración del sitio* de Plone.

La pestaña principal de este panel de control, *Temas*, mostrará todos los temas disponibles,
con botones para activar / desactivar, modificar, copiar o eliminar cada uno, así como
botones para crear nuevos temas o mostrar este texto de ayuda.

Haga clic en una imagen de vista previa del tema para abrir una vista previa de ese tema en una nueva pestaña
o ventana La vista previa es navegable, pero los envíos de formularios y algunos de 
las características avanzados no funcionarán.

Seleccionar un tema
-------------------

Para aplicar un tema existente, simplemente haga clic en el botón *Activar* debajo del
vista previa del tema. El tema activo actualmente se resaltará en color amarillo. Si
usted desactiva el tema actualmente activo, no se aplicará el tema Diazo, es decir
se aplicará el tema "vanilla" de Plone.

*Nota*: El panel de control *Temas* nunca sera tematizado, lo que garantiza que pueda
siempre desactive un tema errante que podría inutilizar el panel de control.
Por lo tanto, es posible que no vea ninguna diferencia inmediatamente después de habilitar 
un tema. Simplemente navegar a otra página en el sitio Plone, sin embargo, y debería ver el
tema aplicado.

Creando un nuevo tema
---------------------

Se pueden crear nuevos temas de una de estas dos maneras:

* Haga clic en el botón *Nuevo tema* en la parte superior de la pestaña *Temas* en el 
  panel de control *Temas* e ingrese un título y una descripción en el formulario que aparece.
  Se creará un tema escueto, y se te llevará a la pantalla *Modificar tema*
  (ver a continuación), donde puede editar o crear archivos de temas y reglas.
* Haga clic en el botón *Copiar* debajo de cualquier tema existente e ingrese un título y
  descripción en el formulario que aparece. Se creará un nuevo tema como una copia
  del tema existente, y se te llevará al *Modificar tema* (ver
  a continuación), donde puede editar o crear archivos de temas y reglas.

Subiendo un tema existente
--------------------------

Los temas se pueden distribuir como archivos Zip, que contienen la maqueta HTML y
el archivo de reglas. Para descargar un tema existente, haga clic en el botón 
*Descargar* debajo del tema en la pestaña *Temas* del panel de control *Temas*.

Para cargar un archivo Zip de este tipo en otro sitio, use el botón *Cargar archivo Zip*
en la pestaña *Temas* del panel de control *Temas*. Puede elegir si reemplazará o no 
cualquier tema existente con el mismo nombre (según el nombre del directorio de nivel 
superior contenido en el archivo Zip).

También puede cargar un archivo Zip de una maqueta HTML estática que no contenga un
archivo de reglas, como un diseño proporcionado por un diseñador Web independiente de Plone.

En este caso, se agregará un archivo básico ``rules.xml`` para que pueda comenzar a construir
un tema desde el uso de la pantalla *Modificar tema* (ver a continuación). El archivo de reglas 
generado asumirá que el archivo de maqueta HTML principal se llama ``index.html``, pero puedes 
cambiar esto en el archivo ``rules.xml``.

Una vez que hayas cargado correctamente un archivo Zip de tema, serás llevado a la pantalla
*Modificar tema* (ver a continuación), donde puede editar o crear archivos de temas.

*Sugerencia:* Si aparece un mensaje de error como "El archivo cargado no contiene
un archivo de tema válido", esto generalmente significa que ha subido un archivo Zip
que contiene múltiples archivos y carpetas, en lugar de un solo nivel superior
carpeta el cual contenga todos los recursos del tema. Esto podría suceder si comprimiste
un tema o maqueta de HTML al agregar sus archivos y carpetas directamente a un archivo Zip,
en lugar de comprimir el directorio en el que se encontraron. Para arreglar esto,
simplemente descomprima el archivo en su computadora en un nuevo directorio, suba un nivel,
y comprimir este directorio por sí mismo en un nuevo archivo Zip, que luego puede
subir.

Modificando el tema
-------------------

Puede modificar un tema haciendo clic en *Modificar tema* debajo de un tema en la
pestaña *Temas* del panel de control *Temas*. Esta pantalla también se lanzó
automáticamente cuando creas o cargas un nuevo tema.

*Nota:* Solo los temas creados o cargados a través del panel de control *Temas* pueden
ser modificado a través de Plone. Los temas instalados por complementos de terceros o
distribuido en el sistema de archivos no se pueden modificar, aunque los cambios realizados 
en el sistema de archivos se reflejará de inmediato si Zope se ejecuta en modo de depuración.
Para modificar un tema del sistema de archivos, puede copiarlo en un nuevo tema en Plone 
haciendo clic en *Copiar* debajo del tema en el panel de control *Temas*.

La pantalla *Modificar tema* muestra inicialmente un administrador de archivos, con un árbol de archivos en
la izquierda y un editor a la derecha. Haga clic en un archivo en el árbol de archivos para abrir un
editor o vista previa: HTML, CSS, JavaScript y otros archivos de texto pueden ser editados
directamente a través del editor. Otros archivos (por ejemplo, imágenes) se mostrarán como
una vista previa.

*Nota:* El editor avanzado con resaltado de sintaxis no está disponible en
Microsoft Internet Explorer.

Haga clic en *Nueva carpeta* para crear una nueva carpeta. También puede hacer clic derecho en una carpeta
en el árbol de archivos para mostrar esta acción.

Haga clic en *Nuevo archivo* para crear un nuevo archivo de texto. También puede hacer clic derecho en una
carpeta en el árbol de archivos para mostrar esta acción.

Haga clic en *Cargar archivo* para cargar un archivo desde su computadora. También puedes
hacer clic en una carpeta en el árbol de archivos para abrir esta acción.

Haga clic en *Vista previa* para obtener una vista previa del tema, ya que se aplicará con 
la maqueta y reglas tal como se guardan actualmente. La vista previa es navegable, pero las 
formas y ciertas las funciones avanzadas no funcionarán.

Para guardar el archivo que se está editando actualmente, haga clic en el botón *Guardar archivo*, o use
el atajo de teclado ``Ctrl + S`` (Windows / Linux) o ``Cmd + S`` (Mac).

Para cambiar el nombre o eliminar un archivo o carpeta, haga clic derecho en el árbol de archivos y
seleccione la acción apropiada.

El inspector de temas
---------------------

El inspector de temas proporciona una interfaz avanzada para descubrir y construir
las reglas de un tema Diazo. Se puede iniciar haciendo clic en botón de *Mostrar
inspectores* en la pantalla *Modificar tema* para temas de Plone, o
haciendo clic en el botón *Inspeccionar tema* debajo de un tema del sistema de 
archivos en el pestaña *Temas* del panel de control *Temas*.

El inspector de temas consta de dos paneles:

* La *Maqueta HTML*. Si hay varios archivos HTML en el tema, puede cambiar
  entre ellos usando la lista desplegable debajo del panel *maqueta HTML*.
* El *Contenido sin tema*. Esto muestra a Plone sin ningún tema aplicado.

Cualquiera de los paneles se puede maximizar haciendo clic en el ícono de flechas 
en la parte superior derecha de ya sea.

Los paneles *Maquetas HTML* y *Contenido sin tema* se pueden cambiar a vista de código fuente,
mostrando su marcado HTML subyacente, haciendo clic en el ícono etiquetas en la parte superior derecha
de cualquiera de los paneles.

A medida que pasa el curso del ratón sobre elementos en los paneles *Maqueta HTML* o *Contenido sin tema*,
ya verás:

* Un esquema que muestra el elemento debajo del cursor.
* Un selector CSS o XPath en la barra de estado en la parte inferior si el panel que
  identificaría de forma única este elemento en una regla de Diazo.

Haga clic en un elemento o presione ``Enter`` mientras se desplaza sobre un elemento para
seleccionarlo. El elemento seleccionado más recientemente en cada panel se muestra en
abajo a la derecha de la barra de estado relevante.

Presione ``Esc`` mientras se desplaza sobre un elemento para seleccionar su elemento primario. Esto es
útil al tratar de seleccionar elementos contenedores "invisibles". Presione ``Enter``
para guardar esta selección.

El contenido de los paneles *Maquetas HTML* o (más comúnmente) *Contenido sin tema* 
se puede navegar, por ejemplo, para acceder a una página de contenido que requiere
reglas del tema, al deshabilitar al inspector. Use los interruptores de palanca en la parte inferior
derecho del panel correspondiente para habilitar o deshabilitar el selector.

El creador de reglas
--------------------

Haga clic en el botón *Construir regla* cerca de la parte superior del *Modificar tema* o 
*Inspeccionar tema* para iniciar un asistente interactivo de creación de reglas. Te harán 
preguntas qué tipo de regla construir y luego se le pide que seleccione los elementos 
relevantes en los paneles *Maquetas HTML* y/o *Contenido sin tema* según sea necesario. 
Por defecto, esto usará todas las selecciones guardadas, a menos que desmarques la casilla 
*Usar seleccionado elementos* en la primera página si el asistente.

Una vez que el asistente finalice, se le mostrará la regla generada. Puedes editar
esto si lo deseas. Si hace clic en *Insertar*, la nueva regla generada será
insertado en el editor ``rules.xml`` en o cerca de su posición actual del cursor.
Puede moverlo o editarlo más adelante como lo desee.

Haga clic en *Vista previa de tema* para obtener una vista previa del tema en una nueva 
pestaña o ventana. No lo olvides guardar el archivo ``rules.xml`` si ha realizado cambios.

*Nota*: en modo solo lectura, puede compilar reglas e inspeccionar la maqueta HTML y
tema, pero no cambiar el archivo ``rules.xml``. En este caso, el botón *Insertar*
del generador de reglas (ver a continuación) tampoco estará disponible.

*Nota:* La capacidad de insertar reglas desde el asistente de *Creador de reglas* 
no es disponible en Microsoft Internet Explorer, aunque se le dará la opción
para copiar la regla al portapapeles al usar este navegador.

Ajustes avanzados
-----------------

El panel de control *Temas* también contiene una pestaña llamada *Configuración avanzada*. 
Aquí están los dragones precaución.

La pestaña *Ajustes avanzadas* está dividida en dos áreas. El primero, *Detalles de Tema*, 
contiene la configuración subyacente que se modifica cuando se trata de un tema aplicado 
desde el panel de control *Temas*. Estos son:

* Si los temas de Diazo están habilitados o no.
* La ruta al archivo de reglas, convencionalmente llamado ``rules.xml``, ya sea
  relativa a la raíz del sitio Plone o como una ruta absoluta a un externo
  servidor.
* El prefijo que se aplica al convertir rutas relativas en los temas (por ejemplo, referencias a 
  imágenes en un atributo ``src`` de la etiqueta ``<img />``) en las absolutas en el tiempo de 
  renderizado.
* El ``DOCTYPE`` HTML para aplicar a la salida renderizada, si es diferente al predeterminado
  ``XHTML 1.0 Transitional``.
* Sea o no permitido a recursos del tema (como el archivo ``rules.xml``) ser leído o no
  por red. Deshabilitar esto proporciona un impulso de rendimiento modesto.
* Una lista de nombres de host para los cuales nunca se aplica un tema. Más comúnmente, esto
  contiene ``127.0.0.1``, lo que le permite ver un sitio no modificado a través de
  ``http://127.0.0.1:8080`` y un sitio tematizado en ``http://localhost:8080`` durante
  desarrollo, por ejemplo.
* Una lista de parámetros de tema y las expresiones TALES para generarlos
  (vea abajo).

El segundo, *Tema base*, controla la presentación del contenido sin modificar, y
aplicar incluso si no se aplica ningún tema de Diazo. Estas son las configuraciones que usaron
que se encuentra en el panel de control de *Temas* en versiones anteriores de Plone.

Referencia
==========

El resto de esta guía contiene materiales de referencia útiles para constructores
del tema.

Despliegue y prueba de temas
----------------------------

Para compilar y probar un tema, primero debe crear una maqueta HTML estática del
*look and feel* lo que quieres, y luego construye un archivo de reglas para describir 
cómo Plone mapas de contenido a los marcadores de posición en esta maqueta.

La maqueta se puede crear en cualquier lugar con la herramienta que te resulte más cómoda
crear páginas web. Para simplificar la integración con Plone, se recomienda
asegurarse de que utiliza enlaces relativos para los archivos de recursos como CSS, JavaScript e imagen, 
para que se visualice correctamente cuando se abra en un navegador web desde un archivo local. 
Plone convertirá estos enlaces relativos a las rutas absolutas apropiadas
automáticamente, asegurando que el tema funcione sin importar qué URL esté viendo el usuario
cuando el tema se aplica a un sitio Plone.

Hay varias maneras de llevar el tema dentro Plone:

1. En el sistema de archivos

Si usted usa un instalador o "buildout" estándar para instalar si sitio Plone,
usted debería tener un directorio llamado ``resources`` en el directorio raíz 
de su instalación Plone (este es creado usando la opción ``resources`` en la receta 
buildout ``plone.recipe.zope2instance``. Ver 
https://pypi.org/project/plone.recipe.zope2instance para más detalles.)

Usted puede buscar (o crear) un directorio ``theme`` dentro de este directorio, el cual
es usado para contener temas. Cada tema necesita su propio directorio con un único nombre.
Cree uno (ej. ``resources/theme/mytheme``) y coloque sus archivos HTML y cualquier 
referencia a recursos dentro de este directorio. Usted puede usar subdirectorios si usted 
lo desea, pero se recomienda mantener los archivos HTML del tema básico en la parte superior
del directorio de temas.

También necesitará un archivo de reglas llamado ``rules.xml`` dentro de este directorio. Si
usted no tiene aun uno, iniciar con un archivo vacío::

    <?xml version="1.0" encoding="UTF-8"?>
    <rules
        xmlns="http://namespaces.plone.org/diazo"
        xmlns:css="http://namespaces.plone.org/diazo/css"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform">`

        <theme href="theme.html" />
        <replace css:content-children="#content" css:theme-children="#main" />

    </rules>

Siempre que ejecute Zope en modo de depuración (por ejemplo, lo inicie con el comando
``bin/instance fg``), los cambios en el tema y las reglas deben tener efecto
inmediatamente. Puede obtener una vista previa o habilitar el tema a través del panel 
de control *Temas*, y luego modificar iterativamente el archivo ``rules.xml`` o la maqueta 
del tema como lo desees.

2. A través de la web

Si prefiere (o no tiene acceso al sistema de archivos), puede crear temas completamente
a través del panel de control de Plone, ya sea mediante la duplicación de un tema existente, o
comenzando desde cero con un tema casi vacío.

Vea las instrucciones sobre el uso del panel de control de arriba para más detalles.

Una vez que se ha creado un tema, puede modificarlo mediante el 
panel de control *Temas*. Ver arriba para más detalles.

3. Como un archivo zip

Los temas se pueden descargar de Plone como archivos Zip, el cual luego se pueden cargar
en otros sitios.

Vea las instrucciones sobre el uso del panel de control de arriba para más detalles.

De hecho, puede crear archivos zip de tema válidos comprimiendo un directorio del tema
en el sistema de archivos usando una herramienta de compresión estándar como *7-Zip* o
*Winzip* (para Windows) o la acción incorporada *Compress* en el buscador Mac OS X.
Solo asegúrate de comprimir exactamente una carpeta que contenga todos los archivos de tema
y el archivo ``rules.xml``. (No comprima el contenido de la carpeta
directamente: cuando se desempaqueta, el archivo zip debe producir exactamente una carpeta que
a su vez contiene todos los archivos relevantes).

4. En un paquete Python (solo programadores)

Si está creando un paquete de Python que contiene personalizaciones de Plone 
el cual usted intenta instalar en su sitio, puede dejar que registre un tema 
para la instalación en el sitio.

Para hacer esto, coloque un directorio llamado Ej. ``theme`` en la parte superior 
del paquete, junto al archivo de Zope ``configure.zcml``, y agregue una declaración
``<plone:static />`` al archivo ``configure.zcml``::

    <configure
        xmlns:plone="http://namespaces.plone.org/plone"
        xmlns="http://namespaces.zope.org/zope">

        ...

        <plone:static name="mytheme" directory="theme" type="theme" />

        ...

    </configure>

Observe la declaración del espacio de nombres ``plone`` en la raíz del
elemento `` <configure />``. Coloque los archivos del tema y el archivo ``rules.xml``
en el ``theme`` directorio.

Si su paquete tiene un perfil GenericSetup, puede habilitar automáticamente el
tema sobre la instalación de este perfil agregando un archivo ``theme.xml`` en el 
directorio ``profiles/default``, el cual contiene, por ejemplo, ::

    <theme>
        <name>mytheme</name>
        <enabled>true</enabled>
    </theme>

El archivo manifest
-------------------

Es posible proporcionar información adicional sobre un tema colocando un 
archivo llamado ``manifest.cfg`` junto al archivo ``rules.xml`` en la parte 
superior de un tema directorio.

Este archivo puede verse así::

    [theme]
    title = My theme
    description = A test theme

Como se muestra aquí, el archivo de manifiesto se puede utilizar para proporcionar un uso 
más amigable título y una descripción más larga para el tema, para usar en el panel de control.
Solo se requiere el encabezado ``[theme] ``; todas las demás teclas son opcionales.

También puedes configurar::

    rules = http://example.org/myrules.xml

Para utilizar un nombre de archivo de regla diferente a ``rules.xml`` (debe proporcionar 
una dirección URL o ruta relativa).

Para cambiar el prefijo de ruta absoluta (consulte *Configuración avanzada*), use::

    prefix = /some/prefix

Para emplear un ``DOCTYPE`` en el contenido del tema que no sea 
``XHTML 1.0 Transicional``, agregar, por ejemplo::

    doctype = <!DOCTYPE html>

Para proporcionar una vista previa fácil de usar de su tema en el panel de control *Temas*,
agregar::

    preview = preview.png

Aquí, ``preview.png`` es un archivo de imagen relativo a la ubicación del
archivo ``manifest.cfg``.

Las extensiones del motor de theming Diazo pueden agregar compatibilidad con bloques adicionales de
parámetros configurables.

Sintaxis de reglas
------------------

El siguiente es un breve resumen de la sintaxis de las reglas de Diazo. Ver
http://docs.diazo.org/en/latest/ para más detalles y más ejemplos.

Selectores
++++++++++

Cada regla está representada por una etiqueta XML que opera en uno o más HTML
elementos en el contenido y/o tema. Los elementos para operar están indicados
usando atributos de las reglas conocidas como *selectores*.

La forma más sencilla de seleccionar elementos es usar un selector de expresiones CSS, como
``css:content="#content"`` o ``css:theme="#main .content"``. Cualquier expresión válida CSS 3
(incluidos pseudo-selectores como ``:first-child`` puede ser usado).

Los selectores estándar, ``css:theme`` y ``css:content``, funcionan en el elemento(s)
que coinciden. Si desea operar los elementos hijos coincididos en lugar del elemento, use 
``css:theme-children="..."`` o ``css:content-children="..."`` en su lugar.

Si no puede construir una expresión CSS 3 adecuada, puede usar las expresiones 
XPath como ``content="/head/link"`` o ``theme="//div[@id='main']"`` 
(Tenga en cuenta la falta de un prefijo ``css:`` cuando se utilizan expresiones XPath). 
Los dos enfoques son equivalentes, y puedes mezclar y combinar libremente, pero no puedes
tener, por ejemplo, un atributo ``css:theme`` y un ``theme`` en una sola regla. Para
operar en elementos hijos de un nodo seleccionado con una expresión XPath, use
``theme-children="..."`` o ``content-children="..."``.

Puede obtener más información sobre XPath en la siguiente dirección 
URL https://www.w3schools.com/xml/xpath_intro.asp.

Condiciones
+++++++++++

Por defecto, cada regla se ejecuta, aunque las reglas no coinciden con ningún elemento
Por supuesto, no haremos nada. Puede hacer una regla, conjunto de reglas o referencia 
de tema (ver a continuación) condicional sobre un elemento que aparece en el contenido 
mediante la adición de un atributo a la regla como ``css:if-content="#some-element"`` 
(para usar un XPath) expresión en su lugar, suelte el prefijo ``css:``). Si ningún 
elemento coincide con el expresión, la regla es ignorada.

**Consejo:** si una regla ``<replace />`` coincide con un elemento en el tema, pero no 
en el contenido, el nodo del tema se descartará (se reemplazará por nada). Si lo haces
no quiere este comportamiento y no está seguro si el contenido contendrá el/los elemento(s) 
relevante(s), usted puede usar la regla condicional ``css:if-content``. Ya que este es un 
escenario común, hay un atajo: ``css:if-content=""`` significa "use la expresión del 
atributo ``css:content``".

Del mismo modo, puede construir una condición basada en la ruta de la actual
solicitud mediante el uso de un atributo como ``if-path="/news"`` (tenga en cuenta que no 
hay ``css:if-path`` ). Si la ruta comienza con una barra inclinada, coincidirá desde la 
raíz del sitio Plone. Si termina con una barra inclinada, coincidirá con el final de la URL.
Puede establecer una ruta absoluta utilizando una barra inicial y una barra posterior.

Finalmente, puede usar expresiones XPath arbitrarias contra cualquier variable definida
usando un atributo como ``if="$host = 'localhost'"`` . Por defecto, las variables
``url`` , ``scheme`` , ``host`` y ``base`` están disponibles, representando la
URL actual. Los temas pueden definir variables adicionales en sus archivos manifiestos.

Reglas disponibles
++++++++++++++++++

Los diversos tipos de reglas se resumen a continuación.

``rules``
#########

::

    <rules>
        ...
    </rules>

Envuelve un conjunto de reglas. Debe usarse como el elemento raíz del archivo de 
reglas. Anidado ``<rules />`` se puede usar con una *condición* para aplicar una 
sola condición a un conjunto de normas.

Cuando se usa como el elemento raíz del archivo de reglas, los diversos espacios 
de nombres XML deben declararse::

    <rules
        xmlns="http://namespaces.plone.org/diazo"
        xmlns:css="http://namespaces.plone.org/diazo/css"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
        ...
    </rules>

``theme`` y ``notheme``
#########################

::

    <theme href="theme.html" />
    <theme href="news.html" if-path="/news" />
    <notheme if="$host = 'admin.example.org'" />

Elija el archivo de tema que se utilizará. El ``href`` es una ruta relativa al archivo
de reglas. Si hay varios elementos ``<theme />``, se puede dar como máximo uno sin una 
condición el cual se usará el primer tema con una condición que sea true,
con el tema sin condición, si hay alguno, usado como una reserva.

``<notheme />`` se puede usar para especificar una condición bajo la cual ningún tema
debería ser usado. ``<notheme />`` tiene prioridad sobre ``<theme />``.

**Consejo:** Para asegurarte de que no hagas un estilo accidental de páginas que no 
sean Plone, agrega una condición como ``css:if-content="#visual-portal-wrapper"`` al 
último tema listado, y no tienen ningún temas sin condiciones.

``replace``
###########

::

    <replace
        css:content="#content"
        css:theme="#main"
        />

Replaces the matched element(s) in the theme with the matched element(s) from
the content.

``before`` y ``after``
########################

::

    <before
        css:content-children="#portal-column-one"
        css:theme-children="#portlets"
        />

    <after
        css:content-children="#portal-column-two"
        css:theme-children="#portlets"
        />

Inserta los elementos coincidentes del contenido antes o después de el/los elemento(s) 
coincidente en el tema. Al usar ``theme-children``, puede insertar el/los elemento(s) 
de contenido coincidente como primer (anterior) o último (anexo) elemento(s)
dentro del elemento(s) del tema coincidente.

``drop`` y ``strip``
######################

::

    <drop css:content=".documentByLine" />
    <drop theme="/head/link" />
    <drop css:theme="#content *" attributes="onclick onmouseup" />

    <strip css:content="#parent-fieldname-text" />

Eliminar elemento(s) del tema o contenido. Tenga en cuenta que a diferencia de la mayoría 
de las otras reglas, una regla ``<drop />`` o ``<strip />`` puede operar en el ``theme`` o
``content``, pero no ambos. ``<drop />`` elimina los elementos coincidentes y cualquier 
hijo, mientras que ``<strip />`` elimina el (los) elemento(s) coincidente(s), pero deja cualquier niño en el lugar.

``<drop />`` puede recibir una lista de ``attributes`` separada por espacios en blanco 
para soltar. En este caso, los elementos coincidentes no se eliminarán. Utilizar
``attributes="*"`` para soltar todos los atributos.

``merge`` y ``copy``
######################

::

    <merge
        attributes="class"
        css:content="body"
        css:theme="body"
        />

    <copy
        attributes="class"
        css:content="#content"
        css:theme="#main"
        />

Estas reglas operan en atributos. ``<merge />`` agregará el contenido del
atributo(s) nombrado(s) en el tema para el(los) valor(es) de cualquier atributo 
existente con el mismo nombre(s) en el contenido, separados por espacios en blanco. 
Se usa principalmente para fusionar clases de CSS.

``<copy />`` copiará los atributos de los elementos coincidentes en el contenido
a los elementos coincidentes en el tema, reemplazando por completo cualquier atributo 
con el mismo nombre que ya puede estar en el tema.

El atributo ``attributes`` puede contener una lista de elementos separados por espacios 
en blanco de atributos, o el valor especial ``*`` para operar en todos los atributos del
elemento que coincide.

Modificación avanzada
++++++++++++++++++++++

En lugar de seleccionar el markup para insertar en el tema desde el contenido, puede
colocar el marcado directamente en el archivo de reglas, como nodos secundarios del  elemento de la regla relevante::

    <after css:theme="head">
        <style type="text/css">
            body > h1 { color: red; }
        </style>
    </after>

Esto también funciona en el contenido, lo que le permite modificarlo sobre la marcha 
antes de que cualquier las reglas sea aplicada::

    <replace css:content="#portal-searchbox input.searchButton">
        <button type="submit">
            <img src="images/search.png" alt="Search" />
        </button>
    </replace>

Además de incluir HTML estático de esta manera, puede usar instrucciones XSLT
que operan en el contenido. Incluso puedes usar selectores ``css:`` directamente 
en el XSLT.::

    <replace css:theme="#details">
        <dl id="details">
            <xsl:for-each css:select="table#details > tr">
                <dt><xsl:copy-of select="td[1]/text()"/></dt>
                <dd><xsl:copy-of select="td[2]/node()"/></dd>
            </xsl:for-each>
        </dl>
    </replace>

Las reglas pueden operar en contenido que se obtiene de otro lugar que no sea el
la página actual está siendo renderizada por Plone, usando el atributo ``href`` 
para especificar una ruta de un recurso relativo a la raíz del sitio Plone::

    <!-- Pull in extra navigation from a browser view on the Plone site root -->
    <after
        css:theme-children="#leftnav"
        css:content=".navitem"
        href="/@@extra-nav"
        />

Parámetros del tema
-------------------

Es posible pasar parámetros arbitrarios a su tema, que puede ser referenciados 
como variables en expresiones XPath. Los parámetros se pueden configurar en el
panel de control de Temas de Plone, y puede ser importado de un archivo ``manifest.cfg``.

Por ejemplo, podría tener un parámetro ``mode`` que podría establecerse en
cadena ``live`` o ``test``. En tus reglas, podrías hacer algo como esto
para insertar una advertencia cuando estás en el servidor de prueba::

    <before css:theme-children="body" if="$mode = 'test'">
        <span class="warning">Warning: This is the test server</span>
    </before>

Incluso podría usar el valor del parámetro directamente, por ejemplo::

    <before css:theme-children="body">
        <span class="info">This is the <xsl:value-of select="$mode" /> server</span>
    </before>

Los siguientes parámetros están siempre disponibles para los temas Plone:

``scheme``
    La porción de esquema de la URL entrante, por lo general ``http`` o ``https``.
``host``
    El nombre de host en la URL entrante.
``path``
    El segmento de ruta de la URL entrante. Esto no incluirá ningún virtual
    tokens de alojamiento, es decir, es la ruta que ve el usuario final.
``base``
    La URL base de Zope (la variable de solicitud ``BASE1``).

Puede agregar parámetros adicionales a través del panel de control, usando expresiones 
TALES. Los parámetros se enumeran en la pestaña *Avanzado*, uno por línea, en la forma 
``<name> = <expression>``.

Por ejemplo, si desea evitar el tecleo de páginas cargadas por las superposiciones de 
Plone, puede hacer uso del parámetro de solicitud ``ajax_load`` que ellos definen. 
Su archivo de reglas puede incluir::

    <notheme if="$ajax_load" />

Para agregar este parámetro así como el parámetro ``mode`` descrito anteriormente,
podría agregar lo siguiente en el panel de control::

    ajax_load = python: request.form.get('ajax_load')
    mode = string: test

El lado derecho es una expresión TALES. *Debe* evaluar a una string, integer, float, 
boolean o ``None``: las listas, los diccionarios y los objetos no son soportado. 
``python:``, ``string:`` y las expresiones de ruta funcionan como lo hacen
en las Zope Page Templates.

Las siguientes variables están disponibles al construir estas expresiones TALES:

``context``
    El contexto de la solicitud actual, generalmente un objeto de contenido.
``request``
    La solicitud actual.
``portal``
    El objeto raíz del portal.
``context_state``
    La vista ``@@plone_context_state``, desde la cual puede buscar información adicional
    valores como la URL del contexto o la vista predeterminada.
``portal_state``
    La vista ``@@plone_portal_state``, que puede buscar adicionalmente
    valores como la URL raíz de navegación o si el usuario actual ha iniciado 
    sesión o no.

Ver el paquete ``plone.app.layout`` para detalles sobre el ``@@plone_context_state`` y
las vistas ``@@plone_portal_state``.

Los parámetros del tema suelen ser parte integral de un tema y, por lo tanto, se 
establecerán basado en el archivo manifiesto de un tema cuando un tema es importado 
o habilitado. Esto esta hecho usando la sección ``[theme:parameters]`` en el archivo 
``manifest.cfg``. por ejemplo::

    [theme]
    title = My theme
    description = A test theme

    [theme:parameters]
    ajax_load = python: request.form.get('ajax_load')
    mode = string: test

Depuración del tema
-------------------

Cuando Zope está en modo de desarrollo (por ejemplo, corriendo en primer plano en una 
consola con el comando ``bin/instance fg``), el tema se volverá a compilar en cada solicitud. En modo no desarrollo, se compila una vez cuando se accedió por primera vez, 
y luego solo se vuelve a compilar cuando los valores del panel de control han sido 
cambiados.

Además, en el modo de desarrollo, es posible desactivar temporalmente el tema
al agregar un parámetro de cadena de consulta ``diazo.off=1``. Por ejemplo::

    http://localhost:8080/Plone/some-page?diazo.off=1

Finalmente, puede obtener una superposición que contenga sus reglas, con anotaciones 
sobre cómo muchas veces las condiciones coincidían tanto con el tema como con el 
documento. El color verde significa que la condición coincide, el color rojo significa 
que no. Toda la etiqueta de la regla será de color verde (es decir, tuvo un efecto) 
siempre y cuando todas las condiciones dentro sean de color verdes.

Para habilitar esto, agregue ``diazo.debug=1``. Por ejemplo::

    http://localhost:8080/Plone/some-page?diazo.debug=1

El parámetro se ignora en modo no desarrollo.

Reglas comúnmente utilizadas
----------------------------

Las siguientes recetas ilustran reglas comúnmente usadas en la construcción de 
temas de Plone:

Para copiar el título de la página::

    <replace css:theme="title" css:content="title" />

Para copiar la etiqueta ``<base />`` (necesaria para que funcionen los enlaces de Plone)::

    <replace css:theme="base" css:content="base" />

Si no hay una etiqueta ``<base />`` en el tema, puede hacer:

    <before css:theme-children="head" css:content="base" />

Para eliminar todos los estilos y recursos de JavaScript del tema y copiarlos
de la herramienta ``portal_css`` de Plone en su lugar::

    <!-- Drop styles in the head - these are added back by including them from Plone -->
    <drop theme="/html/head/link" />
    <drop theme="/html/head/style" />

    <!-- Pull in Plone CSS -->
    <after theme-children="/html/head" content="/html/head/link | /html/head/style" />

Para copiar los recursos de JavaScript de Plone::

    <!-- Pull in Plone CSS -->
    <after theme-children="/html/head" content="/html/head/script" />

Para copiar la clase de la etiqueta ``<body />`` (necesaria para ciertas funciones y 
estilos de JavaScript en Plone así puedan funcionar correctamente)::

    <!-- Body -->
    <merge attributes="class" css:theme="body" css:content="body" />

Avanzado: usando portal_css para administrar su CSS
----------------------------------------------------

Los "registros de recursos" de Plone, incluida la herramienta ``portal_css``, 
se pueden usar para administrar hojas de estilo CSS Esto ofrece varias ventajas 
sobre simplemente vincular a sus hojas de estilo en la plantilla, como:

* Control detallado sobre el pedido de hojas de estilo
* Fusión de hojas de estilo para reducir la cantidad de descargas requeridas para 
  renderizar tu pagina
* Compresión de hojas de estilo sobre la marcha (por ejemplo, eliminación de espacio 
  en blanco)
* La capacidad de incluir o excluir una hoja de estilo basada en una expresión

Por lo general, es deseable (y en ocasiones completamente necesario) abandonar el
archivo del tema intacto, pero aún puede usar la herramienta ``portal_css`` para 
administrar su hojas de estilo. El truco es:

* Registre los estilos de su tema con la herramienta ``portal_css`` de Plone (esto es
  normalmente se hace mejor cuando envías un tema en un paquete de Python: hay
  Actualmente no hay forma de automatizar esto para un tema importado de un archivo Zip o
  creado a través de la web)
* Suelta los estilos del tema con una regla, y luego
* Incluye todos los estilos de Plone

Por ejemplo, puede agregar las siguientes reglas::

    <drop theme="/html/head/link" />
    <drop theme="/html/head/style" />

    <!-- Pull in Plone CSS -->
    <after theme-children="/html/head" content="/html/head/link | /html/head/style" />

El uso de una expresión "or" para el contenido en la regla ``<after />`` significa
que se mantiene el orden relativo de los elementos de enlace y estilo.

Para registrar hojas de estilo en la instalación del producto usando GenericSetup, use 
el import step ``cssregistry.xml`` en tu directorio GenericSetup ``profiles/default``::

    <?xml version="1.0"?>
    <object name="portal_css">

     <!-- Set conditions on stylesheets we don't want to pull in -->
     <stylesheet
         expression="not:request/HTTP_X_THEME_ENABLED | nothing"
         id="public.css"
         />

     <!-- Add new stylesheets -->
     <stylesheet title="" authenticated="False" cacheable="True"
        compression="safe" conditionalcomment="" cookable="True" enabled="on"
        expression="request/HTTP_X_THEME_ENABLED | nothing"
        id="++theme++my.theme/css/styles.css" media="" rel="stylesheet"
        rendering="link"
        applyPrefix="True"
        />

    </object>

Sin embargo, hay una advertencia importante. Su hoja de estilo puede incluir 
referencias a URL relativa de la siguiente forma:

    background-image: url(../images/bg.jpg);

Si su hoja de estilo vive en un directorio de recursos (por ejemplo, está registrado 
en la herramienta ``portal_css`` con el id ``++theme++my.theme/css/styles.css``, esto
funcionará bien siempre que el registro (y Zope) esté en modo de depuración. La URL 
relativa será resuelta por el navegador a 
``++theme++my.theme/images/bg.jpg``.

Sin embargo, puede descubrir que la URL relativa se rompe cuando se coloca el registro
en modo de producción. Esto se debe a que la fusión de recursos también cambia la URL
de la hoja de estilo para ser algo así como::

    /plone-site/portal_css/Suburst+Theme/merged-cachekey-1234.css

Para corregir esto, debe establecer el indicador ``applyPrefix`` en ``true`` cuando
instalando su recurso CSS usando el archivo ``cssregistry.xml``. Hay un
flag correspondiente en la interfaz de usuario del ``portal_css``.

A veces es útil mostrar algunos de los CSS de Plone en el sitio con estilo. Tú
puede lograr esto usando una regla Diazo ``<after />`` o similar para copiar
CSS de Plone generado ``<head />`` en el tema. Puedes usar la herramienta
``portal_css`` para desactivar las hojas de estilo que no desea.

Sin embargo, si también desea que el sitio se pueda usar en modo no temático 
(por ejemplo, en un URL separada), es posible que desee tener un conjunto de 
estilos más grande habilitado cuando Diazo no se usa. Para hacerlo más fácil, 
puede usar las siguientes expresiones como condiciones en la herramienta ``portal_css`` 
(y la herramienta ``portal_javascripts`` si es relevante), en la herramienta 
``portal_actions``, en las page templates y en otros lugares que usan sintaxis de 
expresiones TAL::

    request/HTTP_X_THEME_ENABLED | nothing

Esta expresión devolverá True si Diazo está actualmente habilitado, en cuyo caso
se establecerá un encabezado HTTP "X-Theme-Enabled".

Si más adelante despliega el tema en un servidor web frontal, como nginx, puede
establezca el mismo encabezado de solicitud allí para obtener el mismo efecto, 
incluso si ``plone.app.theming`` está desinstalado.

Utilizar::

    not: request/HTTP_X_THEME_ENABLED | nothing

para "ocultar" una hoja de estilo del sitio tematizado.
