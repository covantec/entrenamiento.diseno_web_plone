.. -*- coding: utf-8 -*-

.. _creando_temas_diazo:

=======================
Creando temas con diazo
=======================

.. sidebar:: Sobre este artículo

    :Autor(es): Leonardo J. Caballero G.
    :Correo(s): leonardoc@plone.org
    :Compatible con: Plone 3.x, plone 4.x
    :Fecha: 11 de Enero de 2021


¿Qué es diazo?
==============

Es una tecnología para hacer temas / apariencias HTML, aplicando un estilo 
consistente a aplicaciones y los archivos estáticos, independientemente de 
cómo se implementan, y separando de todo el sitio de estilo del nivel de 
aplicación de plantillas, (Ver Figura 1.1).

Características
--------------- 

* Es la nueva manera de crear temas para el Plone.

* Permite aplicar cualquier HTML en Plone.
 
* Es un motor de temas con XSLT.


¿Como funciona?
===============

.. figure:: _static/diazo-concepto.png
  :align: center
  :alt: Funcionamiento de diazo

  Funcionamiento de diazo.

* Con un simple XML usted substituye elementos de su ``template`` HTML por 
  contenido generado dinámicamente por el Plone.

* Este concepto se basa en la técnica de programación `Screen scraping`_.

* Se implementa de forma sencilla en Plone usando el motor de temas `diazo`_ 
  y el producto `plone.app.theming`_.

A continuación se ilustrar el funcionamiento de la tecnología ``diazo``, (Ver Figura 1.1).


El producto plone.app.theming
-----------------------------

Con el producto **Soporte de temas Diazo**, te deja crear un paquete de Plone
al cual le agregas un tema con sus archivos `HTML`_, `CSS`_, `JS`_, etc. Un archivo 
de reglas ``diazo`` basado en XML :file:`rules.xml`, un archivo de :file:`manifest.cfg`, 
para darle el nombre al tema y si quieres que se active al instalarse, ademas de las 
direcciones URLs para los recursos que quieras que se activa / desactivar.

Lo mas interesante es que una vez instalado le deja subir temas usando archivos 
comprimidos en formato **.zip** que incluyen las reglas, el manifiesto, etc y todo 
los demás recursos de su ``template`` vía Web.


Diferencias entre plone.app.theming y diazo
-------------------------------------------

Con el producto ``plone.app.theming`` funciona dentro del sistema de Plone, aunque 
puede leer temas fuera del sitio las reglas tienen que estar en Plone. Los temas 
para ``diazo`` funcionan con ``plone.app.theming`` y viceversa; básicamente,  
``plone.app.theming`` es una implementación de ``diazo``.


Diferencias entre Deliverance y diazo
-------------------------------------

El paquete ``diazo`` es una implementación de `Deliverance`_ basada en `XSLT`_ funcionan 
casi de la misma manera, en un principio su implementación para Plone era el
producto `collective.xdv`_, hasta que llegó ``plone.app.theming``.

El servicio ``diazo`` puede ser usado como `Deliverance`_ al colocarse como una parte 
en la configuración `buildout`_ y configurar este como proxy a otros sitios independientes 
de Plone.


Instalación
===========

En Plone se integra ``diazo`` a través del paquete ``plone.app.theming`` el 
cual esta incorporado por defecto en Plone 4.2 y versiones superiores.

Para instalar el paquete ``plone.app.theming`` dentro de su sitio Plone, 
entonces valla al panel de control de los **Complementos** en 
:menuselection:`Configuración de sitio --> Complementos` como un usuario 
Administrador Plone, y marque la casilla del producto **Soporte de temas Diazo** 
y haga clic en el botón ``Activar``.

Ahora usted usted puede acceder al panel de control para los **Temas** en 
:menuselection:`Configuración de sitio --> Temas`.


Estructura básica de un tema
============================

* Un tema es un simple archivo **.zip** conteniendo una carpeta con al menos 
  dos (02) archivos, como se describe en la siguiente estructura de directorios:

  .. code-block:: sh

      tema-diazo/
      |-- index.html
      `-- rules.xml

* Normalmente, el paquete es más complejo, conteniendo muchos los archivos CSS, 
  Javascripts e imágenes.

  .. note:: 
      
      *Diazo y PloneTheme*

      Adicionalmente puede empaquetar este tema diazo en un :term:`paquete Egg`, 
      para que incorpore comportamientos dinámicos de archivos ZPT, módulos Python, etc.


Crear una carpeta
-----------------

Crear una carpeta con el nombre de su tema. En esta carpeta irá a guardar 
los archivos de su tema:

.. code-block:: sh

    $ mkdir NOMBRE-TEMA
    
.. warinig:

    Donde **NOMBRE-TEMA** es el nombre de paquete de su tema.


Creando el archivo manifest.cfg
...............................

Puede crear el archivo :file:`manifest.cfg` con los siguientes comando:

.. code-block:: sh

    $ cd NOMBRE-TEMA ; nano manifest.cfg

Debe tener la siguiente sintaxis:

.. code-block:: cfg

    [theme]
    title = Mi primer tema diazo
    description = Mi primer tema diazo para Plone
    preview = preview.png

    [theme:parameters]
    portal_url = portal_state/portal_url


Creando el archivo index.html
.............................

Puede crear el archivo :file:`index.html`, este debe agregarse en el
mismo directorio del archivo :file:`manifest.cfg` con los siguientes
comando:

.. code-block:: sh

    $ nano index.html

Debe al menos tener la siguiente estructura HTML:

.. code-block:: html

    <html>
      <head>
       <title>Mi primer tema diazo</title> 
      </head>
      <body>
       <h1 id="titulo">Mi primer tema diazo</h1>
       <div id="menu">menú del sitio</div>
       <div id="contenido">Lorem ipsum... </div>
      </body> 
    </html>


Creando el archivo rules.xml
............................

Puede crear el archivo :file:`rules.xml`, este debe agregarse en el
mismo directorio del archivo :file:`index.html` con el siguiente comando:

.. code-block:: sh

    $ nano rules.xml

Debe crear al menos la siguiente estructura XML:

.. code-block:: xml

    <?xml version="1.0" encoding="UTF-8"?>

    <rules
        xmlns="http://namespaces.plone.org/diazo"
        xmlns:css="http://namespaces.plone.org/diazo/css"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    
        <theme href="index.html" css:if-content="#visual-portal-wrapper" />
        <replace css:content="#portal-globalnav" css:theme="#menu" />
        <replace css:content="#portal-columns" css:theme="#contenido" />
    
    </rules>

**Define cual template va a utilizar**:

Usted puede establecer cual plantilla HTML usara para este tema con la 
siguiente directiva ``diazo``:

.. code-block:: xml

    <theme href="index.html" css:if-content="#visual-portal-wrapper" />

.. seealso::

  - Referencia completa de la directiva de la regla `<theme /> <http://docs.diazo.org/en/latest/basic.html#theme>`_.

**Adiciona la navegación de Plone**:

Usted puede importar estructura de la navegación de Plone con la siguiente 
directiva ``diazo``:

.. code-block:: xml

    <replace css:content="#portal-globalnav" css:theme="#menu" />

.. seealso::

  - Referencia completa de la directiva de la regla `<replace /> <http://docs.diazo.org/en/latest/basic.html#replace>`_.

**Adiciona el Contenido**:

Usted puede agregar el contenido del sitio Plone con la siguiente 
directiva ``diazo``:

.. code-block:: xml

    <replace css:content="#portal-columns" css:theme="#contenido" />

.. seealso::

  - Referencia completa de la directiva de la regla `<replace /> <http://docs.diazo.org/en/latest/basic.html#replace>`_.


Colocando en práctica
=====================

Para probar el paquete tema ``diazo`` que lleva hecho hasta ahora puede
seguir los siguientes pasos:

#. Crear un archivo **zip** con su carpeta del nivel superior del tema.

#. Agregue al sitio Plone

#. :menuselection:`Configuración del sitio --> Temas`.

#. Entonces para subir el archivo haga clic en el botón ``Subir archivo Zip``.

   .. warning::
       Es importante destacar que el tema ``diazo`` de este previamente cargado 
       no son modificado a través de la Web por medidas de seguridad.

   .. tip::
       Si desea modificar el tema ``diazo`` debe *copiar*, el tema previamente 
       cargado haciendo clic en el botón **Copiar** le mostrara un mensaje emergente 
       para agregar un *Título* y *Descripción* diferente al que cargo previamente y 
       hace clic en el botón **Crear**.
   
Seguidamente al cargar el tema ``diazo``, le mostrará el editor de código fuente, 
(Ver Figura 1.2).

.. figure:: _static/theming-controlpanel-mapper.png
  :align: center
  :alt: Modificar tema creado

  Modificar tema creado

Después de aplicar el tema, usted debe tener el código HTML, con el menú y el
contenido de Plone, sin embargo, los estilos no se aplican Plone, (Ver Figura 1.3).

.. figure:: _static/tema-diazo-plone0.png
  :align: center
  :alt: Tema aplicado al Plone sin estilos

  Tema aplicado al Plone sin estilos


Agregando los estilos
=====================

**Importando el CSS de Plone**:

Usted puede re-usar los estilos CSS de Plone con la siguiente directiva ``diazo``:

.. code-block:: xml

    <replace css:content="head" css:theme="head" />

Esta llamada substituye todo el elemento ``<head />`` de su HTML por el elemento 
``<head />`` de Plone, (Ver Figura 1.4).

.. figure:: _static/tema-diazo-plone1.png
  :align: center
  :alt: Importando el CSS de Plone

  Importando el CSS de Plone


Reglas diazo
============

A continuación se describen algunas las reglas ``diazo`` mas comunes.


La regla <replace />
--------------------

A continuación el siguiente ejemplo:

.. code-block:: xml

    <replace css:theme="title" css:content="title"/>

El resultado aquí es que el elemento ``<title />`` en el tema será substituido 
por el elemento ``<title />`` del  contenido (dinámico), (Ver Figura 1.5).

.. figure:: _static/tema-diazo-plone2.png
  :align: center
  :alt: Remplaza el <title /> del tema por el <title /> del contenido

  Remplaza el <title /> del tema por el <title /> del contenido

.. seealso::

  - Referencia completa de la directiva de la regla `<replace /> <http://docs.diazo.org/en/latest/basic.html#replace>`_.


La regla <before /> y <after />
-------------------------------

A continuación el siguiente ejemplo:

.. code-block:: xml

    <after css:content="#portal-searchbox" css:theme="#contenido" />

Este ejemplo colocara la búsqueda de Plone al final de la página, (Ver Figura 1.6).

.. figure:: _static/tema-diazo-plone3.png
  :align: center
  :alt: Agregar el cuadro de búsqueda de Plone al final de la página

  Agregar el cuadro de búsqueda de Plone al final de la página.

.. seealso::

  - Referencia completa de la directiva de las reglas `<before /> y <after /> <http://docs.diazo.org/en/latest/basic.html#before-and-after>`_.


La regla <drop />
-----------------

A continuación el siguiente ejemplo:

.. code-block:: xml

    <drop css:content="#portal-searchbox .searchSection" />

Se utiliza para eliminar los elementos del tema o del contenido 
que no se utilizan.

El ejemplo anterior se eliminará el mensaje *"Sólo en esta sección"* que 
viene con la búsqueda de Plone.

.. seealso::

  - Referencia completa de la directiva de las reglas `<drop /> <http://docs.diazo.org/en/latest/basic.html#drop>`_.


La regla <merge />
------------------

A continuación el siguiente ejemplo:

.. code-block:: xml

    <merge attributes="class" css:theme="body" css:content="body" />

Se utiliza para combinar los valores de atributos, especialmente usado para 
combinar las clases CSS.

* Si el tema tiene en su etiqueta ``body`` de esta manera:

  .. code-block:: xml

      <body class="alpha beta">

* Y el contenido posee una etiqueta ``body`` como:

  .. code-block:: xml

      <body class="delta gamma">

* El resultado del ejemplo anteriormente seria:

  .. code-block:: xml

        <body class="alpha beta delta gamma">

.. seealso::

  - Referencia completa de la directiva de las reglas `<merge /> <http://docs.diazo.org/en/latest/basic.html#merge>`_.


Orden de ejecución de reglas
----------------------------

En la mayoría de los casos, usted no debe preocuparse demasiado sobre el funcionamiento
interno del compilador ``diazo``. Sin embargo, a veces puede ser útil para entender el
orden en que se aplican las reglas, en este caso el compilador ``diazo`` ejecutará las
reglas según un orden propio y no necesariamente en el orden escrito. No hay necesidad
de decorar, pero es bueno que sea señalado:

#. En **primer lugar** siempre se ejecutan las reglas ``<before>`` usando el atributo ``theme``
   (pero no usando el atributo ``theme-children``).

#. En **segundo lugar** seguidamente se ejecutan las reglas ``<drop />``.

#. En **tercer lugar** seguidamente se ejecutan las reglas ``<replace />`` usando el atributo
   ``theme`` (pero no usando el atributo ``theme-children``), siempre que regla ``<drop />``
   no se aplica al mismo nodo del tema o se utilizó el ``method="raw"``.

#. En **cuarto lugar** seguidamente se ejecutan las reglas ``<strip />``. Tenga en cuenta que las
   reglas ``<strip />`` no impiden que otras reglas se ponga en marcha, incluso si el nodo de
   contenido o el tema va a ser quitado.

#. En **quinto lugar** seguidamente se ejecutan las reglas que usan los ``attributes``.

#. En **sexto lugar** se ejecutara próximamente las reglas ``<before />``, ``<replace />`` y
   ``<after />`` usando el atributo ``theme-children``, siempre no allá reglas ``<replace />``
   usando en el tema que fue se aplicó al mismo nodo previamente del tema.

#. En **séptimo lugar** se ejecutara por último las reglas ``<before />`` usando el atributo
   ``theme`` (pero no usando el atributo ``theme-children``).


Descarga código fuente
======================

Usted puede obtener el código fuente de este ejemplo, para esto ejecute el siguiente comando:

.. code-block:: sh

  $ git clone https://github.com/Covantec/tema-diazo.git


Tema mas completo
=================

Usted podrá encontrar dos (02) ejemplos de tema más completo en las siguientes direcciones:

- https://pypi.org/project/plonetheme.onegov

- https://pypi.org/project/beyondskins.responsive

Más ejemplos consulte el índice de paquetes Python en búsqueda de `temas basados en diazo`_.

Referencias
===========

- `diazo documentation`_.

- `Construindo temas para Plone com Diazo`_ por la empresa `Simples Consultoria`_.

.. _`HTML`: https://es.wikipedia.org/wiki/HTML
.. _`CSS`: https://es.wikipedia.org/wiki/CSS
.. _`JS`: https://es.wikipedia.org/wiki/JS
.. _`diazo`: https://pypi.org/project/diazo
.. _`Screen scraping`: https://es.wikipedia.org/wiki/Screen_scraping
.. _`Deliverance`: http://www.coactivate.org/projects/ploneve/crear-temas-con-deliverance-y-banjo
.. _`XSLT`: https://es.wikipedia.org/wiki/XSLT
.. _`collective.xdv`: https://pypi.org/project/collective.xdv
.. _`buildout`: https://plone-spanish-docs.readthedocs.io/es/latest/buildout/replicacion_proyectos_python.html
.. _`plone.app.theming`: https://pypi.org/project/plone.app.theming
.. _`temas basados en diazo`: https://pypi.org/search/?q=diazo+theme
.. _`diazo documentation`: http://docs.diazo.org/en/latest/index.html
.. _`Construindo temas para Plone com Diazo`: https://www.slideshare.net/simplesconsultoria/constuindo-temas-para-plone-com-diazo
.. _`Simples Consultoria`: http://www.simplesconsultoria.com.br/
