.. -*- coding: utf-8 -*-

.. _diazo_instroduccion:

Descripción general
===================

En sus inicios fue conocido como `XDV`_ hasta que fue renombrado como `diazo`_. 
Este implementa un lenguaje como `Deliverance`_ usando un puro motor `XSLT`_. 
Con ``diazo``, usted *"compila"* el tema y el conjunto de reglas (``ruleset``) 
en un solo paso, entonces usa una súper / simple transformación en cada solicitud 
adicional. Por otra parte, usted puede compilar el tema durante el desarrollo, 
protegerlo dentro de un repositorio de control de versiones Subversion o Git, 
y no tocar ``diazo`` durante la implementación.

Este le permite aplicar un tema incluido en una página Web `HTML`_ estática a 
un sitio Web creado de manera dinámica utilizando cualquier tecnología del lado 
del servidor. Con ``diazo``, usted puede tomar una diagramación de un diseño ``HTML`` 
creado por un diseñador de páginas Web y convertirlo en un tema para su favorito 
de la CMS, el rediseño de la interfaz de usuario de una aplicación Web operativa 
sin ni siquiera tener acceso al código fuente original, o crear una experiencia 
de usuario unificada a través de múltiples sistemas dispares, todo en cuestión 
de horas, no semanas, (Ver Figura 1.1).

.. figure:: _static/diazo-concepto.png
  :align: center
  :width: 45%
  :alt: Funcionamiento de diazo

  Funcionamiento de diazo.

Cuando se utiliza ``diazo``, tendrá que trabajar con la sintaxis y conceptos 
familiares al trabajar con ``HTML`` y `CSS`_. Y por lo que le permite integrar 
sin problemas los archivos ``XSLT`` en su regla, ``diazo`` hace que en los casos 
más simple y complejos comunes las exigencias sean posibles.

.. note::
    ``diazo`` aplica la misma filosofía de :ref:`Web scraping <diazo_webscraping>` 
    para aplicar estilos a temas Web.


.. _diazo_webscraping:

Sobre el Web scraping
---------------------

También es llamado *Web data extraction*, es una técnica de software de computador 
de extracción de información desde sitios Web.

.. tip::
    La traducción aproximada de *Web scraping* es raspado de la Web.

Los scripts y aplicaciones **Web scraping** simulan una persona viendo un sitio Web 
con un navegador. Generalmente procesa el HTML de un sitio Web para extraer datos 
para manipularlos, por ejemplo, para convertir esa página Web a otro formato. 

**Web scraping** es el proceso de colección automáticamente de información desde la 
*world wide web*. Esto consiste en tomar una presentación de una información (normalmente 
texto, aunque puede incluir información gráfica) para, mediante ingeniería inversa, 
extraer los datos que dieron lugar a esa presentación. Por ejemplo, Extraer de la 
página Web de un diario el tiempo meteorológico previsto.

En general, hay que destacar que los sistemas de los que se extrae la información 
no están diseñados para extraer dicha información (en algunos casos, es al contrario, 
como en los sistemas de captcha).

.. _`diazo`: https://pypi.org/project/diazo
.. _`Deliverance`: https://pypi.org/project/Deliverance
.. _`XSLT`: https://es.wikipedia.org/wiki/XSLT
.. _`XDV`: https://pypi.org/search/?q=XDV+plone&o=
.. _`HTML`: https://es.wikipedia.org/wiki/HTML
.. _`CSS`: https://es.wikipedia.org/wiki/Hojas_de_estilo_en_cascada
.. _`diazo.org`: http://docs.diazo.org/en/latest/
