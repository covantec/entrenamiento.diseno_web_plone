.. -*- coding: utf-8 -*-

Motor del tema Diazo
====================

Es una herramienta para hacer temas / apariencias HTML, aplicando un estilo consistente 
a aplicaciones y los archivos estáticos, independientemente de cómo se implementan, y
separando de todo el sitio de estilo del nivel de aplicación de plantillas.

.. toctree::
   :maxdepth: 2

   introduccion
   instalacion
   usando
   faq
   xpath/index
   xslt/index
   ..
        configuracion
