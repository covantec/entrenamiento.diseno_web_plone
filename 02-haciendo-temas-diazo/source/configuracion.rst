.. -*- coding: utf-8 -*-

.. _diazo_configuracion:

Configuración
=============

Para aplicar las reglas con ``diazo`` debemos hacerlo en el 
archivo :file:`rules.xml` y esto consiste en configurar el 
puerto que servirá como fuente de contenido, y el puerto 
donde se verán los cambios y el efecto de las reglas de 
``diazo``. Para aplicar las reglas hay que seleccionar la 
ruta y crear una clase.

Para configurar diazo con Plone
-------------------------------

El archivo de reglas debe ir así:

.. code-block:: xml

    <?xml version="1.0" encoding="UTF-8"?>
    <rules
        xmlns="http://namespaces.plone.org/diazo"
        xmlns:css="http://namespaces.plone.org/diazo/css"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns:xi="http://www.w3.org/2001/XInclude">

        <theme href="theme/index.html"/>

    </rules>
