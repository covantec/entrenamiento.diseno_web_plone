.. -*- coding: utf-8 -*-

Elementos XSLT
==============

La tabla siguiente proporciona un breve resumen de todos los elementos
``XSLT``.

Los elementos
-------------

+----------------+-------------------+
|    Elemento    |    Descripción    |
+----------------+-------------------+
|    BBBB        |    AAAA           |
+----------------+-------------------+

* `xsl:apply-imports`_, Invoca una regla de plantilla invalidada.

* `xsl:apply-templates`_, Dirige el procesador ``XSLT`` para que busque la
  plantilla adecuada que se debe aplicar, según el tipo y el contexto
  del nodo seleccionado.

* `xsl:attribute`_, Crea un nodo de atributo y lo adjunta a un elemento
  resultante.

* `xsl:attribute-set`_, Define un conjunto de atributos con nombre.

* `xsl:call-template`_, Invoca una plantilla por nombre.

* `xsl:choose`_, Proporciona múltiples pruebas condicionales junto con los
  elementos ``<xsl:otherwise>`` y ``<xsl:when>``.

* `xsl:comment`_, Genera un comentario en el resultado.

* `xsl:copy`_, Copia el nodo actual del origen al resultado.

* `xsl:copy-of`_, Inserta subárboles y fragmentos del árbol de resultados en
  el árbol de resultados.

* `xsl:decimal-format`_, Declara un formato-digital, que controla la interpretación
  de un modelo de formato utilizado por la función ``format-number``.

* `xsl:element`_, Crea un elemento con el nombre especificado en el resultado.

* `xsl:fallback`_, Llama al contenido de la plantilla que puede proporcionar un
  sustituto razonable al comportamiento del nuevo elemento cuando se encuentre.

* `xsl:for-each`_, Aplica una plantilla repetidamente, aplicándola a su vez en cada
  nodo de un conjunto.

* `xsl:if`_, Permite obtener fragmentos de plantillas condicionales simples.

* `xsl:import`_, Importa otro archivo ``XSLT``.

* `xsl:include`_, Incluye otro archivo ``XSLT``.

* `xsl:key`_, Declara una clave para utilizar con la función ``key()`` en expresiones
  de lenguaje de ruta XML (XPath).

* `xsl:message`_, Envía un mensaje de texto al búfer del mensaje o al cuadro de diálogo
  del mensaje.

* `xsl:namespace-alias`_, Sustituye el prefijo relacionado con un espacio de nombres
  dado por otro prefijo.

* `xsl:number`_, Inserta un número con formato en el árbol de resultados.

* `xsl:otherwise`_, Proporciona múltiples pruebas condicionales junto con los elementos
  ``<xsl:choose>`` y ``<xsl:when>``.

* `xsl:output`_, Especifica las opciones que se deben utilizar a la hora de serializar
  el árbol de resultados.

* `xsl:param`_, Declara un parámetro con nombre que se puede utilizar dentro de un
  elemento ``<xsl:stylesheet>`` o un elemento ``<xsl:template>``. Permite especificar
  un valor predeterminado.

* `xsl:preserve-space`_, Conserva los espacios en blanco en un documento.

* `xsl:processing-instruction`_, Genera una instrucción de proceso en el resultado.

* `msxsl:script`_\*, Define variables y funciones globales para extensiones de scripts.

* `xsl:sort`_, Especifica los criterios de ordenación para las listas de nodos seleccionadas
  por ``<xsl:for-each>`` o ``<xsl:apply-templates>``.

* `xsl:strip-space`_, Elimina espacios en blanco en un documento.

* `xsl:stylesheet`_, Especifica el elemento de documento en un archivo ``XSLT``. El elemento
  de documento contiene otros elementos ``XSLT``.

* `xsl:template`_, Define una plantilla reutilizable para generar los resultados deseados
  para los nodos de un tipo y contexto en particular.

* `xsl:text`_, Genera texto en el resultado.

* `xsl:transform`_, Lleva a cabo la misma función que ``<xsl:stylesheet>``.

* `xsl:value-of`_, Inserta el valor del nodo seleccionado como texto.

* `xsl:variable`_, Especifica un valor enlazado de una expresión.

* `xsl:when`_, Proporciona múltiples pruebas condicionales junto con los elementos ``<xsl:choose>``
  y ``<xsl:otherwise>``.

* `xsl:with-param`_, Pasa un parámetro a una plantilla.

**\* Denota una extensión de propiedad de Microsoft para compatibilidad con el script.**

----

Referencia
----------

- `Función format-number <https://docs.microsoft.com/es-es/previous-versions/bb399689(v=vs.110)?redirectedfrom=MSDN>`_.

- `Elemento <xsl:apply-templates> <https://docs.microsoft.com/es-es/previous-versions/bb399623(v=vs.110)?redirectedfrom=MSDN>`_.

- `Elemento <xsl:choose> <https://docs.microsoft.com/es-es/previous-versions/bb399698(v=vs.110)?redirectedfrom=MSDN>`_.

- `Elemento <xsl:for-each> <https://docs.microsoft.com/es-es/previous-versions/bb399633(v=vs.110)?redirectedfrom=MSDN>`_.

- `Elemento <xsl:otherwise> <https://docs.microsoft.com/es-es/previous-versions/bb399692(v=vs.110)?redirectedfrom=MSDN>`_.

- `Elemento <xsl:stylesheet> <https://docs.microsoft.com/es-es/previous-versions/bb399660(v=vs.110)?redirectedfrom=MSDN>`_.

- `Elemento <xsl:template> <https://docs.microsoft.com/es-es/previous-versions/bb399472(v=vs.110)?redirectedfrom=MSDN>`_.

- `Elemento <xsl:when> <https://docs.microsoft.com/es-es/previous-versions/bb399667(v=vs.110)?redirectedfrom=MSDN>`_


.. _`xsl:apply-imports`: https://docs.microsoft.com/es-es/previous-versions/ms256178(v=vs.110)?redirectedfrom=MSDN
.. _`xsl:apply-templates`: https://docs.microsoft.com/es-es/previous-versions/ms256184(v=vs.110)?redirectedfrom=MSDN
.. _`xsl:attribute`: https://docs.microsoft.com/es-es/previous-versions/ms256165(v=vs.110)?redirectedfrom=MSDN
.. _`xsl:attribute-set`: https://docs.microsoft.com/es-es/previous-versions/ms256163(v=vs.110)?redirectedfrom=MSDN
.. _`xsl:call-template`: https://docs.microsoft.com/es-es/previous-versions/ms256487(v=vs.110)?redirectedfrom=MSDN
.. _`xsl:choose`: https://docs.microsoft.com/es-es/previous-versions/ms256169(v=vs.110)?redirectedfrom=MSDN
.. _`xsl:comment`: https://docs.microsoft.com/es-es/previous-versions/ms256145(v=vs.110)?redirectedfrom=MSDN
.. _`xsl:copy`: https://docs.microsoft.com/es-es/previous-versions/ms256128(v=vs.110)?redirectedfrom=MSDN
.. _`xsl:copy-of`: https://docs.microsoft.com/es-es/previous-versions/ms256183(v=vs.110)?redirectedfrom=MSDN
.. _`xsl:decimal-format`: https://docs.microsoft.com/es-es/previous-versions/ms256092(v=vs.110)?redirectedfrom=MSDN
.. _`xsl:element`: https://docs.microsoft.com/es-es/previous-versions/ms256047(v=vs.110)?redirectedfrom=MSDN
.. _`xsl:fallback`: https://docs.microsoft.com/es-es/previous-versions/ms256234(v=vs.110)?redirectedfrom=MSDN
.. _`xsl:for-each`: https://docs.microsoft.com/es-es/previous-versions/ms256166(v=vs.110)?redirectedfrom=MSDN
.. _`xsl:if`: https://docs.microsoft.com/es-es/previous-versions/ms256209(v=vs.110)?redirectedfrom=MSDN
.. _`xsl:import`: https://docs.microsoft.com/es-es/previous-versions/ms256126(v=vs.110)?redirectedfrom=MSDN
.. _`xsl:include`: https://docs.microsoft.com/es-es/previous-versions/ms256094(v=vs.110)?redirectedfrom=MSDN
.. _`xsl:key`: https://docs.microsoft.com/es-es/previous-versions/ms256203(v=vs.110)?redirectedfrom=MSDN
.. _`xsl:message`: https://docs.microsoft.com/es-es/previous-versions/ms256441(v=vs.110)?redirectedfrom=MSDN
.. _`xsl:namespace-alias`: https://docs.microsoft.com/es-es/previous-versions/ms256448(v=vs.110)?redirectedfrom=MSDN
.. _`xsl:number`: https://docs.microsoft.com/es-es/previous-versions/ms256084(v=vs.110)?redirectedfrom=MSDN
.. _`xsl:otherwise`: https://docs.microsoft.com/es-es/previous-versions/ms256147(v=vs.110)?redirectedfrom=MSDN
.. _`xsl:output`: https://docs.microsoft.com/es-es/previous-versions/ms256187(v=vs.110)?redirectedfrom=MSDN
.. _`xsl:param`: https://docs.microsoft.com/es-es/previous-versions/ms256096(v=vs.110)?redirectedfrom=MSDN
.. _`xsl:preserve-space`: https://docs.microsoft.com/es-es/previous-versions/ms256144(v=vs.110)?redirectedfrom=MSDN
.. _`xsl:processing-instruction`: https://docs.microsoft.com/es-es/previous-versions/ms256461(v=vs.110)?redirectedfrom=MSDN
.. _`msxsl:script`: https://docs.microsoft.com/es-es/previous-versions/ms256042\(v=vs.110\).aspx
.. _`xsl:sort`: https://docs.microsoft.com/es-es/previous-versions/ms256196(v=vs.110)?redirectedfrom=MSDN
.. _`xsl:strip-space`: https://docs.microsoft.com/es-es/previous-versions/ms256473(v=vs.110)?redirectedfrom=MSDN
.. _`xsl:stylesheet`: https://docs.microsoft.com/es-es/previous-versions/ms256204(v=vs.110)?redirectedfrom=MSDN
.. _`xsl:template`: https://docs.microsoft.com/es-es/previous-versions/ms256110(v=vs.110)?redirectedfrom=MSDN
.. _`xsl:text`: https://docs.microsoft.com/es-es/previous-versions/ms256107(v=vs.110)?redirectedfrom=MSDN
.. _`xsl:transform`: https://docs.microsoft.com/es-es/previous-versions/ms256040(v=vs.110)?redirectedfrom=MSDN
.. _`xsl:value-of`: https://docs.microsoft.com/es-es/previous-versions/ms256181(v=vs.110)?redirectedfrom=MSDN
.. _`xsl:variable`: https://docs.microsoft.com/es-es/previous-versions/ms256447(v=vs.110)?redirectedfrom=MSDN
.. _`xsl:when`: https://docs.microsoft.com/es-es/previous-versions/ms256164(v=vs.110)?redirectedfrom=MSDN
.. _`xsl:with-param`: https://docs.microsoft.com/es-es/previous-versions/ms256091(v=vs.110)?redirectedfrom=MSDN
