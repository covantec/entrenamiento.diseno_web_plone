.. -*- coding: utf-8 -*-

==============
Funciones XSLT
==============

Las funciones XLST se utilizan como parte de las expresiones ``XPath`` de
una hoja de estilos ``XSLT`` para tener acceso al nodo actual (``current()``),
fusionar distintos archivos de datos XML en uno solo (``document()``),
mantener compatibilidad de versiones (``element-available()`` o
``function-available()``), dar formato a los números (``format-number()``) o
comprobar propiedades del sistema. 

La siguiente tabla muestra una lista completa de las funciones ``XSLT`` tal 
y como se especifican en la norma W3C y se implementan en las versiones
4.0 o posteriores de *Microsoft XML Core Services* (``MSXML``).

Recuerde que estas funciones solo pueden llamarse desde el procesador
``XSLT``. No se pueden llamar desde el método ``selectNodes`` del DOM 
(Document Object Model, Modelo de objetos de documento). No ocurre 
lo mismo desde las funciones ``XPath``, que pueden especificarse dentro 
del argumento para el método ``selectNodes``.

Listado de funciones
--------------------

* `current`_, Devuelve un conjunto de nodos que tiene el nodo actual como 
  único miembro.

* `document`_, Proporciona un modo de recuperar otros recursos XML desde la 
  hoja de estilos ``XSLT`` más allá de los datos iniciales proporcionados por el 
  flujo de entrada.

* `element-available`_, Devuelve un valor verdadero únicamente si el nombre 
  expandido es el nombre de una instrucción.

* `format-number`_, Convierte el primer argumento en una cadena mediante la 
  cadena de modelo de formato que especifica el segundo argumento.

* `function-available`_, Devuelve un valor verdadero si la función se encuentra 
  en la biblioteca de funciones.

* `generate-id`_, Devuelve una cadena que solo identifica el nodo del argumento 
  ``node-set`` que está en primer lugar en el orden del documento.

* `key`_, Recupera elementos previamente marcados con una instrucción ``<xsl:key>``.

* `node-set`_, Convierte un árbol en un conjunto de nodos. El nodo resultante 
  siempre contiene un único nodo, que es el nodo raíz del árbol.

* `system-property`_, Devuelve un objeto que representa el valor de la propiedad 
  del sistema que identifica el nombre.

* `unparsed-entity-uri`_, Devuelve declaraciones de entidades sin analizar de la 
  definición del tipo de documento (``DTD``) del documento de origen.

A continuación se muestran pruebas de nodos, no funciones:

-  ``text()``

-  ``processing-instruction()``

-  ``comment()``

-  ``node()``

Para obtener más información, vea `Pruebas de nodos`_.

.. seealso::

    - `Referencia de tipos de datos XML`_.

.. _`current`: https://docs.microsoft.com/es-es/previous-versions/ms256477(v=vs.110)?redirectedfrom=MSDN
.. _`document`: https://docs.microsoft.com/es-es/previous-versions/ms256465(v=vs.110)?redirectedfrom=MSDN
.. _`element-available`: https://docs.microsoft.com/es-es/previous-versions/ms256475(v=vs.110)?redirectedfrom=MSDN
.. _`format-number`: https://docs.microsoft.com/es-es/previous-versions/ms256225(v=vs.110)?redirectedfrom=MSDN
.. _`function-available`: https://docs.microsoft.com/es-es/previous-versions/ms256124(v=vs.110)?redirectedfrom=MSDN
.. _`generate-id`: https://docs.microsoft.com/es-es/previous-versions/ms256051(v=vs.110)?redirectedfrom=MSDN
.. _`key`: https://docs.microsoft.com/es-es/previous-versions/ms256228(v=vs.110)?redirectedfrom=MSDN
.. _`node-set`: https://docs.microsoft.com/es-es/previous-versions/ms256197(v=vs.110)?redirectedfrom=MSDN
.. _`system-property`: https://docs.microsoft.com/es-es/previous-versions/ms256189(v=vs.110)?redirectedfrom=MSDN
.. _`unparsed-entity-uri`: https://docs.microsoft.com/es-es/previous-versions/ms256080(v=vs.110)?redirectedfrom=MSDN
.. _`Pruebas de nodos`: https://docs.microsoft.com/es-es/previous-versions/ms256450(v=vs.110)?redirectedfrom=MSDN
.. _`Referencia de tipos de datos XML`: https://docs.microsoft.com/es-es/previous-versions/bb399504(v=vs.110)?redirectedfrom=MSDN
