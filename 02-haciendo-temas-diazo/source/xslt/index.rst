.. -*- coding: utf-8 -*-

.. _referencia_xslt:


Referencia de XSLT
==================

.. note::
    Este documentación fue tomada originalmente de la `referencia XSLT`_ 
    de MSDN.

Esta sección proporciona documentación de referencia de *Extensible Stylesheet 
Language Transformations - XSLT* y abarca lo siguiente:


.. toctree::
    :maxdepth: 2
 
    elementos
    sintaxis
    funciones

.. _`referencia XSLT`: https://docs.microsoft.com/es-es/previous-versions/ms256069(v=vs.110)?redirectedfrom=MSDN
