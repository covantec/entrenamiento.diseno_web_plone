.. -*- coding: utf-8 -*-

Sintaxis de XSLT
================

En esta sección se tratan los siguientes temas:

-  `Nombres completos <https://docs.microsoft.com/es-es/previous-versions/ms256151(v=vs.110)?redirectedfrom=MSDN>`_.

-  `Valores NaN <https://docs.microsoft.com/es-es/previous-versions/ms256038(v=vs.110)?redirectedfrom=MSDN>`_.

-  `Expresiones <https://docs.microsoft.com/es-es/previous-versions/ms256066(v=vs.110)?redirectedfrom=MSDN>`_.

-  `Modelos <https://docs.microsoft.com/es-es/previous-versions/ms256113(v=vs.110)?redirectedfrom=MSDN>`_.
