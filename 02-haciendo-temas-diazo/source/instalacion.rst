.. -*- coding: utf-8 -*-

.. _diazo_instalacion:

Instalación
===========

Para instalar ``diazo``, usted debe instalar la distribución 
de `diazo`_  de PyPI.

.. note::

    El paquete ``diazo`` sólo se requiere para obtener 
    el compilador ``diazo`` y las herramientas de desarrollo. 
    Si se implementa el tema ``diazo`` en un servidor web, no 
    es necesario la distribución ``diazo`` en ese servidor.

Usando easy_install
-------------------

Usted puede instalar la distribución de ``diazo`` usando 
`easy_install`_, `pip`_ o `zc.buildout`_. Por ejemplo, usando 
:program:`easy_install` (sería ideal si se ejecuta dentro de 
un `entorno virtual`_ Python):

.. code-block:: console

    $ easy_install -U diazo

De esta forma usted ya tiene instalado las herramienta del 
servidor ``diazo``.

.. tip::
    Al finalizar la instalación, más scripts están disponibles 
    en el consola comando para trabajar con ``diazo``. Para entender 
    su uso, por favor, consulte la :ref:`referencia de comando <diazo_usando>`.


.. _diazo_buildout:

Usando zc.buildout
------------------

Opcionalmente, si esta usando zc.buildout, usted puede usar la 
siguiente configuración :file:`buildout.cfg` como punto de arranque. 
Este asegura que los scripts de consola estén instalados, lo cual 
es importante si usted necesita ejecutar manualmente el compilador 
``diazo``:

.. code-block:: cfg

    [buildout]
    parts =
       diazo

    [diazo]
    recipe = zc.recipe.egg
    eggs = diazo

En algunos sistemas operativos, en particular, **Mac OS X** la 
instalación de un *"buen"* :term:`paquete` (Python egg) de ``lxml`` 
puede ser problemático, debido a una falta de coincidencia en las 
versiones del sistema operativo de las librerías ``lxml`` con respecto 
a la ``libxml2`` y ``libxslt``. Para resolver esto, se puede compilar 
una librería ``lxml`` de forma estático de :term:`paquete Egg` usando 
la siguiente receta buildout:

.. code-block:: cfg

    [buildout]
    # lxml debería estar de primero en la lista parts
    parts =
       lxml
       diazo

    [lxml]
    recipe = z3c.recipe.staticlxml
    egg = lxml

    [diazo]
    recipe = zc.recipe.egg
    eggs = diazo


Entonces usted tiene que comenzar de arranque del proyecto, con el 
siguiente comando:

.. code-block:: console

    $ python bootstrap.py

Luego ejecute la construcción de su configuración zc.buildout, con 
el siguiente comando:

.. code-block:: console

    $ ./bin/buildout -vN

.. note::

    Note que el paquete ``lxml`` es una dependencia de ``diazo``, usted 
    podría necesitar instalar los paquetes de desarrollo de ``libxml2`` 
    y ``libxslt`` para poder construir esta configuración zc.buildout. 
    En *Debian GNU/Linux* o *Ubuntu Linux* usted puede ejecutar el 
    siguiente comando:

.. code-block:: console

    $ sudo apt-get install build-essential python-dev libxml2-dev libxslt1-dev

Una ves instalado, usted debería buscar los scripts :program:`diazocompiler` 
y :program:`diazorun` en su directorio :file:`bin`.

----

.. _diazo_wsgi:

Usando middleware WSGI
-----------------------

Si usted quiere usar el filtro `middleware WSGI`_, usted debería usar el 
parámetro extra ``[wsgi]`` cuando se instale el paquete ``diazo``, a 
continuación un ejemplo:

.. code-block:: cfg

    [buildout]
    extends = http://good-py.appspot.com/release/diazo/1.0b1
    versions = versions
    parts =
        diazo

    [diazo]
    recipe = zc.recipe.egg
    eggs =
        diazo [wsgi]
        PasteScript

    [lxml]
    recipe = z3c.recipe.staticlxml
    egg = lxml

Entonces usted tiene que comenzar de arranque del proyecto, con el 
siguiente comando:

.. code-block:: console

    $ python bootstrap.py

Luego ejecute la construcción de su configuración zc.buildout, con el 
siguiente comando:

.. code-block:: console

    $ ./bin/buildout -vN

Al finalizar la construcción zc.buildout más archivos se añaden a la 
lista scripts disponibles en el directorio :file:`bin/`, incluyendo 
los scripts :program:`paster`, :program:`diazocompiler` o 
:program:`diazorun`.

Ahora puede crear una carpeta llamada :file:`theme` que contiene diversos 
recursos para el tema.

.. code-block:: console

    $ mkdir theme

A continuación, crea el archivo :file:`proxy.ini` en el directorio de 
su proyecto zc.buildout:

.. code-block:: ini

    [server:main]
    use = egg:Paste#http
    host = 0.0.0.0
    port = 5000

    [composite:main]
    use = egg:Paste#urlmap
    /static = static
    / = default

    [app:static]
    use = egg:Paste#static
    document_root = %(here)s/theme

    [pipeline:default]
    pipeline = theme
               content

    [filter:theme]
    use = egg:diazo
    rules = %(here)s/rules.xml
    prefix = /static
    debug = true

    # Proxy: por ejemplo, Plone, cuyo nombre es Plone en 127.0.0.1:8080.
    [app:content]
    use = egg:Paste#proxy
    address = http://127.0.0.1:8080/VirtualHostBase/http/127.0.0.1:5000/Plone

Uno sólo tiene que ejecutar el ``proxy`` con el siguiente comando:

.. code-block:: console

    $ ./bin/paster serve --reload proxy.ini

A continuación, puede tener acceso a nuestra página en http://127.0.0.1:5000.

.. todo::
    Agregar imagen del URL http://127.0.0.1:5000.

.. todo::
    Agregar imagen del URL http://127.0.0.1:8080/Plone.


.. _`diazo`: https://pypi.org/project/diazo
.. _`easy_install`: https://plone-spanish-docs.readthedocs.io/es/latest/python/setuptools.html
.. _`pip`: https://plone-spanish-docs.readthedocs.io/es/latest/python/distribute_pip.html
.. _`zc.buildout`: https://plone-spanish-docs.readthedocs.io/es/latest/buildout/replicacion_proyectos_python.html
.. _`entorno virtual`: https://plone-spanish-docs.readthedocs.io/es/latest/python/creacion_entornos_virtuales.html
.. _`middleware WSGI`: https://en.wikipedia.org/wiki/Python_Paste#WSGI_middleware
