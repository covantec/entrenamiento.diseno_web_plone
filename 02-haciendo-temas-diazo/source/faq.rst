.. -*- coding: utf-8 -*-

.. _diazo_consejos:

Preguntas Frecuentes
====================

**¿Qué es diazo?**

    Es una tecnología para hacer temas / apariencias HTML, aplicando un estilo 
    consistente a aplicaciones y los archivos estáticos, independientemente de 
    cómo se implementan, y separando de todo el sitio de estilo del nivel de 
    aplicación de plantillas

**¿Qué diferencia hay entre XDV y diazo?**

    Ninguna en si misma más allá de la evolución natural del desarrollo XDV, 
    ahora es conocida como ``diazo``.

**¿Que diferencia hay entre diazo y Deliverance?**

    Realmente la diferencia esta en que ``diazo`` usa transformaciones ``XSLT`` las cuales 
    pueden hacer transformaciones no solamente para HTML, sino también pueden manipular XML 
    de todo tipo como RSS, ATOM.

    En cambio ``Deliverance`` te deja usar ``CSS3``, pero sus procesos internos son un misterio 
    para la mayoría, al menos sabiendo que ``diazo`` usa ``XSLT``, y de esta forma el programador o integrador puedo intuir como está haciendo las transformaciones, ya que ``XSLT`` es estándar.

**¿Por que el uso de XSLT?**

    * ``XSLT`` significa Transformaciones ``XSL``.
    
    * ``XSLT`` es la parte más importante de ``XSL``.
    
    * ``XSLT`` transforma un documento XML en otro documento ``XML``.
    
    * ``XSLT`` utiliza ``XPath`` para navegar en documentos ``XML``.
    
    * ``XSLT`` es una recomendación de la *W3C*.
