.. -*- coding: utf-8 -*-

.. _diazo_usando:

Guía de uso
===========

Luego de la instalación, tiene *tres (03)* nuevos ``scripts`` 
disponibles en el para trabajar con ``diazo`` en la consola 
comando, como :ref:`diazopreprocessor <diazopreprocessor_script>`, 
:ref:`diazocompiler <diazocompiler_script>` o :ref:`diazorun <diazorun_script>`.


.. _diazopreprocessor_script:

Comando diazopreprocessor
-------------------------

Puede consultar la ayuda del comando :program:`diazopreprocessor` ejecutando 
el siguiente comando:

.. code-block:: console

    $ diazopreprocessor -h
    Use: diazopreprocessor [-r] RULES

    Pre-procesa RULES, un archivo de reglas diazo


    Opciones:
      -h, --help            muestra este mensaje de ayuda y sale
      -o output.xsl, --output=output.xsl
                            archivo de salida (instead of stdout)
      -p, --pretty-print    Pretty print output (may alter rendering in browser)
      --trace               Compiler trace logging
      -a /, --absolute-prefix=/
                            urls relativas en el archivo de tema serán hechas a 
                            enlaces absolutos con este prefijo.
      -i INC, --includemode=INC
                            incluye modo (document, ssi, ssiwait o esi)
      -n, --network         Le permite leer a la red para consultar recursos
      -t theme.html, --theme=theme.html
                            archivo de Tema
      -r rules.xml, --rules=rules.xml
                            archivo de reglas Diazo
      -c param1,param2=defaultval, --custom-parameters=param1,param2=defaultval
                            Listado separado por coma de nombres de parámetro 
                            personalizado con opcional valores por defecto que 
                            el tema that the compiled theme will
                            be able accept when run
      -e extra.xsl, --extra=extra.xsl
                            Extra XSL to be included in the transform (depracated,
                            use inline xsl in the rules instead)
      -s n, --stop=n        Stop preprocessing at stage n


.. _diazocompiler_script:

Comando diazocompiler
---------------------

Puede consultar la ayuda del comando :program:`diazocompiler` ejecutando 
el siguiente comando:

.. code-block:: console

    $ diazocompiler -h
    Use: diazocompiler [opciones] [-r] RULES [-t] THEME

      THEME es un archivo HTML.
      RULES is a file defining a set of diazo rules in css syntax, e.g:
        <rules xmlns="http://namespaces.plone.org/diazo">
            <copy content="//div[@id='content-wrapper']"
                  theme="//div[@id='page-content']"/>
        </rules>

    Opciones:
      -h, --help            muestra este mensaje de ayuda y sale
      -o output.xsl, --output=output.xsl
                            archivo de salida (instead of stdout)
      -p, --pretty-print    Pretty print output (may alter rendering in browser)
      --trace               Compiler trace logging
      -a /, --absolute-prefix=/
                            urls relativas en el archivo de tema serán hechas a 
                            enlaces absolutos con este prefijo.
      -i INC, --includemode=INC
                            incluye modo (document, ssi, ssiwait o esi)
      -n, --network         Le permite leer a la red para consultar recursos
      -t theme.html, --theme=theme.html
                            archivo de Tema
      -r rules.xml, --rules=rules.xml
                            archivo de reglas Diazo
      -c param1,param2=defaultval, --custom-parameters=param1,param2=defaultval
                            Comma-separated list of custom parameter names with
                            optional default values that the compiled theme will
                            be able accept when run
      -e extra.xsl, --extra=extra.xsl
                            Extra XSL to be included in the transform (depracated,
                            use inline xsl in the rules instead)


.. _diazorun_script:

Comando diazorun
----------------

Puede consultar la ayuda del comando :program:`diazorun` ejecutando 
el siguiente comando:

.. code-block:: console

    $ diazorun -h
    Use: diazorun -x TRANSFORM CONTENT

      TRANSFORM es la transformación de tema compilado
      CONTENT es un archivo HTML.

    Use: diazorun -r RULES [opciones] CONTENT


    Opciones:
      -h, --help            muestra este mensaje de ayuda y sale
      -o output.xsl, --output=output.xsl
                            archivo de salida (en lugar de la salida estándar)
      -p, --pretty-print    Impresión de salida bonita (puede alterar la representación 
                            en el navegador)
      --trace               Registro de seguimiento del compilador
      -a /, --absolute-prefix=/
                            urls relativas en el archivo de tema serán hechas a 
                            enlaces absolutos con este prefijo.
      -i INC, --includemode=INC
                            incluye modo (document, ssi, ssiwait o esi)
      -n, --network         Le permite leer a la red para consultar recursos
      -t theme.html, --theme=theme.html
                            archivo de Tema
      -r rules.xml, --rules=rules.xml
                            archivo de reglas Diazo
      -c param1,param2=defaultval, --custom-parameters=param1,param2=defaultval
                            Lista de nombres de parámetros personalizados separados 
                            por comas con valores predeterminados opcionales que el 
                            tema compilado podrá aceptar cuando se ejecute
      -e extra.xsl, --extra=extra.xsl
                            Se debe incluir XSL adicional en la transformación (obsoleto, 
                            utilice en línea xsl en las reglas en su lugar)
      -x transform.xsl, --xsl=transform.xsl
                            transformación XSL
      --path=PATH           ruta URI
      --parameters=param1=val1,param2=val2
                            Definir los valores de parámetros arbitrarios
      --runtrace-xml=runtrace.xml
                            Escribe un formato xml runtrace al archivo
      --runtrace-html=runtrace.html
                            Escribe un formato html runtrace al archivo


Modos de uso
------------

Hay tres (03) maneras de utilizar el motor de plantillas ``diazo`` a 
continuación se describen cada opción:

* Si requiere un tema ``diazo`` para Plone, debe utilizar el 
  :ref:`Soporte de temas Diazo <diazo_ploneapptheming>` incorporado en Plone.

* Si requiere un tema para una aplicación Python :ref:`WSGI <diazo_wsgi>`, 
  puede utilizar el componente de *middleware WSGI*.

* Si requiere un tema casi para cualquier cosa, puede implementar un
  tema compilado para Nginx u otro servidor Web.

Para probar ``diazo``, sin embargo, la forma más fácil es instalar 
un ``proxy`` simple. La idea es ejecutar un servidor web local que 
se aplica el tema ``diazo`` a una respuesta que viene de un sitio Web 
existente, ya sea a nivel local o en algún lugar en el Internet.


.. _diazo_ploneapptheming:

Soporte de temas Diazo en Plone
...............................

Plone incorpora por defecto desde la versión 4.1, un complemento llamado 
**Soporte de temas Diazo** distribuido en el paquete `plone.app.theming`_, 
el cual posee un *panel de control* llamado **Temas** que le permite manipular 
los temas ``diazo`` disponible para su sitio Plone, (Ver Figura 4.1).

.. figure:: _static/theming_controlpanel_1.png
  :align: center
  :width: 65%
  :alt: Panel de control Temas

  Panel de control Temas.

Si está trabajando con Plone, el modo más fácil de usar, 
debido a ``diazo`` es una dependencia que fue instalada por complemento 
``plone.app.theming``, esto le omite su instalación. 

Ademas este complemento proporciona un panel de control para configurar 
el archivo de reglas de ``diazo``, el tema y otras opciones, y los parámetros 
en una cadena de transformación que se ejecuta después de Plone ha hecho que 
la página final para aplicar la transformación ``diazo``, (Ver Figura 4.2).

.. figure:: _static/plone_app_theming.png
  :align: center
  :width: 65%
  :alt: Editor de temas diazo

  Editor de temas diazo.

**Desarrollando temas**

Incluso si usted tiene la intención de implementar el tema complejo en
otro servidor web, ``plone.app.theming`` es una herramienta de desarrollo útil:
mientras Zope está en *"modo de desarrollo"*, se volverá a compilar el tema
sobre la marcha, lo que le permite hacer cambios en el tema y las reglas
en tiempo real. También proporciona algunas herramientas para empacar su tema
y su implementación en diferentes sitios.

.. todo::
    Por terminar.


.. _diazo_wsgi:

Diazo como middleware WSGI
..........................

.. todo::
    Por definir.


.. _diazo_web_xslt:

Soporte de servidor Web y XSLT
..............................

.. todo::
    Por definir.

.. _`plone.app.theming`: https://pypi.org/project/plone.app.theming
