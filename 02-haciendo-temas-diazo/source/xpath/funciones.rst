.. -*- coding: utf-8 -*-

.. _funciones_xpath:

Funciones de XPath
==================

Puede utilizar las funciones ``XPath`` (lenguaje de rutas XML) para volver a
definir consultas de ``XPath`` y mejorar la eficacia y flexibilidad de
programación de ``XPath``.

Las funciones se dividen en los siguientes grupos.

* `Node-Set`_, Toma un argumento *node-set* y devuelve un conjunto de nodos, 
  o devuelve o proporciona información sobre un nodo concreto de un conjunto 
  de nodos.

* `Cadena`_, Realiza evaluaciones, da formato y manipula argumentos de cadenas.

* `Booleano`_, Evalúa las expresiones de argumentos para obtener un resultado 
  booleano.

* `Número`_, Evalúa las expresiones de argumentos para obtener un resultado 
  numérico.

* `Funciones de extensión de XPath de Microsoft`_, Se trata de las funciones de 
  extensiones de Microsoft para ``XPath`` que proporcionan la capacidad para seleccionar 
  nodos según el tipo de ``XSD``.

También incluye funciones de comparación de cadenas, comparación de números y 
conversión de fecha y hora.

Todas las funciones de la biblioteca de funciones se especifican mediante un 
prototipo de función que proporciona el tipo resultante, el nombre de la función 
y el tipo de argumento. Si un tipo de argumento va seguido de un signo de interrogación, 
el argumento es opcional; de no ser así, el argumento es obligatorio. Los nombres 
de función distinguen entre mayúsculas y minúsculas.

.. _`Node-Set`: https://docs.microsoft.com/es-es/previous-versions/ms256482(v=vs.110)?redirectedfrom=MSDN
.. _`Cadena`: https://docs.microsoft.com/es-es/previous-versions/ms256180(v=vs.110)?redirectedfrom=MSDN
.. _`Booleano`: https://docs.microsoft.com/es-es/previous-versions/ms256218(v=vs.110)?redirectedfrom=MSDN
.. _`Número`: https://docs.microsoft.com/es-es/previous-versions/ms256035(v=vs.110)?redirectedfrom=MSDN
.. _`Funciones de extensión de XPath de Microsoft`: https://docs.microsoft.com/es-es/previous-versions/ms256453(v=vs.110)?redirectedfrom=MSDN
