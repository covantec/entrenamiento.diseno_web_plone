.. -*- coding: utf-8 -*-

.. _referencia_xpath:

===================
Referencia de XPath
===================

.. note::
    Este documentación fue tomada originalmente de la `referencia XPath`_ 
    de MSDN.

Esta sección proporciona la siguiente documentación de referencia acerca
del *XML Path Language - XPath*:

.. toctree::
 
    sintaxis
    funciones

Otros recursos
--------------

Para obtener más información, consulte XML Path Language (XPath) Version 1.0
(Recomendación del W3C del 16 de noviembre de 1999) en www.w3.org/TR/xpath.

.. _`referencia XPath`: https://docs.microsoft.com/es-es/previous-versions/ms256115(v=vs.110)?redirectedfrom=MSDN
