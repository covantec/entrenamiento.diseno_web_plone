.. -*- coding: utf-8 -*-

.. _sintaxis_xpath:

Sintaxis de Xpath
=================

Una expresión ``XPath`` (lenguaje de rutas ``XML``) utiliza una notación de
ruta, como las que se utilizan en direcciones URL, para dirigir partes
de un documento ``XML``. La expresión se evalúa para obtener un objeto del
conjunto de nodos, un valor booleano, un número o un tipo de cadena. Por
ejemplo, la expresión ``book/author`` devolverá un conjunto de nodos de los
elementos ``<author>`` incluidos en los elementos ``<book>``, si dichos
elementos aparecen declarados en el documento XML de origen. Además, una
expresión ``XPath`` puede tener predicados (expresiones de filtro) o
llamadas de función. Por ejemplo, la expresión ``book[@type="Fiction"]``
hace referencia a los elementos ``<book>`` cuyo atributo ``type`` se establece
como "Fiction".

La siguiente tabla resume algunas de las características similares de
las direcciones URL y las expresiones de ``XPath``.

Direcciones URL

Expresiones de ``XPath``

Jerarquía compuesta de carpetas y archivos en un sistema de archivos.

Jerarquía compuesta de elementos y otros nodos en un documento ``XML``.

Los archivos de cada nivel tienen nombres únicos. Las direcciones URL
siempre identifican un solo archivo.

Es posible que los nombres de los elementos de cada nivel no sean
únicos. Las expresiones de ``XPath`` identifican un conjunto de todos los
elementos coincidentes

Se evalúan en relación con una carpeta concreta, llamada la "carpeta
actual".

Se evalúa en relación con un nodo concreto llamado el "contexto" para la
expresión.

Esta sección trata la sintaxis de expresiones ``XPath`` (lenguaje de rutas ``XML``), 
incluido lo siguiente:

- `Contexto para las expresiones de XPath`_.

- `Operadores y caracteres especiales`_.

- `Colecciones`_.

- `Filtros y patrones de filtros`_.

- `Valores booleanos, comparaciones y expresiones fijas`_.

- `Comparaciones`_.

- `Operaciones fijas`_.

- `Rutas de ubicación`_.

- `Ejemplo de XPath`_.

Los temas de esta sección utilizan el `Archivo XML de muestra para sintaxis de XPath (inventory.xml)`_.

``XPath`` también admite espacios de nombres y tipos de datos. Los prefijos
de los espacios de nombres pueden incluirse en expresiones de manera que
las operaciones coincidentes puedan buscar determinados prefijos de
espacios de nombres.

.. _`Contexto para las expresiones de XPath`: https://docs.microsoft.com/es-es/previous-versions/ms256199(v=vs.110)?redirectedfrom=MSDN
.. _`Operadores y caracteres especiales`: https://docs.microsoft.com/es-es/previous-versions/ms256122(v=vs.110)?redirectedfrom=MSDN
.. _`Colecciones`: https://docs.microsoft.com/es-es/previous-versions/ms256090(v=vs.110)?redirectedfrom=MSDN
.. _`Filtros y patrones de filtros`: https://docs.microsoft.com/es-es/previous-versions/ms256060(v=vs.110)?redirectedfrom=MSDN
.. _`Valores booleanos, comparaciones y expresiones fijas`: https://docs.microsoft.com/es-es/previous-versions/ms256081(v=vs.110)?redirectedfrom=MSDN
.. _`Comparaciones`: https://docs.microsoft.com/es-es/previous-versions/ms256135(v=vs.110)?redirectedfrom=MSDN
.. _`Operaciones fijas`: https://docs.microsoft.com/es-es/previous-versions/ms256074(v=vs.110)?redirectedfrom=MSDN
.. _`Rutas de ubicación`: https://docs.microsoft.com/es-es/previous-versions/ms256039(v=vs.110)?redirectedfrom=MSDN
.. _`Ejemplo de XPath`: https://docs.microsoft.com/es-es/previous-versions/ms256086(v=vs.110)?redirectedfrom=MSDN
.. _`Archivo XML de muestra para sintaxis de XPath (inventory.xml)`: https://docs.microsoft.com/es-es/previous-versions/ms256095(v=vs.110)?redirectedfrom=MSDN
