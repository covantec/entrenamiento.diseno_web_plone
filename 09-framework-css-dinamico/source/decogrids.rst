.. -*- coding: utf-8 -*-

.. _decogrids:

=========================
Deco Grid System en Plone
=========================

.. _decogrids_acerca:

Acerca Deco Grid System
=======================

`Deco Grid System`_, es un sistema de rejilla implementado por defecto en Plone 4 en adelante. El 
uso de rejilla Deco es muy útil para el diseño y distribución de elementos en la interfaz de usuario

.. _decogrids_info:

.. sidebar:: Ficha técnica del producto

   :Página del proyecto: https://github.com/limi/deco.gs
   :Repositorio de código: https://github.com/limi/deco.gs
   :Programador del producto: Alexander Limi.

Soporte por defecto en Plone
============================

Plone soporta por defecto *Deco Grid System* a través del tema 
`plonetheme.sunburst`_, el cual incluye el archivo CSS `columns.css`_, 
el cual implementa el sistema de rejilla Deco, (Ver Figura 2.1).

.. figure:: _static/plonetheme_sunburst.png
  :align: center
  :width: 75%
  :alt: Paquete de tema plonetheme.sunburst

  Paquete de tema *plonetheme.sunburst*.

.. _plonetheme_sunburst_quees:

¿Qué hace?
----------

``plonetheme.sunburst`` es el `paquete de tema`_ por defecto dentro 
Plone no usa ninguna tabla para diseñar, ya que se basa sistema de 
rejilla *Deco Grid System* el cual actualmente es usado en old.plone.org. 
Este trabaja perfectamente en todos los navegadores Web, incluyendo IE6.

La rejilla utilizada trabaja tanto en modos ``fixed-width`` y ``flexible-width``, 
hay una sección comentada en tope del archivo CSS el cual usted puede habilitar 
si quiere un diseño ``fixed-width``.

Este tema cuando es visto en un dispositivo con menos de 640px de ancho, 
los portlets prolijamente se mete bajo la área de contenido principal. Esto 
significa que el sitio trabaja bien con CSS habilitadas para dispositivos 
como tablets y teléfonos que quizás tenga resolución limitada.

.. _plonetheme_sunburst_info:

.. sidebar:: Ficha técnica del producto

   :Página del proyecto: https://pypi.org/project/plonetheme.sunburst
   :Repositorio de código: https://github.com/plone/plonetheme.sunburst
   :Programador del producto: Plone Foundation.

.. _plonetheme_sunburst_instalar:

¿Cómo instalarlo?
-----------------

En versiones Plone 4.x y posterior este tema viene incorporado por defecto, 
así que no es necesario configurarlo, instalarlo usando la herramienta :term:`buildout`
ni mucho menos activarlo en el panel de **Complementos** o **Temas**.

Personalizar rejilla de Plone
=============================

Si desea personalizar la rejilla basada en *Deco Grid System* para su 
producto de tema Plone puede usar el paquete `kombinat.decogrid`_.

.. _kombinat_decogrid:

Paquete kombinat.decogrid
-------------------------

El paquete ``kombinat.decogrid`` es un generador de código CSS para 
el sistema de diseños basado en *Deco Grid System*.

.. _kombinat_decogrid_quees:

¿Qué hace?
..........

``kombinat.decogrid`` provee un generador de código CSS para el sistema 
de diseños basado en *Deco Grid System*.

Este paquete genera código CSS similar al archivo ``columns.css`` en 
producto tema ``plonetheme.sunburst``.

Copie el archivo CSS generado dentro del directorio de estilos skins 
de su `paquete de tema`_ ``ploneconf2014.theme/diazo_resources/static/columns.css`` 
para sobrescribir cualquier otra definiciones.

.. _kombinat_decogrid_info:

.. sidebar:: Ficha técnica del producto

   :Página del proyecto: https://pypi.org/project/kombinat.decogrid/
   :Repositorio de código: https://github.com/collective/kombinat.decogrid
   :Programador del producto: Daniel Widerin.

.. _kombinat_decogrid_instalar:

¿Cómo instalarlo?
.................

La instalación de este producto se realiza usando la herramienta 
:term:`buildout` para esto, usted tiene que agregar el mismo archivo 
:file:`buildout.cfg` la siguiente configuración:

.. code-block:: cfg

  [buildout]
  parts =
      ...
      decogrid

  [decogrid]
  recipe = zc.recipe.egg
  eggs = kombinat.decogrid

Luego ejecute el script :command:`buildout`, de la siguiente forma:

.. code-block:: sh

  $ ./bin/buildout -vN

Con este comando busca el paquete en el repositorio :term:`PyPI`, descarga e 
instala el producto.

Guía de comando
...............

El ejecutable auto-generado esta ubicado en su directorio buildout ``bin/``.
Ese le permite muchas opciones para generar código CSS para *Deco Grid System*, 
el cual puede ser directamente colocado dentro de su archivo ``columns.css``:

.. code-block:: sh

  $ ./bin/decogrid_generatecss -h
  use: decogrid_generatecss [-h] [-c COLUMNS] [-m MARGIN] [-w WIDTH]
                            [-l LEVEL] [-t TAG] [-o]

  decogrid_generatecss [argumentos opcionales] decogrid_generatecss generador 
  de archivo columns.css basado en Deco Grid para Plone (https://github.com/limi/deco.gs). 
  Los valores por defecto son escritos en paréntesis.

  argumentos opcionales:
    -h, --help            muestra esta mensaje de ayuda y sale
    -c COLUMNS, --columns COLUMNS
                          cuantas columnas (16)
    -m MARGIN, --margin MARGIN
                          margen en píxeles entre cada celda (10)
    -w WIDTH, --width WIDTH
                          ancho del portal en píxeles incluyendo margenes 
                          potenciales de izquierda y/o derecha (960)
    -l LEVEL, --convenience-level LEVEL
                          máximo nivel de conveniencia de clases para crear (7)
    -t TAG, --tag TAG     etiqueta html para ser usado por las clases CSS (div) 
                          'row', 'cell' y 'convenience'
    -o, --omit-margin     omite margen izquierdo y derecho alrededor del portal
                          (recomendado para anidación de rejillas dentro rejillas)

Ejemplo de uso
..............

Esta utilidad es muy útil si usted quiere usar diseño de rejilla anidada 
por que usted puede omitir el margen el cual es dado por defecto en el 
archivo ``columns.css`` incorporado Plone. Para generar un nuevo archivo 
``columns.css`` ejecutando el siguiente comando:

.. code-block:: sh

  $ ./bin/decogrid_generatecss -o -w 950 -c 16 > columns.css

El archivo ``columns.css`` generado por el script ``decogrid_generatecss`` 
luce de esta forma:

.. literalinclude:: _static/columns.css
   :language: css


Luego de generar el archivo ``columns.css`` generado por el 
script ``decogrid_generatecss`` puede personalizar la rejilla 
de 16 columnas *Deco Grid System*.

Ancho de columna
~~~~~~~~~~~~~~~~

El ancho de las clases luego puede ser calculada fácilmente 
con (6.25*n - 2.25)%. Esto conduce, por ejemplo, las siguientes 
clases:

``width-full``
    El ancho total es de 97.75%, ya que la izquierda y la 
    derecha, respectivamente, permanecen recibir margen 
    regular de 1.125%.

``width-1:2``
    El ancho es de 47.75%.

``width-3:4``
    El ancho es de 72.75%.

Posicionamiento
~~~~~~~~~~~~~~~

El posicionamiento de las clases se hace desde el borde derecho 
usando ``margin-left: -100 + (6.25*n) + 1.125``. Esto conduce, 
por ejemplo, las siguientes clases:

``position-0``
    ``{margin-left: -73.875%;}``

``position-1:4``
    ``{margin-left: -73.875%;}``

``position-1:2``
    ``{margin-left: -48.875%;}``

``position-3:4``
    ``{margin-left: -23.875%;}``

Mover columnas
~~~~~~~~~~~~~~

Ahora debe ser el ejemplo posicionado, las dos columnas a la 
izquierda del contenido del portlet, tendrán sólo pequeños 
cambios que son necesarios:

En primer lugar, el modulo ``sunburstview.py`` del paquete 
``plonetheme.sunburst`` existente se sobrescribe en el archivo 
ZCML del paquete ``ploneconf2014.theme`` ubicado en 
``ploneconf2014.theme/ploneconf2014/theme/browser/configure.zcml``:

.. code-block:: xml

    <browser:page
        for="*"
        name="sunburstview"
        class=".sunburstview.SunburstView"
        layer=".interfaces.IThemeSpecific"
        permission="zope.Public"
        allowed_interface="plonetheme.sunburst.browser.interfaces.ISunburstView"
        />

Luego copie el archivo ``sunburstview.py`` del paquete 
``plonetheme.sunburst`` en el directorio del paquete 
``ploneconf2014.theme`` ubicado en ``ploneconf2014.theme/ploneconf2014/theme/browser/`` 
y se cambia en el cálculo de la clase CSS para aquellas etiquetas ``div`` 
que contiene el contenido:

Si se muestran ambas columnas de portlets, el contenido de 
2 a 4 columnas de ancho y empieza desde el centro de la 
página, así:

.. code-block:: python

    elif sl and sr:
        return "cell width-1:2 position-1:2"

Si sólo la columna de portlet derecha, pero no la derecha 
a mostrar, la columna de la derecha se mantiene en su posición 
y en este caso, el contenido será de 2 columnas de ancho 
y comenzar desde el centro de la página:

.. code-block:: python

    elif (sr and not sl) and (not portal_state.is_rtl()):
        return "cell width-1:2 position-1:2"

Por último, tenemos que ajustar el archivo ``main_template.pt`` 
del paquete ``plonetheme.sunburst`` a la columna de portlet derecha 
muestre siempre en segundo lugar en el diseño de rejilla. Para este 
propósito, la clase de posición es cambiada a la etiqueta ``div`` 
con el ID de ``portal-column-two``:

.. code-block:: xml

    <div id="portal-column-two"
         class="cell width-1:4 position-1:4"
         ...
         tal:attributes="class python:isRTL and 'cell width-1:4 position-0' 
                                            or 'cell width-1:4 position-1:4'">

Probar rejilla Deco
~~~~~~~~~~~~~~~~~~~

Puede probar la aplicación del sistema de rejilla Deco 
registrando de forma temporal la *page template* ``grid-test.pt`` 
(que incorpora el paquete ``kombinat.decogrid``) ubicado en la ruta 
``kombinat.decogrid/kombinat/decogrid/grid-test.pt``;
dentro del paquete tema ``ploneconf2014.theme`` en el directorio 
``ploneconf2014.theme/ploneconf2014/theme/browser/``.

Luego puede acceder a la *page template* en la dirección URL 
http://localhost:8080/Plone/grid-test.pt

.. _decogrids_faq:

Preguntas uso frecuente
=======================

¿Cual es la diferencia del Deco Grid System de otro frameworks grid como Blueprint, 960.gs, y otros?
----------------------------------------------------------------------------------------------------

Nosotros creemos Deco Grids es mucha más simple de entender y usar, la convención 
de nombre y el modelo mental es muy simple, y ese posiblemente degrada mejor.

-  No se suministra con nada que esté fuera del sistema de rejilla - opciones 
   de fuente, constructores de formularios, otra CSS no relacionado. Está 
   construido para caer justo a en su existente código CSS con un monto mínimo 
   de esfuerzo.   

-  Está muy bien adecuado para diseño de algoritmos automáticos para CMSs y 
   otras aplicaciones web.

-  No asume que siempre usted trabaja en diseños basados en píxeles, como un 
   montón de otros frameworks lo hacen.

-  No necesita soluciones provisionales / hacks para funcionar de 
   forma fiable en todos los navegadores.

-  Es más pequeño que los otros frameworks de rejilla. Mucho más pequeño pesa 
   menos de 1K - vs. 11K (Blueprint), 5,4K (`960 Grids system`_), etc. Nos dan 
   cuenta de que esta comparación el tamaño no es del todo justa, ya que incluyen 
   un montón de cosas no útiles para rejilla, pero incluso cuando usted quita 
   de sus archivos de todo eso, todavía estamos siendo mucho más ligero.

¿Cuales navegadores trabaja con el Deco Grid System?
----------------------------------------------------

Todos los navegadores que se remontan a (e incluyendo!) Internet Explorer 6,
sin envoltorio, recorte, u otro mal comportamiento. Es una roca sólida.
Es posible que funcione en los navegadores más antiguos, pero aún no hemos probado esto.

¿Funciona con dos diseños fijos (fixed) y fluidos (fluid)?
----------------------------------------------------------

¡Por supuesto!

¿Importa el orden de las células dentro de una fila?
----------------------------------------------------

No, usted puede poner en el orden que tiene más sentido para la pantalla
lectores y dispositivos móviles.

¿Usted mismo lo inventó?
------------------------

La técnica de base utilizada en Deco es algo que se documenta en el
artículo "`Faux Absolute Positioning`_" en A List Apart. Deco es sólo una
sistema que se aplica este método a la noción de un sistema de rejilla CSS.

¿Cómo se hace el posicionamiento vertical? Deco sólo suministra posicionamiento horizontal.
-------------------------------------------------------------------------------------------

Los sistemas CSS "rejilla" son por lo general sobre la colocación horizontal. La
colocación vertical es fácil de hacer con los márgenes habituales CSS, y depende del
ritmo vertical de su diseño, por lo que Deco no abastece esto - en su 
propósito.

¿Cual sistema de rejilla CSS comparable?, ¿Hay otros sistemas que funcionan como Deco Grid System lo hace?
----------------------------------------------------------------------------------------------------------

El más cercano en la filosofía y enfoque sería Emastic, que tiene una
objetivo similar de suministrar un diseño que se adapta bien y no se basa en
colocación de píxel. Emastic utiliza un menos robusto y más complicado
el posicionamiento de la técnica, sin embargo.

¿Qué pasa con la notación ``\3a`` en las definiciones de las clases ``width-1:2`` etc.?
----------------------------------------------------------------------------------------

Es una solución de notación desde Internet Explorer 6 (y 7, posiblemente)
no es compatible con la barra invertida sin formato para los nombres de clases con dos 
puntos en ellos. Por supuesto, puede cambiar el nombre de esto si usted piensa que es feo. 
Nosotros pensamos es más fácil pensar en las tres cuartas partes de ``width-3:4`` lugar de
``width-3_4`` o ``width-3-4``.

¿Yo puedo tener rejillas anidada dentro de rejillas?
----------------------------------------------------

Funciona, pero por lo general no se recomienda, por lo que no arruine su
diseño rompiendo la rejilla invisible con demasiada frecuencia. Funciona 
técnicamente hablando.

¿Cuál es el tamaño del Deco Grid system?
----------------------------------------

La funcionalidad básica es de dos clases (!), Además de 16 conveniencias
de clases para cada posición y ancho. Después de usar el compresor YUI CSS,
es *714 o 907 bytes*, para las versiones de 12 y 16 columnas,
respectivamente.

¿Qué otra cosa que necesito saber?
----------------------------------

Es una buena regla para no combinar otra CSS en los ``divs`` que rigen el
diseño - por lo menos no los márgenes, el ``padding`` y el posicionamiento. 
Crea tu propios elementos dentro de su lugar. Esto hace que las cosas 
fiables y predecibles, y le ahorrará un poco de dolor de depuración.

¿No el uso de divs dedicados para el diseño semántico?, ¿No se acaba reinventando tablas usando los divs?
---------------------------------------------------------------------------------------------------------

En primer lugar, HTML (con algunas excepciones como la etiqueta ``<address>``)
en realidad no es semántico, es *estructural*. Por supuesto, puedes argumentar este
hasta que las ranas vuelva a casa, pero lo más importante a tener en cuenta es que
lectores de pantalla - que son lo más cercano que lleguemos a la semántica de hoy - no 
tiene problema con el uso de ``divs`` como este y en comparación con las soluciones de otros
diseños necesitan, pensamos que usted encontrará este enfoque simple y refrescante.

El punto es no utilizar tablas para diseñar, es que los dispositivos tales como
lectores de pantalla interpretarlos como *tablas de datos*, que no lo son.
Ellos se saltan sobre cualquier ``divs``. Es por esto que existen ``divs`` en el primer lugar - 
una etiqueta libre de semántica para agrupar las cosas.

----

¿Hay algunas buenas herramientas para hacer mas fácil el trabajar con grids?
----------------------------------------------------------------------------

¡Si! recomiendo `GridFox <https://konigi.com/links/gridfox-grid-layout-firefox-extension/>`_ como una 
herramienta de navegador para visualizar rejillas. Para diseñar con herramientas estándar, 
el `960 Grid System <https://960.gs/>`_ tiene un montón de buenas plantillas disponibles
que se puede utilizar como plantillas / capas para alinear su diseño. Estos se aplican
igualmente bien al *Deco Grid System*, (Ver Figura 2.1).

.. figure:: _static/plonetheme_sunburst_gridfox.png
  :align: center
  :width: 75%
  :alt: GridFox con el tema plonetheme.sunburst

  GridFox con el tema *plonetheme.sunburst*.


¿Como hacer al código Deco grid luzca?
--------------------------------------

Un ejemplo simple debería ser:

.. code-block:: html

  <div class="row">
    <div class="cell width-4 position-0">
      Esta celda tiene cuatro (04) unidades de ancho
    </div>
    <div class="cell width-12 position-4">
      Esta celda tiene once (11) unidades de ancho
    </div>
  </div>

Esto produce el diseño siguiente (*agregado el color de fondo para ilustrar* 
Ver Figura 2.2):

.. figure:: _static/deco_grids_00.png
  :align: center
  :alt: Deco Grid System en uso

  Deco Grid System en uso.

Descarga código fuente
======================

Usted puede obtener los códigos fuentes de los paquetes 
usados, ejecutando los siguientes comando:

.. code-block:: sh

  $ git clone https://github.com/plone/plonetheme.sunburst.git
  $ git clone https://github.com/collective/kombinat.decogrid.git

Luego de descargar este código fuente, es recomendable 
leer el archivo :file:`README.rst` y lea las instrucciones 
descrita en ese archivo.

Configuraciones de ejemplo
--------------------------

Usted puede obtener el código fuente de `plonethemes.suite`_, 
que dispone las configuraciones :term:`buildout` necesarias 
para ver los paquetes ``plonetheme.sunburst`` y ``kombinat.decogrid`` 
funcionando, ejecutando el siguiente comando:

.. code-block:: sh

  $ git clone https://github.com/plone-ve/plonethemes.suite.git

Luego de descargar este código fuente, es recomendable leer el archivo 
:file:`README.rst` y siga las instrucciones descrita en ese archivo.

Referencias
===========

* `Deco Grid System en Plone Entwicklerhandbuch`_.

* `kombinat.decogrid en Plone Entwicklerhandbuch`_.

.. _`Deco Grid System`: https://github.com/limi/deco.gs
.. _`plonetheme.sunburst`: https://github.com/plone/plonetheme.sunburst
.. _`columns.css`: https://github.com/plone/plonetheme.sunburst/blob/master/plonetheme/sunburst/skins/sunburst_styles/columns.css
.. _`960 Grids system`: https://960.gs/
.. _`Faux Absolute Positioning`: https://alistapart.com/article/fauxabsolutepositioning
.. _`paquete de tema`: https://plone-spanish-docs.readthedocs.io/es/latest/programacion/crear_producto_tema.html#producto-tema
.. _`plonethemes.suite`: https://github.com/plone-ve/plonethemes.suite
.. _`kombinat.decogrid`: https://pypi.org/project/kombinat.decogrid/
.. _`Deco Grid System en Plone Entwicklerhandbuch`: https://www.plone-entwicklerhandbuch.de/anhang/praxisbeispiele/deco-grid-system/deco-grid-system
.. _`kombinat.decogrid en Plone Entwicklerhandbuch`: https://pypi.org/project/kombinat.decogrid/
