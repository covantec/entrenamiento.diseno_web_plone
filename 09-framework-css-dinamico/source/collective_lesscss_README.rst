.. -*- coding: utf-8 -*-

.. _clesscss:

=================
LESS CSS en Plone
=================

En este articulo busca explicar la instalación del producto 
``collective.lesscss``, para agregar CSS dinámicos con LESS 
en su tema Plone.


.. _lesscss_acerca:

Acerca LESS
===========

LESS es un pre-procesador CSS que utilizan la sintaxis estándar 
de CSS por lo que es bastante sencillo convertir una hoja de 
estilos al formato LESS. Los archivos LESS utiliza la extensión 
``.less``.

.. figure:: _static/less_logo.png
  :align: center
  :width: 65%
  :alt: El pre-procesador LESS CSS

  El pre-procesador LESS CSS.

Aunque tenga tanto parecido a la CSS, pero tiene un montón de 
funciones extra que pueden acelerar el desarrollo. Este extiende 
las CSS con comportamiento dinámico como variables, mixins, 
operaciones y funciones. LESS se ejecuta tanto del lado del 
cliente Web (Chrome, Safari, Firefox) como del lado del 
servidor con Node.js y Rhino.

La instalación de LESS al estar escrito en JavaScript la instalación 
es tan sencilla como importar la librería de JavaScript a tu 
documento HTML, además, también existen algunas herramientas, muchas 
de ellas gratuitas, que ayudan a compilar tus archivos escritos en LESS.

.. note::

    Usted puede buscar mas información acerca de LESS 
    en http://lesscss.org/


.. _clesscss_quees:

Paquete collective.lesscss
==========================

El paquete ``collective.lesscss`` le permite programar agregando
hojas de estilos LESS dentro de un sitio Plone.


¿Qué hace?
----------

Este :term:`Producto Plone` le permite agregar hojas de estilos LESS 
a su sitio Plone.

.. _clesscss_info:

.. sidebar:: Ficha técnica del producto

   :Página del proyecto: https://pypi.org/project/collective.lesscss/
   :Repositorio de código: https://github.com/collective/collective.lesscss
   :Programador del producto: Victor Fernandez de Alba.


.. _clesscss_instalar:

¿Cómo instalarlo?
=================

La instalación de este producto se realiza usando la herramienta 
:term:`buildout` para esto usted tiene que agregar el producto a 
las sección ``eggs`` del archivo :file:`buildout.cfg` como se 
muestra a continuación:

.. code-block:: cfg

  eggs =
      collective.lesscss
      
Luego ejecute el script :command:`buildout`, de la siguiente forma:

.. code-block:: sh

  $ ./bin/buildout -vN

Con este comando busca el paquete en el repositorio :term:`PyPI`, 
descarga e instala el producto en su instancia Zope para sus sitios 
Plone allí hospedados.

Entonces inicie la :term:`Instancia de Zope`, de la siguiente forma:

.. code-block:: sh

  $ ./bin/instance fg
  
Luego de esto ya tiene disponible el servidor Zope, el producto puede 
ser activado en cada sitio Plone dentro de su :term:`Instancia de Zope` 
como se describe a continuación:


Activarlo en Plone
==================

Para activar este producto en un sitio Web Plone 4 usted debe acceder 
a la sección :menuselection:`Configuración del sitio --> Complementos`, 
ubicada en la esquina superior derecha en el nombre del usuario, 
(Ver Figura 5.2).

.. figure:: _static/productos_complementos_1.png
  :align: center
  :alt: Acceder a la Configuración del sitio

  Acceder a la Configuración del sitio

Después haga clic en panel de control **Complementos**, (Ver Figura 5.3).

.. figure:: _static/productos_complementos_2.png
  :align: center
  :alt: Acceder al panel de control Complementos

  Acceder al panel de control Complementos

Entonces marque la casilla llamada **collective.lesscss** y luego 
presione el botón **Activar**.


.. _clesscss_usar:

Integración con Plone
=====================

Este paquete clona el comportamiento de la herramienta ``portal_css``, 
extendiéndolo para los métodos de compilación de *recursos LESS* del 
lado del cliente y del lado del servidor.

El paquete ``collective.lesscss`` ofrece las siguientes características:

* Este agrega la herramienta ``portal_less`` en la ZMI.

* Habilita un perfil ``GenericSetup`` para importar / exportar a través 
  del archivo *lessregistry.xml*.

* Sobrescribe el viewlet del producto *Products.ResourceRegistries* para 
  agregar la parte de *recursos LESS* para el etiqueta HTML <head>.


Agregando recursos LESS
-----------------------

Este paquete está destinado a ser utilizado en conjunción con un paquete 
de tema Plone definido por el usuario. Como un desarrollador, usted puede 
incluir la mayor cantidad de *recursos LESS* que usted pueda necesitar para 
construir su tema. Usted puede agregar *recursos LESS* usando un perfil 
``GenericSetup`` llamado *lessregistry.xml*. La sintaxis es clonada del 
perfil *cssregistry.xml*:

.. code-block:: xml

    <?xml version="1.0"?>
    <object name="portal_less" meta_type="LESS Stylesheets Registry">
      <stylesheet title="++bootstrap++less/bootstrap.less" authenticated="False"
        enabled="on" id="++bootstrap++less/bootstrap.less" rendering="link"/>
    </object>

Así se registra el recurso LESS en el archivo ``bootstrap.less`` en la 
herramienta ``portal_less`` desde la ZMI, (Ver Figura 5.4).

.. figure:: _static/collective_lesscss_portal_less.png
  :align: center
  :alt: La herramienta portal_less del ZMI

  La herramienta **portal_less** del ZMI.


Panel de Control 
----------------

En Plone 4 acceda a la :menuselection:`Configuración del sitio --> Configuración de Complementos --> Herramienta hoja de estilos LESS`, (Ver Figura 5.5).

.. figure:: _static/collective_lesscss_controlpanel_0.png
  :align: center
  :alt: Acceder a la Herramienta hoja de estilos LESS

  Acceder a la *Herramienta hoja de estilos LESS*.

En este panel usted puede administrar la manera en que los *recursos LESS* 
son compilados accediendo al panel de configuración de *recursos LESS* 
localizado en la Configuración del Sitio. Por defecto el modo *compilar 
recursos LESS del lado del cliente* esta habilitado, (Ver Figura 5.6).

.. figure:: _static/collective_lesscss_controlpanel_1.png
  :align: center
  :alt: Acceder a la configuración de recursos LESS

  Acceder a la *configuración de recursos LESS*.


Compilando del lado del cliente
-------------------------------

La compilación recursos LESS del lado del cliente está destinado a ser 
utilizado en modo de desarrollo. El paquete ``collective.lesscss`` usara 
el método estándar para compilar del lado del cliente usando el *less.js* 
(v1.3, al momento en escribir esto) y exponiendo los recursos LESS después 
en la herramienta ``portal_css``:

.. code-block:: html

    <link rel="stylesheet/less" type="text/css" href="styles.less">
    <!-- Here goes the rest of portal_javascript resources -->
    <script src="less.js" type="text/javascript"></script>


Compilando del lado del servidor
--------------------------------

La compilación recursos LESS del lado del servidor es recomendado para modo 
de producción. Deshabilitando esta opción, el sitio compilara del lado del 
servidor entonces dentro de los recursos CSS y habilitara una volátil caché 
en estos. ``Node.js`` es utilizado del lado del servidor para la compilación, 
entonces usted deben tener un ``Node.js`` instalado en su sistema. Es 
recomendado dejar a :term:`buildout` que manipule esta instalación por usted:

.. code-block:: cfg

    [buildout]
    parts = ...
            nodejs
            ...

    [nodejs]
    recipe = gp.recipe. node
    url = http://nodejs.org/dist/v0.10.29/node-v0.10.29.tar.gz
    npms = less
    scripts = lessc

Este descargara y compilara ``Node.js`` y las extensiones LESS. El script 
ejecutable ``lessc`` estará disponible en el directorio ``bin`` de su proyecto 
:term:`buildout`. 

Por favor, revise el `archivo buildout.cfg`_ para mayor referencia.

En caso que usted ya tenga instalado un ``Node.js`` y las extensiones 
LESS en su sistema es requerido para este caso crear un enlace simbólico 
del ejecutable ``lessc`` en el directorio ``bin`` de su proyecto 
:term:`buildout`.

.. warning::

   **NOTA IMPORTANTE**: 

   ¡La compilación del lado del servidor requiere tener declarado los recursos 
   vía el paquete ``plone.resource`` en su paquete!, a continuación un ejemplo 
   de esto:

    .. code-block:: xml

        <plone:static
            directory="resources/less"
            type="bootstrap"
            name="less"
        />

Y además, si usted no esta usando el paquete ``plone.app.themimg`` para 
el desarrollo de su tema usted debería declarar el tipo de recurso que usted 
esta usando creando una clase en alguna parte en su tema (e.j. en el archivo 
``traversal.py``):

.. code-block:: python

    from plone.resource.traversal import ResourceTraverser

    class BootstrapTraverser(ResourceTraverser):
    """The theme traverser.

    Allows traveral to /++bootstrap++<name> using ``plone.resource`` to fetch
    things stored either on the filesystem or in the ZODB.
    """
    name = 'bootstrap'

Y después, declara el adaptador vía declarativas ZCML:

.. code-block:: xml

    <adapter
        name="bootstrap"
        for="* zope.publisher.interfaces.IRequest"
        provides="zope.traversing.interfaces.ITraversable"
        factory=".traversal.BootstrapTraverser"
    />

Usted debería ahora estar habilitado para acceder a los recursos dentro 
del directorio ``resources`` con la dirección URL::

    http://localhost/Plone/++bootstrap++less/


Paquete example.bootstrap
=========================

Usted puede consultar el paquete `example.bootstrap`_ para obtener un 
ejemplo completo en como integrar *recursos LESS* de 
:ref:`Twitter Bootstrap <twitter_bootstrap>` en su paquete de tema.


Descarga código fuente
======================

Usted puede obtener el código fuente usado en este producto, ejecutando el 
siguiente comando:

.. code-block:: sh

  $ git clone https://github.com/collective/collective.lesscss.git

Luego de descargar este código fuente, es recomendable leer el archivo 
:file:`README.rst` y siga las instrucciones descrita en ese archivo.


Configuraciones de ejemplo
--------------------------

Usted puede obtener el código fuente de `plonethemes.suite`_, que dispone 
las configuraciones :term:`buildout` necesarias para ver este paquete 
funcionando, ejecutando el siguiente comando:

.. code-block:: sh

  $ git clone https://github.com/plone-ve/plonethemes.suite.git

Luego de descargar este código fuente, es recomendable leer el archivo 
:file:`README.rst` y siga las instrucciones descrita en ese archivo.

.. _`example.bootstrap`: https://github.com/sneridagh/example.bootstrap
.. _`archivo buildout.cfg`: https://github.com/collective/collective.lesscss/blob/master/buildout.cfg
.. _`plonethemes.suite`: https://github.com/plone-ve/plonethemes.suite
