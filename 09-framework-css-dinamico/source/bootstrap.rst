.. -*- coding: utf-8 -*-

.. _twitter_bootstrap:

==========================
Twitter Bootstrap en Plone
==========================


.. _twitter_bootstrap_acerca:

Acerca Twitter Bootstrap
========================

`Twitter Bootstrap`_, es un framework CSS para diseño de sitios 
y aplicaciones web. Contiene plantillas de diseño con tipografía, 
formularios, botones, cuadros, menús de navegación y otros elementos 
de diseño basado en HTML y CSS, así como, extensiones de JavaScript 
opcionales adicionales.

.. figure:: _static/bootstrap_logo.png
  :align: center
  :width: 65%
  :alt: Twitter Bootstrap Framework

  Twitter Bootstrap Framework

*Bootstrap* tiene un soporte relativamente incompleto para HTML5 y 
CSS 3, pero es compatible con la mayoría de los navegadores web. 
La información básica de compatibilidad de sitios web o aplicaciones 
esta disponible para todos los dispositivos y navegadores. Existe 
un concepto de compatibilidad parcial que hace disponible la información 
básica de un sitio web para todos los dispositivos y navegadores. 
Por ejemplo, las propiedades introducidas en CSS3 para las esquinas 
redondeadas, gradientes y sombras son usadas por *Bootstrap* a pesar 
de la falta de soporte de navegadores antiguos. Esto extiende la 
funcionalidad de la herramienta, pero no es requerida para su uso.

Desde la versión 2.0 también soporta diseños sensibles. Esto significa 
que el diseño gráfico de la página se ajusta dinámicamente, tomando 
en cuenta las características del dispositivo usado (Computadoras, 
tabletas, teléfonos móviles).


Bootstrap y LESS
----------------

*Bootstrap* es modular y consiste esencialmente en una serie de hojas 
de estilo :ref:`LESS <lesscss_acerca>` que implementan la variedad 
de componentes de la herramienta. Una hoja de estilo llamada 
``bootstrap.less`` incluye los componentes de las hojas de estilo. 
Los desarrolladores pueden adaptar el mismo archivo de Bootstrap, 
seleccionando los componentes que deseen usar en su proyecto.

Los ajustes son posibles en una medida limitada a través de una hoja 
de estilo de configuración central. Los cambios más profundos son 
posibles mediante las declaraciones *LESS*.

El uso del lenguaje de *hojas de estilo LESS* permite el uso de 
variables, funciones y operadores, selectores anidados, así como 
clases mixin.

Desde la versión 2.0, la configuración de *Bootstrap* también tiene 
una opción especial de "Personalizar" en la documentación. Por otra 
parte, los desarrolladores eligen en un formulario los componentes 
y ajustes deseados, y de ser necesario, los valores de varias opciones 
a sus necesidades. El paquete consecuentemente generado ya incluye la 
hoja de estilo CSS pre-compilada.


Sistema de rejilla y diseño sensible
------------------------------------

*Bootstrap* viene con una disposición de rejilla estándar de 940 píxeles 
de ancho. Alternativamente, el desarrollador puede usar un diseño de 
ancho-variable. Para ambos casos, la herramienta tiene cuatro variaciones 
para hacer uso de distintas resoluciones y tipos de dispositivos: 
teléfonos móviles, formato de retrato y paisaje, tabletas y computadoras 
con baja y alta resolución (pantalla ancha). Esto ajusta el ancho de las 
columnas automáticamente.


Entendiendo la hoja de estilo CSS
---------------------------------

*Bootstrap* proporciona un conjunto de hojas de estilo que proveen 
definiciones básicas de estilo para todos los componentes de HTML. 
Esto otorga una uniformidad al navegador y al sistema de anchura, 
da una apariencia moderna para el formateo de los elementos de texto, 
tablas y formularios.


Componentes re-usables
----------------------

En adición a los elementos regulares de HTML, *Bootstrap* contiene otra 
interfaz de elementos comúnmente usados. Ésta incluye botones con 
características avanzadas (e.g grupo de botones o botones con opción de 
menú desplegable, listas de navegación, etiquetas horizontales y verticales, 
ruta de navegación, paginación, etc.), etiquetas, capacidades avanzadas 
de miniaturas tipográficas, formatos para mensajes de alerta y barras 
de progreso.


Plug-ins de JavaScript
----------------------

Los componentes de JavaScript para *Bootstrap* están basados en la librería 
jQuery de JavaScript. Los plug-ins se encuentran en la herramienta de 
plug-in de jQuery. Proveen elementos adicionales de interfaz de usuario 
como diálogos, tooltips y carruseles. También extienden la funcionalidad 
de algunos elementos de interfaz existentes, incluyendo por ejemplo una 
función de auto-completar para campos de entrada (input). La versión 2.0 
soporta los siguientes plug-ins de JavaScript: Modal, Dropdown, Scrollspy, 
Tab, Tooltip, Popover, Alert, Button, Collapse, Carousel y Typeahead.


Soporte por defecto en Plone
============================

Plone soporta por defecto *Twitter Bootstrap* a través del tema ``diazo`` 
*Twitter Bootstrap Example* el cual esta incorporado el editor de temas, ya 
que lo ofrece un ejemplo para crear temas con este framework, (Ver Figura 3.1).

Adicionalmente soporta otros diseños de *Twitter Bootstrap* vía el 
:term:`Producto Plone` adicional a través del paquete 
:ref:`diazotheme.bootstrap <diazotheme_bootstrap>`.

.. figure:: _static/theming_controlpanel_1.png
  :align: center
  :alt: panel de control de Temas

  panel de control de Temas

.. _twitter_bootstrap_info:

.. sidebar:: Ficha técnica del producto

   :Página del proyecto: https://github.com/plone/plone.app.theming
   :Repositorio de código: https://github.com/plone/plone.app.theming
   :Programador del producto: Plone Fundation.


.. _twitter_bootstrap_activar:

Activarlo en Plone
------------------

Para activar el producto editor de temas ``diazo`` en Plone usted debe acceder 
a la sección :menuselection:`Configuración del sitio --> Complementos`, ubicada 
en la esquina superior derecha en el nombre del usuario, (Ver Figura 3.2).

.. figure:: _static/productos_complementos_1.png
  :align: center
  :alt: Acceder a la Configuración del sitio

  Acceder a la Configuración del sitio

Después haga clic en panel de control **Complementos**, (Ver Figura 3.3).

.. figure:: _static/productos_complementos_2.png
  :align: center
  :alt: Acceder al panel de control Complementos

  Acceder al panel de control Complementos

Entonces haga clic en el botón **Activar**  en el tema **Soporte de temas Diazo**.
Seguidamente debe acceder el *panel de control de Temas* accediendo por la sección 
:menuselection:`Configuración del sitio --> Temas`, (Ver Figura 3.4).

.. figure:: _static/theming_controlpanel_0.png
  :align: center
  :alt: Acceder al panel de control de Temas

  Acceder al panel de control de Temas

Estando en el *panel de control de Temas* puede activar el tema diazo ``Twitter Bootstrap Example`` 
haciendo clic en el botón **Activar**, para usarlo en su sitio Web, (Ver Figura 3.5).

.. figure:: _static/twitter_bootstrap_example_0.png
  :align: center
  :alt: Activar tema Twitter Bootstrap Example

  Activar tema *Twitter Bootstrap Example*.

Para finalizar abra en una nueva ventana la siguiente dirección http://localhost:8080/Plone/ 
y podrá ver el tema *activo*, (Ver Figura 3.6).

.. figure:: _static/twitter_bootstrap_example_1.png
  :align: center
  :alt: Tema Twitter Bootstrap Example en uso

  Tema *Twitter Bootstrap Example* en uso.

----

.. _diazotheme_bootstrap:

Paquete diazotheme.bootstrap
============================

El paquete ``diazotheme.bootstrap`` provee implementaciones de temas diazo 
para el framework CSS *Twitter Bootstrap*.


.. _diazotheme_bootstrap_quees:

¿Qué hace?
----------

Este paquete provee una implementación **tres (03)** temas ``diazo`` usando el  
framework *Twitter Bootstrap*. Estos temas ``diazo`` son 
:ref:`Twitter Bootstrap Hero <diazotheme_bootstrap_hero>`, 
:ref:`Twitter Bootstrap Marketing Narrow <diazotheme_bootstrap_narrow>` y 
:ref:`Twitter Bootstrap Starter <diazotheme_bootstrap_starter>` para usar desde 
el editor de temas.


.. _diazotheme_bootstrap_info:

.. sidebar:: Ficha técnica del producto

   :Página del proyecto: https://github.com/TH-code/diazotheme.bootstrap
   :Repositorio de código: https://github.com/TH-code/diazotheme.bootstrap
   :Programador del producto: Thijs Jonkman at TH-code.


.. _diazotheme_bootstrap_instalar:

¿Cómo instalarlo?
-----------------

La instalación de este producto se realiza usando la herramienta 
:term:`buildout` para esto, usted tiene que agregar la extensión 
``mr.developer`` de ``buildout`` para descargar el paquete 
``diazotheme.bootstrap`` y sus dependencias los paquetes 
``diazoframework.bootstrap`` y ``diazoframework.plone`` desde 
sus respectivos repositorios de código fuente, (*debido a que aun 
no ha sido publicado* en :term:`PyPI`) con la siguiente configuración:

.. code-block:: cfg

  extensions = mr.developer
  sources = sources
  auto-checkout =
      diazoframework.plone
      diazoframework.bootstrap
      diazotheme.bootstrap

  [sources]
  diazoframework.plone = git https://github.com/collective/diazoframework.plone.git
  diazoframework.bootstrap = git https://github.com/collective/diazoframework.bootstrap.git
  diazotheme.bootstrap = git https://github.com/TH-code/diazotheme.bootstrap.git

Adicionalmente en el mismo archivo :file:`buildout.cfg` usted tiene 
que agregar los paquetes a la sección ``eggs`` como se muestra a 
continuación:

.. code-block:: cfg

  eggs =
      diazoframework.plone
      diazoframework.bootstrap
      diazotheme.bootstrap
    
Luego ejecute el script :command:`buildout`, de la siguiente forma:

.. code-block:: sh

  $ ./bin/buildout -vN

Con este comando busca el paquete en el repositorio :term:`PyPI`, descarga e 
instala el producto en su instancia Zope para sus sitios Plone allí hospedados.

Entonces inicie la :term:`Instancia de Zope`, de la siguiente forma:

.. code-block:: sh

  $ ./bin/instance fg
  
Luego de esto ya tiene disponible el servidor Zope, el producto puede ser activado 
en cada sitio Plone dentro de su :term:`Instancia de Zope` como se describe a 
continuación:

..
    Recursos
    --------

    Para acceder a los recursos de este framework se puede llegar 
    a través de `/++framework++bootstrap` y esos están emplazado en 
    el directorio ``diazoframework.bootstrap/diazoframework/bootstrap/framework/``.
    A continuación la lista de recursos disponibles: ::

        _ carousel.html
        _ css
          _ bootstrap.css
          _ bootstrap.min.css
          _ bootstrap-responsive.css
          _ bootstrap-responsive.min.css
        _ fluid.html
        _ hero.html
        _ img
          _ glyphicons-halflings.png
          _ glyphicons-halflings-white.png
        _ index.html
        _ js
          _ bootstrap.js
          _ bootstrap.min.js
        _ marketing-narrow.html
        _ preview.png
        _ rules
          _ body
            _ columns.xml
            _ columns-narrow.xml
            _ content.xml
            _ edit.xml
            _ footer.xml
            _ footer-inverse.xml
            _ footer-narrow.xml
            _ footer-narrow-inverse.xml
            _ forms.xml
            _ general.xml
            _ general-narrow.xml
            _ grid.xml
            _ header.xml
            _ header-inverse.xml
            _ header-narrow.xml
            _ lead.xml
            _ lead-hero.xml
            _ lead-jumbotron.xml
            _ no-edit.xml
            _ portlets.xml
          _ head
            _ base.xml
            _ theme.xml
        _ starter-template.html
        _ sticky-footer.html


.. _diazotheme_bootstrap_activar:

Activar temas
-------------

El paquete ``diazotheme.bootstrap`` dispone **tres (03)** temas diazo listo para usar, 
para esto debe tener *activo* el producto **Soporte de temas Diazo** para acceder al 
*panel de control de Temas* puede activar cada uno de estos.


.. _diazotheme_bootstrap_hero:

Twitter Bootstrap Hero
~~~~~~~~~~~~~~~~~~~~~~

Este tema de ejemplo **Twitter Bootstrap Hero** (el cual se usa en conjunto 
con el tema *Sunburst*) usted puede usarlo en su sitio Plone haciendo clic en el 
botón **Activar** al tema ``Twitter Bootstrap Hero`` estando en el *panel de 
control de Temas*, (Ver Figura 3.7).

.. figure:: _static/diazotheme_bootstrap_theme_0.png
  :align: center
  :alt: Activar tema Twitter Bootstrap Hero

  Activar tema *Twitter Bootstrap Hero*.

Para finalizar abra en una nueva ventana la siguiente dirección http://localhost:8080/Plone/ 
y podrá ver el tema *activo*, (Ver Figura 3.8).

.. figure:: _static/diazotheme_bootstrap_theme_1.png
  :align: center
  :alt: Tema Twitter Bootstrap Hero en uso

  Tema *Twitter Bootstrap Hero* en uso.


.. _diazotheme_bootstrap_narrow:

Twitter Bootstrap Marketing Narrow
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Este tema de ejemplo **Twitter Bootstrap Marketing Narrow** (el cual se usa en conjunto 
con el tema *Sunburst*) usted puede usarlo en su sitio Plone haciendo clic en el 
botón **Activar** al tema ``Twitter Bootstrap Marketing Narrow`` estando en el *panel de 
control de Temas*, (Ver Figura 3.9).

.. figure:: _static/diazotheme_bootstrap_marketingnarrow_0.png
  :align: center
  :alt: Activar tema Twitter Bootstrap Marketing Narrow

  Activar tema *Twitter Bootstrap Marketing Narrow*.

Para finalizar abra en una nueva ventana la siguiente dirección http://localhost:8080/Plone/ 
y podrá ver el tema *activo*, (Ver Figura 3.10).

.. figure:: _static/diazotheme_bootstrap_marketingnarrow_1.png
  :align: center
  :alt: Tema Twitter Bootstrap Marketing Narrow en uso

  Tema *Twitter Bootstrap Marketing Narrow* en uso.


.. _diazotheme_bootstrap_starter:

Twitter Bootstrap Starter
~~~~~~~~~~~~~~~~~~~~~~~~~

Este tema de ejemplo **Twitter Bootstrap Starter** (el cual se usa en conjunto 
con el tema *Sunburst*) usted puede usarlo en su sitio Plone haciendo clic en el 
botón **Activar** al tema ``Twitter Bootstrap Starter`` estando en el *panel de 
control de Temas*, (Ver Figura 3.11).

.. figure:: _static/diazotheme_bootstrap_starter_0.png
  :align: center
  :alt: Activar tema Twitter Bootstrap Starter

  Activar tema *Twitter Bootstrap Starter*.

Para finalizar abra en una nueva ventana la siguiente dirección http://localhost:8080/Plone/ 
y podrá ver el tema *activo*, (Ver Figura 3.12).

.. figure:: _static/diazotheme_bootstrap_starter_1.png
  :align: center
  :alt: Tema Twitter Bootstrap Starter en uso

  Tema *Twitter Bootstrap Starter* en uso.


Descarga código fuente
======================

Usted puede obtener el código fuente usado en este producto, ejecutando el 
siguiente comando:

.. code-block:: sh

  $ git clone https://github.com/collective/diazoframework.plone.git
  $ git clone https://github.com/collective/diazoframework.bootstrap.git
  $ git clone https://github.com/TH-code/diazotheme.bootstrap.git

Luego de descargar este código fuente, es recomendable leer el archivo :file:`README.rst` 
y lea las instrucciones descrita en ese archivo.


Configuraciones de ejemplo
--------------------------

Usted puede obtener el código fuente de `plonethemes.suite`_, que dispone las configuraciones 
:term:`buildout` necesarias para ver los paquetes ``plone.app.themimg``, ``diazoframework.bootstrap`` 
y ``diazoframework.plone`` funcionando, ejecutando el siguiente comando:

.. code-block:: sh

  $ git clone https://github.com/plone-ve/plonethemes.suite.git

Luego de descargar este código fuente, es recomendable leer el archivo 
:file:`README.rst` y siga las instrucciones descrita en ese archivo.

.. _`Twitter Bootstrap`: https://getbootstrap.com/
.. _`diazotheme.bootstrap`: https://github.com/TH-code/diazotheme.bootstrap
.. _`plonethemes.suite`: https://github.com/plone-ve/plonethemes.suite
