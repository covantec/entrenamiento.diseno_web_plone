.. -*- coding: utf-8 -*-

.. _zurb_foundation:

========================
Zurb Foundation en Plone
========================

.. _zurb_foundation_acerca:

Acerca Zurb Foundation
======================

`Zurb Foundation`_, es un framework para front-end responsive. Este 
proporciona una rejilla de responsive y un montón de HTML y CSS de 
componentes de interfaz de usuario, plantillas y fragmentos de código, 
incluyendo tipografía, formas, botones, navegación y otros componentes 
de la interfaz, así como opcionales de JavaScript extensiones.

.. figure:: _static/foundation_logo.png
  :align: center
  :width: 55%
  :alt: Zurb Foundation Framework

  Zurb Foundation Framework

*Foundation* fue diseñado y probado en numerosos navegadores y 
dispositivos. Es el primer framework para móvil responsive 
construido con Sass/SCSS dando a los diseñadores las mejores 
prácticas para el desarrollo rápido. El framework incluye 
patrones más comunes que se necesitan para crear rápidamente 
prototipos de un sitio responsive. Mediante el uso de mixins 
Sass, los componentes de *Foundation* son fácilmente de 
estilizado y simple de extender.

Desde la versión 2.0 también es compatible con el diseño 
responsive. Esto significa que el diseño gráfico de páginas 
web ajusta dinámicamente, teniendo en cuenta las características 
del dispositivo utilizado (PC, tableta, teléfono móvil). 

Además, dado que 4,0 se ha tomado el principio *"Mobile first"* 
del diseño web responsivo, el diseño y desarrollo para dispositivos 
móviles primero, y la mejora de las páginas web y aplicaciones 
para pantallas más grandes.


Estructura y función
--------------------

*Foundation* es modular y consiste esencialmente en una serie de hojas 
de estilo Sass que implementan los distintos componentes del kit de 
herramienta. El componente de Hojas de estilo pueden ser incluidos 
a través de Sass o mediante la personalización de la descarga inicial 
de Foundation. Los desarrolladores pueden adaptar el archivo de 
*Foundation* en sí, seleccionado los componentes que desean utilizar en 
su proyecto.

Los ajustes son posibles a través de una hoja de estilos de configuración 
central. Más profundos cambios son posibles cambiando las variables Sass.

El uso del lenguaje de hoja de estilo Sass permite el uso de variables, 
funciones y operadores, selectores anidados, así como los llamados mixins.

Desde la versión 3.0, la configuración de *Foundation* también tiene una 
opción especial "Personalizar" en la documentación. Por otra parte, los 
desarrolladores utilizan en un formulario para elegir los componentes 
deseados y ajustar, si fuera necesario, los valores de las distintas 
opciones para sus necesidades. El paquete generado posteriormente ya 
incluye la hoja de estilos CSS pre-construidos.


Sistema de rejilla y el diseño responsive
-----------------------------------------

*Foundation* viene con el estándar con un diseño de rejilla flexible de 
940 píxeles de ancho. El kit de herramientas responde plenamente a 
hacer uso de diferentes resoluciones y tipos de dispositivos: teléfonos 
móviles, formato vertical y horizontal, tabletas y PC con una resolución 
baja y alta (pantalla ancha). Esto ajusta el ancho de las columnas 
automáticamente.


La comprensión de estilo CSS
----------------------------

*Foundation* ofrece un conjunto de hojas de estilo que proporcionan 
definiciones de estilo básicas para todos los componentes HTML clave. 
Estos proporcionan un navegador y uniforme en todo el sistema, aspecto 
moderno para dar formato al texto, tablas y elementos de formulario.


Reutilizables componentes
-------------------------

Además de los elementos HTML regulares, *Foundation* contiene otros 
elementos de la interfaz de uso común. Estos incluyen botones con 
características avanzadas (por ejemplo, la agrupación de botones 
o botones con opciones desplegable, hacer y listas de navegación, 
pestañas horizontales y verticales, la navegación, la ruta de 
navegación, paginación, etc.), etiquetas, capacidades tipográficas 
avanzadas y formato para los mensajes como avisos.


Plug-ins de JavaScript
----------------------

Los componentes de JavaScript de *Foundation* se basan en Zepto.js, 
una alternativa más ligera pero con capacidad para un API para el 
framework de JavaScript jQuery. Proporcionan elementos adicionales 
de interfaz de usuario como diálogo, mensajes informativos o ayuda 
y carruseles. También amplían la funcionalidad de algunos elementos 
de la interfaz existentes, incluyendo por ejemplo un botón desplegable 
dividida.

En la versión 4.0 se soportan los siguientes plugins de JavaScript: 
alertas, clearing, cookie, desplegable, formularios, joyride, magellan, 
orbit, placeholder, reveal, section, tooltips, y topbar.


Soporte en Plone
================

Plone soporta como un :term:`Producto Plone` adicional a *Zurb Foundation* 
a través del paquete :ref:`diazotheme.foundation <diazotheme_foundation>`.

.. figure:: _static/theming_controlpanel_1_1.png
  :align: center
  :alt: panel de control de Temas

  panel de control de Temas

.. _zurb_foundation_info:

.. sidebar:: Ficha técnica del producto

   :Página del proyecto: https://github.com/plone/plone.app.theming
   :Repositorio de código: https://github.com/plone/plone.app.theming
   :Programador del producto: Plone Fundation.


.. _zurb_foundation_activar:

Activarlo en Plone
------------------

Para activar el producto editor de temas ``diazo`` en Plone usted debe acceder 
a la sección :menuselection:`Configuración del sitio --> Complementos`, ubicada 
en la esquina superior derecha en el nombre del usuario, (Ver Figura 4.2).

.. figure:: _static/productos_complementos_1.png
  :align: center
  :alt: Acceder a la Configuración del sitio

  Acceder a la Configuración del sitio

Después haga clic en panel de control **Complementos**, (Ver Figura 4.3).

.. figure:: _static/productos_complementos_2.png
  :align: center
  :alt: Acceder al panel de control Complementos

  Acceder al panel de control Complementos

Entonces haga clic en el botón **Activar**  en el tema **Soporte de temas Diazo**.
Seguidamente debe acceder el *panel de control de Temas* accediendo por la sección 
:menuselection:`Configuración del sitio --> Temas`, (Ver Figura 4.4).

.. figure:: _static/theming_controlpanel_0.png
  :align: center
  :alt: Acceder al panel de control de Temas

  Acceder al panel de control de Temas

Estando en el *panel de control de Temas* puede activar el tema diazo ``*Foundation* example`` 
haciendo clic en el botón **Activar**, para usarlo en su sitio Web, (Ver Figura 4.5).

.. figure:: _static/diazotheme_foundation_starter_0.png
  :align: center
  :alt: Activar tema Foundation Starter

  Activar tema *Foundation Starter*.

Para finalizar abra en una nueva ventana la siguiente dirección http://localhost:8080/Plone/ 
y podrá ver el tema *activo*, (Ver Figura 4.6).

.. figure:: _static/diazotheme_foundation_starter_1.png
  :align: center
  :alt: Tema Foundation Starter en uso

  Tema *Foundation Starter* en uso.

----

.. _diazotheme_foundation:

Paquete diazotheme.foundation
=============================

El paquete `diazotheme.foundation`_ provee implementaciones de temas diazo 
para el framework CSS *Zurb Foundation*.


.. _diazotheme_foundation_quees:

¿Qué hace?
----------

Este paquete provee una implementación **dos (02)** temas diazo usando el  
framework *Zurb Foundation*. Estos temas ``diazo`` son 
:ref:`Foundation Starter <diazotheme_foundation_starter>` y 
:ref:`Foundation Example <diazotheme_foundation_example>` para usar desde 
el editor de temas.

.. _diazotheme_foundation_info:

.. sidebar:: Ficha técnica del producto

   :Página del proyecto: https://github.com/TH-code/diazotheme.foundation
   :Repositorio de código: https://github.com/TH-code/diazotheme.foundation
   :Programador del producto: Thijs Jonkman at TH-code.


.. _diazotheme_foundation_instalar:

¿Cómo instalarlo?
-----------------

La instalación de este producto se realiza usando la herramienta 
:term:`buildout` para esto, usted tiene que agregar la extensión 
``mr.developer`` de ``buildout`` para descargar el paquete 
``diazotheme.foundation`` y sus dependencias los paquetes 
``diazoframework.foundation`` y ``diazoframework.plone`` desde 
sus respectivos repositorios de código fuente, (*debido a que aun 
no ha sido publicado* en :term:`PyPI`) con la siguiente configuración:

.. code-block:: cfg

  extensions = mr.developer
  sources = sources
  auto-checkout =
      diazoframework.plone
      diazoframework.foundation
      diazotheme.foundation

  [sources]
  diazoframework.plone = git https://github.com/collective/diazoframework.plone.git
  diazoframework.foundation = git https://github.com/TH-code/diazoframework.foundation.git
  diazotheme.foundation = git https://github.com/TH-code/diazotheme.foundation.git

Adicionalmente en el mismo archivo :file:`buildout.cfg` usted tiene 
que agregar los paquetes a la sección ``eggs`` como se muestra a 
continuación:

.. code-block:: cfg

  eggs =
      diazoframework.plone
      diazoframework.foundation
      diazotheme.foundation

Luego ejecute el script :command:`buildout`, de la siguiente forma:

.. code-block:: sh

  $ ./bin/buildout -vN

Con este comando busca el paquete en el repositorio :term:`PyPI`, descarga e 
instala el producto en su instancia Zope para sus sitios Plone allí hospedados.

Entonces inicie la :term:`Instancia de Zope`, de la siguiente forma:

.. code-block:: sh

  $ ./bin/instance fg
  
Luego de esto ya tiene disponible el servidor Zope, el producto puede ser activado 
en cada sitio Plone dentro de su :term:`Instancia de Zope` como se describe a 
continuación:


.. _diazotheme_foundation_activar:

Activar temas
-------------

El paquete ``diazotheme.foundation`` dispone **dos (02)** temas diazo listo para usar, 
para esto debe tener *activo* el producto **Soporte de temas Diazo** para acceder al 
*panel de control de Temas* puede activar cada uno de estos.


.. _diazotheme_foundation_starter:

Foundation Starter
~~~~~~~~~~~~~~~~~~

También conocido como el tema de ejemplo **Foundation** (el cual se usa en conjunto 
con el tema *Sunburst*) usted puede usarlo en su sitio Plone haciendo clic en el 
botón **Activar** al tema ``Foundation`` estando en el *panel de 
control de Temas*, (Ver Figura 4.7).

.. figure:: _static/diazotheme_foundation_starter_0.png
  :align: center
  :alt: Activar tema Foundation

  Activar tema *Foundation*.

Para finalizar abra en una nueva ventana la siguiente dirección http://localhost:8080/Plone/ 
y podrá ver el tema *activo*, (Ver Figura 4.8).

.. figure:: _static/diazotheme_foundation_starter_1.png
  :align: center
  :alt: Tema Foundation Starter en uso

  Tema *Foundation Starter* en uso.


.. _diazotheme_foundation_example:

Foundation Example
~~~~~~~~~~~~~~~~~~

Este tema de ejemplo **Foundation Example** para usar como un tema padre (el cual 
se usa en conjunto con el tema *Sunburst*) usted puede usarlo en su sitio Plone 
haciendo clic en el botón **Activar** al tema ``Foundation Example`` estando en el 
*panel de control de Temas*, (Ver Figura 4.9).

.. figure:: _static/diazotheme_foundation_example_0.png
  :align: center
  :alt: Activar tema Foundation Example

  Activar tema *Foundation Example*.

Para finalizar abra en una nueva ventana la siguiente dirección http://localhost:8080/Plone/ 
y podrá ver el tema *activo*, (Ver Figura 4.10).

.. figure:: _static/diazotheme_foundation_starter_1.png
  :align: center
  :alt: Tema Foundation Example en uso

  Tema *Foundation Example* en uso.


Descarga código fuente
======================

Usted puede obtener el código fuente usado en este producto, ejecutando el 
siguiente comando:

.. code-block:: sh

  $ git clone https://github.com/collective/diazoframework.plone.git
  $ git clone https://github.com/TH-code/diazoframework.foundation.git
  $ git clone https://github.com/TH-code/diazotheme.foundation.git

Luego de descargar este código fuente, es recomendable leer el archivo :file:`README.rst` 
y lea las instrucciones descrita en ese archivo.


Configuraciones de ejemplo
--------------------------

Usted puede obtener el código fuente de `plonethemes.suite`_, que dispone 
las configuraciones :term:`buildout` necesarias para ver los paquetes 
``plone.app.themimg``, ``diazoframework.foundation`` y ``diazoframework.plone`` 
funcionando, ejecutando el siguiente comando:

.. code-block:: sh

  $ git clone https://github.com/plone-ve/plonethemes.suite.git

Luego de descargar este código fuente, es recomendable leer el archivo 
:file:`README.rst` y siga las instrucciones descrita en ese archivo.

.. _`Zurb Foundation`: https://get.foundation/
.. _`diazotheme.foundation`: https://github.com/TH-code/diazotheme.foundation
.. _`plonethemes.suite`: https://github.com/plone-ve/plonethemes.suite
