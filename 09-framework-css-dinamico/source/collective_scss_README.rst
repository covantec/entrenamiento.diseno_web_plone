.. -*- coding: utf-8 -*-

.. _csasscss:

=================
Sass CSS en Plone
=================

En este articulo busca explicar la instalación del producto 
``collective.scss``, para agregar CSS dinámicos con SCSS 
en su tema Plone.


.. _sasscss_acerca:

Acerca Sass CSS
===============

SCSS es un pre-procesador CSS que utilizan la sintaxis estándar de CSS 
por lo que es bastante sencillo convertir una hoja de estilos al formato 
SCSS. Los archivos SCSS utiliza la extensión ``.scss``.

.. figure:: _static/sass_logo.png
  :align: center
  :width: 45%
  :alt: El pre-procesador SCSS CSS

  El pre-procesador SCSS CSS.

.. todo::
    POR TERMINAR.

.. note::

    Usted puede buscar mas información acerca de SCSS en https://sass-lang.com/


.. _csasscss_quees:

Paquete collective.scss
=======================

El paquete ``collective.scss`` le ofrece integración ``SCSS`` dentro 
de Plone.


¿Qué hace?
----------

Este :term:`Producto Plone` le ofrece integración ``SCSS`` dentro 
de Plone. Este paquete implementa la librería Python llamada 
`python-scss`_ de *Kirill Klenov* disponible en github.

Usted puede escribir hojas de estilos ``SCSS`` y entonces registrarlas 
dentro de la herramienta registro CSS `portal_css`_ en el ZMI.

.. _csasscss_info:

.. sidebar:: Ficha técnica del producto

   :Página del proyecto: https://pypi.org/project/collective.scss/
   :Repositorio de código: https://github.com/collective/collective.scss
   :Programador del producto: JeanMichel FRANCOIS.


.. _csasscss_instalar:

¿Cómo instalarlo?
=================

La instalación de este producto se realiza usando la herramienta 
:term:`buildout` para esto usted tiene que agregar el producto a 
las sección ``eggs`` del archivo :file:`buildout.cfg` como se 
muestra a continuación:

.. code-block:: cfg

  eggs =
      collective.scss
      
Luego ejecute el script :command:`buildout`, de la siguiente forma:

.. code-block:: sh

  $ ./bin/buildout -vN

Con este comando busca el paquete en el repositorio :term:`PyPI`, 
descarga e instala el producto en su instancia Zope para sus sitios 
Plone allí hospedados.

Entonces inicie la :term:`Instancia de Zope`, de la siguiente forma:

.. code-block:: sh

  $ ./bin/instance fg

Luego de esto ya tiene disponible el servidor Zope, el producto puede 
ser activado en cada sitio Plone dentro de su :term:`Instancia de Zope` 
como se describe a continuación:


Activarlo en Plone
==================

Para activar este producto en un sitio Web Plone 4 usted debe acceder 
a la sección :menuselection:`Configuración del sitio --> Complementos`, 
ubicada en la esquina superior derecha en el nombre del usuario, (Ver Figura 5.2).

.. figure:: _static/productos_complementos_1.png
  :align: center
  :alt: Acceder a la Configuración del sitio

  Acceder a la Configuración del sitio

Después haga clic en panel de control **Complementos**, (Ver Figura 5.3).

.. figure:: _static/productos_complementos_2.png
  :align: center
  :alt: Acceder al panel de control Complementos

  Acceder al panel de control Complementos

Entonces marque la casilla llamada **collective.scss** y luego 
presione el botón **Activar**.


.. _cscss_usar:

Como usar SCSS con Plone
========================

Usted puede crear un nuevo archivo (``mysheet.scss``). Lo próximo es 
registrar una hoja de estilo CSS como *Browser View* usando su archivo 
``scss``:

.. code-block:: xml

    <browser:page
      name="mysheet.css"
      for="*"
      class="collective.scss.stylesheet.SCSSView"
      template="mysheet.scss"
      permission="zope2.View
      />


Agregando recursos SCSS
-----------------------

Ahora usted tiene que registrar su hoja de estilos en la herramienta 
de registro CSS en el ZMI ``portal_css`` que ofrece de Plone. Usando 
un perfil GenericSetup usted solo tiene que agregar una entrada en el 
archivo ``cssregistry.xml``, reiniciar su servidor Zope y aplicar el 
perfil:

.. code-block:: xml

    <?xml version="1.0"?>
    <object name="portal_css">
     <stylesheet title=""
        id="mysheet.css"
        media="screen" rel="stylesheet" rendering="import"
        cacheable="True" compression="safe" cookable="True"
        enabled="1" expression="" />
    </object>

.. seealso::

    `collective.sassy`_ es otro paquete que ofrece soporte 
    a SASS en Plone.


Descarga código fuente
======================

Usted puede obtener el código fuente usado en este producto, ejecutando el 
siguiente comando:

.. code-block:: sh

  $ git clone https://github.com/collective/collective.scss.git

Luego de descargar este código fuente, es recomendable leer el archivo 
:file:`README.rst` y siga las instrucciones descrita en ese archivo.


Configuraciones de ejemplo
--------------------------

Usted puede obtener el código fuente de `plonethemes.suite`_, que dispone 
las configuraciones :term:`buildout` necesarias para ver este paquete 
funcionando, ejecutando el siguiente comando:

.. code-block:: sh

  $ git clone https://github.com/plone-ve/plonethemes.suite.git

Luego de descargar este código fuente, es recomendable leer el archivo 
:file:`README.rst` y siga las instrucciones descrita en ese archivo.

.. _`python-scss`: https://github.com/klen/python-scss
.. _`portal_css`: https://plone-spanish-docs.readthedocs.io/es/latest/zope/zmi/index.html#portal-css
.. _`collective.sassy`: https://github.com/tlyng/collective.sassy
.. _`plonethemes.suite`: https://github.com/plone-ve/plonethemes.suite
