.. -*- coding: utf-8 -*-

.. _introduccion:

Introducción
============

En la actualidad es casi un requerimiento indispensable a la 
hora de afrontar proyectos de diseño web que este soporte diseño 
responsivo, adaptables a cualquier pantalla, los diseñadores y 
programadores web contamos con multitud de herramientas que nos 
facilitan el trabajo, especialmente cuando se trata de resolver 
construcciones repetitivas y trilladas; es en estas circunstancias 
donde los frameworks CSS (Foundation, Bootstrap, Pure, etc.) y 
librerías de mixins y utilidades (Compass, Bourbon, etc.) exhiben 
todo su potencial.

Framewok CSS
------------

Son framework de software preparados que están destinados a 
permitir la más fácil, más compatible con los estándares de 
diseño web utilizando el lenguaje de hojas de estilo en cascada. 
La mayoría de estos frameworks contienen al menos una rejilla. 

Los frameworks más funcionales también vienen con más 
características y funciones adicionales basadas JavaScript, pero 
son en su mayoría de orientado al diseño y Unobtrusive JavaScript. 
Algunos ejemplos notables y ampliamente utilizados son 
:ref:`Bootstrap <twitter_bootstrap_acerca>` o 
:ref:`Foundation <zurb_foundation_acerca>`. 

Estos frameworks CSS ofrecen diferentes módulos y herramientas 
como las que se listan a continuación:

* Hoja de estilo Reset.

* Rejilla especialmente para el responsive web design.

* Tipografía web.

* Conjunto de iconos en sprites vs fuentes icono.

* Estilo para tooltips, botones, elementos de formularios.

* Partes de las interfaces gráficas de usuario como efecto acordeón, 
  pestañas, diapositivas (slideshow) o ventanas Modal (Lightbox).

* Ecualizador para crear un contenido igual altura.

* A menudo se utiliza clases de ayuda CSS (left, hide).

.. tip::
    Los framewoks CSS más grandes utilizan :ref:`pre-procesadores CSS <preprocesadores_css>` 
    para facilitar su mantenimiento y personalización de sus archivos CSS.

Clasificación de framework
..........................

Se puede clasificar los frameworks de CSS en dos (02) tipos:

* Multipropósito.- Proporcionan un conjunto de utilidades variadas 
  útiles para distintos aspectos del desarrollo de un diseño web. 
  Ej. `Cascade Framework`_.

* Propósito específico.- Están orientados a proporcionar herramientas 
  para un propósito determinado. Por ejemplo:

  * Frameworks orientados a proporcionar herramientas para el desarrollo 
    de rejillas (Ej. **Deco Grid System**, **Responsive Grid System**, 
    **Skeleton**, **Semantic Grid System**).

  * Frameworks orientadas al control de la parte gráfica del interfaz 
    de usuario (Ej. **Acordeón**, **pestañas**, **diapositivas**).

  * Frameworks orientados a la tipografía, etc.

Diseño web responsivo
---------------------

En la actualidad no se concibe diseñar un sitio Web únicamente 
para escritorio, hay que ofrecer una experiencia perfecta sobre 
cualquier pantalla, desde monitores de escritorio de gran formato 
hasta un pequeño smartphone, pasando por un ecosistema cada vez 
más fragmentado de dispositivos: smartphones, tablets, phablets 
(a medio camino de los anteriores) e incluso smartTVs (para ver 
sitio Web), (Ver Figura 1.1).

.. figure:: _static/mobile-first.png
  :align: center
  :width: 75%
  :alt: Principio "Mobile first" del diseño web responsivo

  Principio "Mobile first" del diseño web responsivo.

.. tip:: 
    También conocido como *Responsive Web Design*.

Compatibilidad con browsers
---------------------------

Hay que tener en cuenta que los frameworks CSS se van a encargar 
de homogeneizar la experiencia de uso a lo largo de los diferentes 
navegadores disponibles en el mercado (compatibilidad de browsers), 
de forma que, como creadores, nos olvidaremos de las particularidades 
de cada uno de ellos, (Ver Figura 1.2).

.. figure:: _static/logos_navegadores_web.png
  :align: center
  :width: 75%
  :alt: Compatibilidad con navegadores Web

  Compatibilidad con navegadores Web.

Diagramar con rejillas
----------------------

.. sidebar:: ¡Sobre la rejilla!

    "El sistema de rejillas es una ayuda, no una garantía. 
    Permite un número de posibles usos y cada diseñador puede 
    buscar una solución adecuada a su estilo personal. Pero 
    hay que aprender a usar la rejilla; es un arte que requiere 
    práctica." --*Josef Müller-Brockmann*.

En el diseño gráfico, una rejilla es una estructura (por lo general 
de dos dimensiones) compuesto por una serie de líneas de guía recta 
de intersección (verticales, horizontales y angulares) o curvas 
utilizadas para estructurar el contenido, (Ver Figura 1.3).

.. figure:: _static/250px-Grid2aib.png
  :align: center
  :width: 35%
  :alt: Sistema de rejillas en medio impresos

  Sistema de rejillas en medio impresos.

La rejilla sirve como una armadura en el que un diseñador puede organizar 
los elementos gráficos (imágenes, pictogramas, párrafos) de una manera 
racional, fácil de absorber de forma.

Una rejilla se puede utilizar para organizar los elementos gráficos 
en relación a una página, en relación con otros elementos gráficos 
en la página, o relación con otras partes del mismo elemento gráfico 
o forma.

.. tip::
    El término impresión menos común "sistema de referencia", es un 
    sistema relacionado con raíces en los inicios de la imprenta.

.. _rejillas_diseno_web:

Uso de la rejilla en el diseño web
..................................

.. sidebar:: El propósito de la rejilla en el diseño web

    Es ideal para el prototipado rápido, pero funcionaría igual de bien 
    cuando se integren en un entorno de producción. Posee hojas de estilo 
    para impresión, diseños de plantillas, y un archivo CSS que tienen 
    mediciones idénticas.

Mientras que los sistemas de rejillas han tenido un uso significativo de 
los medios impresos, el interés de los desarrolladores web ha visto 
recientemente un resurgimiento. 

Los frameworks diseño de página web produciendo HTML y CSS han existido 
durante un tiempo antes de que los nuevos frameworks popularizaron el uso 
de diseños basados en la rejilla.

Algunos sistemas de rejilla especifican elementos de ancho fijo con píxeles, 
y algunos son "fluido", lo que significa que ellos llaman de elemento de 
página de tamaño para estar en unidades relativas, como porcentajes, en lugar 
de unidades absolutas como píxeles o puntos.

Algunos ejemplos de frameworks del sistema de rejilla son:

* :ref:`Deco Grid System <decogrids>`.

* `The 960 Grid System <https://960.gs/>`_.

* `The Responsive Grid System <http://www.responsivegridsystem.com/>`_.

* Semantic Grid System - Un framework de rejilla CSS.

También hay frameworks de CSS que incluyen su propio sistema de rejilla:

* :ref:`Twitter Bootstrap <twitter_bootstrap>`.

* :ref:`Zurb Foundation <zurb_foundation>`.

* `Cascade Framework <https://en.wikipedia.org/wiki/Cascade_Framework>`_.


Sistema de rejillas
...................

Generalmente el número de rejillas más común es de doce (12) rejillas, 
por framework CSS como :ref:`Twitter Bootstrap <twitter_bootstrap>`, 
:ref:`Zurb Foundation <zurb_foundation>`, etc, (Ver Figura 1.4):

.. figure:: _static/deco_grids_example12.png
  :align: center
  :width: 75%
  :alt: Ejemplo de sistema de doce (12) rejillas

  Ejemplo de sistema de doce (12) rejillas.

Más Plone implementa por defecto dieciséis (16) rejillas usando el sistema 
de rejilla :ref:`Deco Grid <decogrids>`, (Ver Figura 1.5).

.. figure:: _static/deco_grids_example16.png
  :align: center
  :width: 75%
  :alt: Ejemplo de sistema de doce (16) rejillas

  Ejemplo de sistema de doce (16) rejillas.

.. _preprocesadores_css:

Pre-procesadores CSS
--------------------

Son aplicaciones que procesan cierto tipo de archivos para crear hojas 
de estilos con algunas de las ventajas de un lenguaje de programación 
común como el uso de variables, funciones, condiciones, la posibilidad 
de hacer cálculos  matemáticos  y que además te permiten utilizar una 
sintaxis mucho más sencilla e intuitiva.

Si tienes algo de experiencia en el desarrollo de sitios web es probable 
que hayas tenido muchos dolores de cabeza trabajando con hojas de estilos 
CSS, esto, sobretodo porque son muy difíciles de mantener ordenadas y 
al no ser propiamente un lenguaje de programación tienen limitantes como 
el no poder utilizar variables, funciones, realizar operaciones matemáticas, 
etc.

Como solución a este problema y es que nacen los *pre-procesadores CSS*. 
Hay muchos pre-procesadores, pero los más populares: :ref:`LESS <lesscss_acerca>` 
y :ref:`SASS <sasscss_acerca>`, cada uno con sus ventajas.

.. figure:: _static/sass_less_logo.png
  :align: center
  :width: 65%
  :alt: Pre-procesadores LESS y SASS

  Pre-procesadores LESS y SASS.

.. _`Cascade Framework`: https://en.wikipedia.org/wiki/Cascade_Framework
