.. -*- coding: utf-8 -*-

=============
Recetas Diazo
=============

Diazo y las recetas XSL para casos especiales.


Agregar atributos al vuelo
==========================

El ejemplo agrega el atributo ``target="_blank"`` a todos los enlaces 
en un portlet con el atributo de class ``portlet-collection-links``.

.. code-block:: xml

   <?xml version="1.0" encoding="UTF-8"?>
   <rules
       xmlns="http://namespaces.plone.org/diazo"
       xmlns:css="http://namespaces.plone.org/diazo/css"
       xmlns:xsl="http://www.w3.org/1999/XSL/Transform">


       <!-- agrega atributo target="_blank" a todos los enlaces 
            en portlet-collection-links -->
       <xsl:template match="//dl[contains(@class,'portlet-collection-links')]//a">
         <a target="_blank">
          <xsl:apply-templates 
            select="./@*[contains(' href title class rel ', concat(' ', name(), ' '))]"/>
          <xsl:value-of select="." />
         </a>
       </xsl:template>


       <!-- tema wrapper, para prevenir aplicar el tema a la ZMI -->
       <rules css:if-content="#visual-portal-wrapper">
         
         [...]

       </rules>
   </rules>

Esto debería también funcionar, pero no si el nodo seleccionado ya tiene 
nodos hijos.

.. code-block:: xml

   <xsl:template match="//dl[contains(@class,'portlet-collection-links')]//a">
       <xsl:attribute name="target">_blank</xsl:attribute><xsl:copy-of select="." />
   </xsl:template>

Entonces a usted le viene el siguiente error:

::

   XSLTApplyError: xsl:attribute: Cannot add attributes to an element if 
   children have been already added to the element.

En el sitio diazo.org hay otra manera descrita en las recetas:
http://docs.diazo.org/en/latest/recipes/adding-an-attribute/index.html

.. code-block:: xml

   <?xml version="1.0" encoding="UTF-8"?>
   <rules xmlns="http://namespaces.plone.org/diazo"
          xmlns:css="http://namespaces.plone.org/diazo/css"
          xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   
       <replace css:theme="#target" css:content="#content" />
   
       <xsl:template match="a">
           <xsl:copy>
               <xsl:attribute name="target">_blank</xsl:attribute>
               <xsl:copy-of select="@*" />
               <xsl:apply-templates />
           </xsl:copy>
       </xsl:template>
   
   </rules>


Agregar clase CSS dependiendo de la existencia de portal-columns
================================================================

Este agrega una clase CSS para cada portal-column existente a la 
etiqueta ``body``.
Si ``portal-column-one`` existe se agrega ``col-one``, si 
``portal-column-content`` existe se agrega ``col-content`` y si 
``portal-column-two`` existe se agrega ``col-two``.

.. code-block:: xml

   <before theme-children="/html/body" method="raw">
     <xsl:attribute name="class">
      <xsl:value-of select="/html/body/@class" />
      <xsl:if css:test="#portal-column-one"> col-one</xsl:if>
      <xsl:if css:test="#portal-column-content"> col-content</xsl:if>
      <xsl:if css:test="#portal-column-two"> col-two</xsl:if>
     </xsl:attribute>
   </before>

Este puede ser usado para cambiar el diseño de rejilla como este
----------------------------------------------------------------
El siguiente ejemplo muestra la ``scss`` para ``compass`` el cual 
define diferente columnas de la rejilla dependiendo de las clases 
en el body.

.. code-block:: css
   
   body.col-one.col-content.col-two #content-wrapper {
     @include container;

     #portal-column-content {
       @include column(12);
       @include prepend(4.5);
     }

     #portal-column-one {
       @include column(4.5);
       @include pull(16.5);
     }

     #portal-column-two {
       @include column(4.5, true);
     }
   }
   body.col-one.col-content #content-wrapper {
     @include container;

     #portal-column-content {
       @include column(16.5, true);
       @include prepend(4.5);
     }

     #portal-column-one {
       @include column(4.5);
       @include pull(21, true);
     }
   }


Mover elementos alrededor de plone
==================================

A veces necesita mover elementos de Plone desde un lugar a otro o 
mezclar algunos elementos juntos. En el siguiente ejemplo se puede 
mezclar la bandera de lenguaje junto con las acciones de documento.

.. code-block:: xml

   <replace css:content-children=".documentActions > ul">
     <xsl:for-each select="//*[@class='documentActions']/ul/li">
       <xsl:copy-of select="." />
     </xsl:for-each>
     <xsl:for-each select="//*[@id='portal-languageselector']/*">
       <xsl:copy-of select="." />
     </xsl:for-each>
   </replace>


Remover el valor del searchButton
=================================

A veces es muy útil remover el valor del texto del botón de búsqueda, 
por ejemplo si usted quiere una imagen solo en el botón de búsqueda. 
El siguiente código ``XSL`` remplaza el valor del class ``searchButton`` 
con una cadena de texto vacía.

.. code-block:: xml

   <xsl:template 
     match="//div[@id='portal-searchbox']//input[@class='searchButton']/@value">
     <xsl:attribute name="value"></xsl:attribute>
   </xsl:template>

Este especial caso también puede ser resuelto con puras sentencias CSS 
como esta:

.. code-block:: css

   .searchButton{
     text-indent: -10000px;
   }
