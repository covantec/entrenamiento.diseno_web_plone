=========================
Fragmentos de código HTML
=========================

Los fragmentos de código HTML de los elementos por defecto 
de Plone para copiar dentro de su plantilla de tema estática.

Plantilla HTML básica
=====================

A continuación la plantilla HTML básica:

.. code-block:: html

   <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
       "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
   <html>
   <head>
     <title>Plone CMS es OpenSource y esta basado en Python y Zope</title>
     <meta name="viewport" content="width=device-width" />
   </head>
   <body>
   <div id="visual-portal-wrapper">

     <!-- header de Plone -->

     <!-- body de Plone -->

     <!-- footer de Plone -->

   </div>
   </body>
   </html>


.. toctree::
   :maxdepth: 2

   plone_layout/index.txt
   plone_elements/index.txt
